"""Extract information on GitLab CI job for tracing test results in Jira."""

import argparse
import json
import os

# Parse arguments

parser = argparse.ArgumentParser(
    description="Extract information on GitLab CI job"
)
parser.add_argument(
    "--component", type=str, help="component under test", default=""
)
parser.add_argument(
    "--environment", type=str, help="test environment", default=""
)
parser.add_argument(
    "--test-plan", type=str, help="test plan ticket number in Jira", default=""
)
parser.add_argument("filename", type=str, help="output file name")
args = parser.parse_args()

# Read GitLab CI environment variables

project_title = os.environ.get("CI_PROJECT_TITLE")
project_url = os.environ.get("CI_PROJECT_URL")
pipeline_url = os.environ.get("CI_PIPELINE_URL")
commit_ref = os.environ.get("CI_COMMIT_REF_NAME")
commit_message = os.environ.get("CI_COMMIT_MESSAGE")

# Construct tracing data

JIRA_PROJECT_ID = "12400"  # XTP project
JIRA_ISSUE_TYPE = "11602"  # Test execution issue type
JIRA_ENVIRONMENT_FIELD = "customfield_11925"  # Field for environments
JIRA_TEST_PLAN_FIELD = "customfield_11927"  # Field for test plan keys

SUMMARY = f"{project_title} Test Execution ({commit_ref})"
DESCRIPTION = f"""Test execution results from {project_title}

Repository: {project_url}
Pipeline: {pipeline_url}
Commit reference (branch/tag): {commit_ref}
Commit message:

{commit_message}
"""

fields = {
    "project": {"id": JIRA_PROJECT_ID},
    "issuetype": {"id": JIRA_ISSUE_TYPE},
    "summary": SUMMARY,
    "description": DESCRIPTION,
}

if args.component:
    fields["components"] = [{"name": args.component}]

if args.environment:
    fields[JIRA_ENVIRONMENT_FIELD] = [args.environment]

if args.test_plan:
    fields[JIRA_TEST_PLAN_FIELD] = [args.test_plan]

data = {"fields": fields}

# Write data to file

with open(args.filename, "w", encoding="utf-8") as fp:
    json.dump(data, fp)

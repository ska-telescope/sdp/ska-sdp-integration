"""Combined test-results.json cucumber files"""

import argparse
import json

parser = argparse.ArgumentParser(
    description="Combined test-results.json cucumber files"
)
parser.add_argument(
    "input_files", type=str, nargs="+", help="input file names"
)
parser.add_argument("output_file", type=str, help="output file name")

args = parser.parse_args()

data = []

for file in args.input_files:
    with open(file, encoding="utf-8") as jfile:
        json_data = json.load(jfile)
    data.extend(json_data)

with open(args.output_file, "w", encoding="utf-8") as fp:
    json.dump(data, fp)

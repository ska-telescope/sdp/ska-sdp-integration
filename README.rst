SDP Integration
===============

This repository integrates the components of the SKA Science Data Processor
(SDP). It contains the Helm chart to deploy the SDP as a subsystem of the Low
or Mid telescope, or stand-alone for testing.

Standard CI machinery
---------------------

This repository is set up to use the
`Makefiles <https://gitlab.com/ska-telescope/sdi/ska-cicd-makefile>`__
and `CI jobs <https://gitlab.com/ska-telescope/templates-repository>`__
maintained by the System Team. For any questions, please look at the
documentation in those repositories or ask for support on Slack in the
#team-system-support channel.

To keep the Makefiles up to date in this repository, follow the
instructions at:
https://gitlab.com/ska-telescope/sdi/ska-cicd-makefile#keeping-up-to-date

Custom CI jobs
^^^^^^^^^^^^^^

The ``k8s-test`` job in the CI pipeline is a custom one to run the SDP
integration tests.

The ``k8s-dp-test`` job and its extensions are used to run the
visibility receive test regularly, on schedule (“Persistent SDP (DP
Platform)”) on the persistent deployment of SDP, which is currently set
up to run on the Data Processing platform, in the dp-shared (and
dp-shared-p) namespace.

Creating a new release
----------------------

When you are ready to make a new release:

- Check out the master branch
- Find the release in the `Release
  Management <https://jira.skatelescope.org/projects/REL/summary>`__
  project or create a new ticket
- Update the version number with

  - ``make bump-patch-release``,
  - ``make bump-minor-release``, or
  - ``make bump-major-release``

- Update ``CHANGELOG.rst`` for the new version
- Create the git tag with ``make git-create-tag``
- Push the changes using ``make git-push-tag``
- Run the manual ``xray-report`` job in the CI pipeline created by the
  push

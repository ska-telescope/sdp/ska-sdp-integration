Science Data Processor
======================

The SKA Science Data Processor (SDP) is a sub-system common to the Low and Mid
telescopes used for producing Science Data Products from observations.

What the SDP can do now:

- Ingest visibility data from the Correlator Beamformer and write
  Measurement Set data products.
- Provide real-time signal displays and statistics.
- Provide pointing calibration solutions for the Mid telescope

What the SDP will be able to do soon:

- Provide real-time calibration solutions to support the operation of the
  telescope (beamformer gain calibration)

The `SDP Integration repository
<https://gitlab.com/ska-telescope/sdp/ska-sdp-integration>`_ brings together
the components of the SDP. It contains the Helm chart to deploy the SDP as a
sub-system of the telescope, or stand-alone for testing.

.. toctree::
  :maxdepth: 1
  :caption: Releases

  releases/changelog

.. toctree::
  :maxdepth: 1
  :caption: Design

  design/overview
  design/components
  design/modules
  design/data-processing

.. toctree::
  :maxdepth: 1
  :caption: Installation

  installation/requirements
  installation/standalone
  installation/remote-cluster
  installation/minikube
  installation/helm-chart

.. toctree::
  :maxdepth: 1
  :caption: Operation

  operation/interacting-with-sdp
  operation/jupyter-notebooks
  operation/download-output-data
  operation/version-information.rst
  operation/tango-attribute-archiving.rst
  operation/pointing-calibration.rst

.. toctree::
  :maxdepth: 1
  :caption: Development

  development/general
  development/using-make
  development/dev-sdp
  development/dev-script
  development/testing
  development/release-process.rst

.. toctree::
  :maxdepth: 1
  :caption: Help

  help/troubleshooting
  help/frequently-asked-questions

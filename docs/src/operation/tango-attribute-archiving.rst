.. _attribute_archiving:

Tango attribute archiving
=========================

In order to be able to inspect the history of key Tango attributes exposed in 
the SDP control system, the Engineering Data Archive (EDA) needs to be deployed 
and configured.

The SDP subarray and controller device attributes are explicitly written to PUSH 
events at appropriate points. These can be `Change` and/or `Archive` events, and 
the EDA acts as a client, subscribing to the Archive events. The attribute value 
in the archive is updated when a new event is detected - i.e. whenever the attribute 
value changes.

The EDA must be configured for the SDP, and this can be achieved using the `template
configuration file <https://gitlab.com/ska-telescope/sdp/ska-sdp-integration/-/raw/master/charts/ska-sdp/data/sdp-eda-config.yaml?ref_type=heads>`_.

After updating the configuration file as described below, it should be used with 
the EDA Configurator Tool as described in the `EDA User Guide <https://confluence.skatelescope.org/display/UD/Attribute+Configuration+for+archiving>`_.

The first three lines of the configuration file must be updated:

.. code-block:: python

    db: <tango-host>.<namespace>.svc.<cluster-domain>:<port>
    manager: <prefix>-eda/cm/01
    archiver: <prefix>-eda/es/01

The first line gives the IP address of the Tango databaseds where the 
Configuration Manager and Event Subscriber are present and the second
and third lines give their device names. Update these lines with the correct

    - Tango host
    - Namespace
    - Cluster domain
    - Port number
    - Device name prefix

The `prefix` should be set to `low` or `mid` as appropriate.

Note that even though it is not strictly necessary to use all the components
of the domain name if you are inside the cluster, or inside the same
namespace, the underlying tool works more effectively if the full domain name
is given every time.

The rest of the configuration file specifies how each attribute should be archived,
and in this case, the archive update is always performed when a push event
is received.

.. code-block:: python

    code_push_event: true
    archive_strategy: ALWAYS





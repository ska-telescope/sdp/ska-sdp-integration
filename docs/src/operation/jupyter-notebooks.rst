.. _jupyter:

SDP Jupyter notebooks
=====================

This page describes how to use the SDP Jupyter notebooks.

.. note::

    You will need to run the Jupyter notebooks on the same cluster
    where you deployed the SDP sub-system.

How Binderhub and Jupyter notebooks work
----------------------------------------

If this is your first time using Jupyter notebooks have a look at this link for `a crash course on Jupyter <https://medium.com/velotio-perspectives/the-ultimate-beginners-guide-to-jupyter-notebooks-6b00846ed2af>`_.
For Binderhub please look at the :external+ska-devportal:doc:`this documentation on Binderhub <tools/binderhub>`.

Which Jupyter notebooks to use and what the notebooks do
--------------------------------------------------------

The :external+ska-sdp-notebooks:doc:`Jupyter notebooks <index>` can be found at `SKA SDP Notebooks <https://gitlab.com/ska-telescope/sdp/ska-sdp-notebooks>`_. There are several notebooks that are used to show different functionalities of the SDP. Each notebook has a detailed explanation of what it does overall and what each of the steps do as well as their expected outcome.

How to use Jupyter notebooks
----------------------------

You need the namespace in which the controllers of the SDP are deployed.

In all the Jupyter Notebooks replace the namespace should be defined in one of the first steps. You need to change the text value of the namespace to your namespace Save the notebook. You can now run through the notebook steps.

.. code:: python

    namespace = "my-namespace"

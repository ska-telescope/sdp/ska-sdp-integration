Version information
===================

The LMC controller device is able to give the version information of the
currently running SDP system. This information includes the component and
sub-chart version information.

The version information is returned as a string. It shows only the enabled
components and sub-charts. If a new version of the components is deployed, or a
new version of the SDP is deployed, then this version information will update.

Usage
-----

To get the version information you need to access the controller device via the
Tango interface.

For example using ITango:

.. code-block:: python

    In [1]: import json

.. code-block:: python

    In [2]: controller = Device("test-sdp/control/0")

.. code-block:: python

    In [3]: json.loads(controller.sdpVersion)
    Out[3]:
        {'version': '0.17.0',
         'components': [{'name': 'etcd',
           'image': 'artefact.skao.int/ska-sdp-etcd',
           'version': '3.5.9'},
          {'name': 'lmc',
           'image': 'artefact.skao.int/ska-sdp-lmc',
           'version': '0.23.1'},
          {'name': 'lmc-queueconnector',
           'image': 'artefact.skao.int/ska-sdp-lmc-queue-connector',
           'version': '1.2.0'},
          {'name': 'dsconfig',
           'image': 'artefact.skao.int/ska-tango-images-tango-dsconfig',
           'version': '1.5.9'},
          {'name': 'proccontrol',
           'image': 'artefact.skao.int/ska-sdp-proccontrol',
           'version': '0.12.1'},
          {'name': 'helmdeploy',
           'image': 'artefact.skao.int/ska-sdp-helmdeploy',
           'version': '0.14.0'},
          {'name': 'console',
           'image': 'artefact.skao.int/ska-sdp-console',
           'version': '0.5.1'}],
         'dependencies': [{'name': 'ska-sdp-qa',
           'repository': 'https://artefact.skao.int/repository/helm-internal',
           'version': '0.19.1'},
          {'name': 'ska-tango-base',
           'repository': 'https://artefact.skao.int/repository/helm-internal',
           'version': '0.4.6'}]}

.. _dashboard_display:

SDP Dashboards and Displays
===========================

Here, you will find information on how you can connect to and use various
dashboards and displays that are related to the SDP.

Requirements
------------

The Taranta dashboard and the Signal Display browser links require
the hostname of the cluster ingress.

If you deployed your instance of the SDP on a cluster, your ``<ingress-host>`` should be
the cluster's address.

    - For the `DP Cluster`_: https://sdhp.stfc.skao.int/
    - For the `TechOps Cluster`_: https://k8s.stfc.skao.int/

Otherwise you will need the Minikube IP address. To get the Minikube IP address you need
to run the following command:

.. code-block:: console

    $ minikube ip

.. _access_taranta_dashboard:

Monitoring SDP via the Taranta dashboard
----------------------------------------

By default, the ``ska-sdp`` chart does not deploy the Taranta dashboard. To enable it,
follow the instructions at :ref:`compl_interf` and upgrade your SDP deployment if needed.

After the upgrade you can access the Taranta dashboard in the
control namespace at this URL:

.. code-block:: console

    http://<ingress-host>/<control-namespace>/taranta/

Next, log in to the Taranta instance.
For information on usernames and passwords refer to this
:external+ska-tango-taranta-suite:doc:`page <taranta_users>`.

To use the dashboard, upload
`this file <https://gitlab.com/ska-telescope/sdp/ska-sdp-integration/-/raw/master/charts/ska-sdp/data/sdp-taranta-dashboard.wj?ref_type=heads>`_,
which can be used to monitor the status of the SDP.

Next, press the play button. This should connect the dashboard to the relevant subarrays.
Once connected, you will see the status of the subarrays. If you select a specific subarray
from the drop down list, it will list the statistics for that subarray.

More information on Taranta and its usage can be found under the :external+webjive:doc:`Taranta documentation <index>`.


Monitor the status of the visibility data with the Signal Displays
------------------------------------------------------------------

The Signal Displays are deployed with SDP by default
Once the SDP is running you can access them by going to this URL:

.. code-block:: console

    http://<ingress-host>/<control-namespace>/signal/display/


More information on the Signal Displays can be found :external+ska-sdp-qa-data-api:doc:`here <display/overview>`.

What data are displayed?
^^^^^^^^^^^^^^^^^^^^^^^^

    - Statistics about the data
    - RFI Graph
    - Spectrum Graphs
    - Phase vs Amplitude Graphs
    - Phase vs Frequencies Graphs
    - Spectrogram Waterfall Plots

Using the interactive legends
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

For some of the graphs there is an interactive legend.

Upon initial receipt of the data, the data is parsed and a list is created.
This list is displayed upon the screen as a list of buttons.

When the list of buttons is displayed, they are coloured to match the associated
data in the charts. When a button is clicked, the colouring is suppressed and the
associated data is suppressed from all of the associated charts. Clicking the button
again will cause the colouring suppression to stop and the data will again pass to the charts.

Note that this legend affects the data before the charting is rendered, which allows for the
charting library to be independent of this functionality.

.. _monitoring_sdp_namespace:

Monitoring SDP Namespace
------------------------

The cluster provides 2 types of dashboards that can be used to monitor the namespace.

These dashboards are based on Grafana dashboards and Kibana logs.

Links and documentation on how to use Grafana and Kibana can be found under the
:external+ska-devportal:doc:`Monitoring and Logging documentation <tools/centralised-monitoring-and-logging>`.

Grafana
^^^^^^^

This service is provided by the cluster and can be accessed via
`Kubernetes Resource Dashboard <https://monitoring.skao.int/d/dad0e09f-32f5-4181-b273-c7a1017221ff/kubernetes-resource-monitoring-dashboard?orgId=1&refresh=5s>`_.

From that link you can specify Cluster, and the Namespace.
You are able to view both the deployment namespace, as well as the processing namespace.

Kibana Logs
^^^^^^^^^^^

This service is provided by the cluster and can be accessed via
`Kibana Logs <https://k8s.stfc.skao.int/kibana/app/discover#/?_g=(filters:!(),refreshInterval:(pause:!t,value:0),time:(from:now-15m,to:now))&_a=(columns:!(kubernetes.namespace,kubernetes.pod.name,kubernetes.container.name,message),filters:!(),grid:(columns:(kubernetes.container.name:(width:(width:120)),kubernetes.namespace:(width:(width:120)),kubernetes.pod.name:(width:(width:150)))),index:cbb05bec-ed81-45f8-b11a-eab26a3df6b1,interval:auto,query:(language:kuery,query:'kubernetes.namespace:%20%22dp-shared%22%20or%20kubernetes.namespace:%20%22dp-shared-p%22'),sort:!(!('@timestamp',desc)))>`_.

From that link you will need to updated the 2 namespaces as required.
It is also possible to filter by processing block by changing the query to be

.. code-block::

    kubernetes.namespace: "dp-shared" or (kubernetes.namespace : "dp-shared-p" and kubernetes.pod.name : proc-pb-test-20231016-47797*)

changing the processing block ID section, and the namespaces as required.

.. _DP Cluster: https://confluence.skatelescope.org/display/SWSI/Data+Processing+Testing+Platform
.. _TechOps Cluster: https://confluence.skatelescope.org/display/SWSI/STFC+Cloud+Deployment+and+Operations

.. _interacting_sdp:

Interacting with the SDP
========================

On this page, we describe a few methods which you can explore to interact with SDP,
including executing a scan and accessing data. Below, we assume that one uses the
command line (a terminal window) to execute commands and to start ITango. To do the
same from a Jupyter notebook, look at the documentation :ref:`here <jupyter>`.

To get a stand-alone instance of the SDP running please refer to :ref:`installing_standalone`.

There are two main paths that one can take to work with SDP:

    - use ITango to run commands on an SDP subarray device
    - use the SDP console to manipulate the SDP Configuration Database.
      This is not recommended for non-developers.

The above pages describe simple scenarios, when test processing scripts are used,
i.e. we do not execute the vis-receive script and do not send and receive data.
To understand how to use the vis-receive script, we recommend that you
follow the instructions in the `example Jupyter notebook for vis-receive`_.

Similarly, if you are interested in running the pointing offset calibration pipeline,
follow the instructions in the `pointing calibration Jupyter notebook`_

In addition, various dashboards and displays can be used to inspect
SDP data products and metrics.

.. toctree::
  :maxdepth: 1
  :caption: Contents

  itango
  console
  dashboards-displays


.. _example Jupyter notebook for vis-receive: https://gitlab.com/ska-telescope/sdp/ska-sdp-notebooks/-/blob/main/src/ska-sdp-vis-receive-low-example.ipynb?ref_type=heads
.. _pointing calibration Jupyter notebook: https://gitlab.com/ska-telescope/sdp/ska-sdp-notebooks/-/blob/main/src/ska-sdp-pointing-script.ipynb?ref_type=heads

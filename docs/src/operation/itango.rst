
.. _access_tango_interface:

Accessing the Tango interface
=============================

In the SKA system, SDP is controlled via tango devices, the controller, and various
subarray devices. The device commands and state transitions are described
in their own documentation pages:

    - :external+ska-sdp-lmc:doc:`Controller device <sdp-controller>`
    - :external+ska-sdp-lmc:doc:`Subarray device <sdp-subarray>`

Most of the commands running on the subarray device require a "configuration string".
The schemas for such strings is described in the
:external+ska-telmodel:doc:`Telescope Model library <schemas/sdp/ska-sdp>`.

By default, the ``ska-sdp`` chart does not deploy the ITango shell pod from the
``ska-tango-base`` chart. Make sure you install/upgrade the chart to have ITango enabled.
(See instructions at :ref:`compl_interf`.)

Start an ITango session with:

.. code-block:: console

    $ kubectl exec -it ska-tango-base-itango-console -n <namespace> -- itango3

List the Tango devices that are currently in the Tango DB:

.. code-block:: python

    In [1]: lsdev
    Device                                   Alias                     Server                    Class
    ---------------------------------------- ------------------------- ------------------------- --------------------
    sys/access_control/1                                               TangoAccessControl/1      TangoAccessControl
    sys/database/2                                                     DataBaseds/2              DataBase
    sys/rest/0                                                         TangoRestServer/rest      TangoRestServer
    sys/tg_test/1                                                      TangoTest/test            TangoTest
    test-sdp/control/0                                                 SDPController/0           SDPController
    test-sdp/queueconnector/01                                         SDPQueueConnector/01      SDPQueueConnector
    test-sdp/subarray/01                                               SDPSubarray/01            SDPSubarray

Next, connect to the subarray device, turn it On, and check its obsState.

.. code-block:: python

    In [2]: d = DeviceProxy('test-sdp/subarray/01')

.. code-block:: python

    In [3]: d.state()
    Out[3]: tango._tango.DevState.OFF

.. code-block:: python

    In [4]: d.On()

.. code-block:: python

    In [5]: d.state()
    Out[5]: tango._tango.DevState.ON

.. code-block:: python

    In [6]: d.obsState
    Out[6]: <obsState.EMPTY: 0>

Note: if you are trying to execute a command that is not allowed in a given obsState, the
subarray will report a tango error and stays in the obsState where it originally was
at the time of command execution.

To start processing on SDP, you will need a configuration string, which provides the set up, the request for
resources, and the request of what processing script to run. You can find an example string in the `Tango Jupyter
notebook <https://gitlab.com/ska-telescope/sdp/ska-sdp-notebooks/-/blob/main/src/ska-sdp-tango-tutorial.ipynb>`_ (the
configuration string can be found `here
<https://gitlab.com/ska-telescope/sdp/ska-sdp-notebooks/-/blob/main/src/ska-sdp-tango-tutorial.ipynb?plain=1#L228>`_,
we recommend that you copy it from the display-version of the notebook and not the raw file).

Save the copied configuration string (which is provided as JSON in the example notebook) as
a python string. Make sure you update the execution block and processing block IDs
(if you run this multiple times, you will need to increment the number at the end):

.. code-block:: python

    import json
    EXECUTION_BLOCK_ID = f"eb-test-20221012-00001"
    PROCESSING_BLOCK_ID_REALTIME = f"pb-testrealtime-20221012-00001"
    PROCESSING_BLOCK_ID_BATCH = f"pb-testbatch-20221012-00001"

    config = json.dumps(<copied-json-string>)

Below, we provide the steps from assigning resources to the processing,
through configure and scan, to releasing the resources and ending the process.

Once the AssignResources command is executed, you will see two processing blocks deployed
in the ``<processing-namespace>``. First the realtime script will run.

.. code-block:: python

    In [8]: d.AssignResources(config)

.. code-block:: python

    In [9]: d.obsState
    Out[9]: <obsState.IDLE: 0>

.. code-block:: python

    In [10]: d.Configure('{"interface": "https://schema.skao.int/ska-sdp-configure/1.0", "scan_type": "target:a"}')

.. code-block:: python

    In [11]: d.obsState
    Out[11]: <obsState.READY: 2>

.. code-block:: python

    In [12]: d.Scan('{"interface": "https://schema.skao.int/ska-sdp-scan/1.0", "scan_id": 1}')

.. code-block:: python

    In [13]: d.obsState
    Out[13]: <obsState.SCANNING: 3>

.. code-block:: python

    In [14]: d.EndScan()

.. code-block:: python

    In [15]: d.obsState
    Out[15]: <obsState.READY: 2>

.. code-block:: python

    In [16]: d.End()

.. code-block:: python

    In [17]: d.obsState
    Out[17]: <obsState.IDLE: 0>

Executing ``d.End()`` will trigger the batch processing block (a dask cluster),
which will automatically terminate once it's finished the processing assigned to it.

To finish up, run:

.. code-block:: python

    In [18]: d.ReleaseResources('{ "interface": "https://schema.skao.int/ska-sdp-releaseres/1.0", '
                   '"resources": {"receptors": ["SKA001", "SKA002", "SKA003", "SKA004"]}}')

.. code-block:: python

    In [19]: d.obsState
    Out[19]: <obsState.EMPTY: 0>

.. code-block:: python

    In [20]: d.Off()

.. code-block:: python

    In [21]: d.state()
    Out[21]: tango._tango.DevState.OFF

ReleaseResources takes the list of resources ("receptors") to be released. If you
want to release all of them at once, you may use ``d.ReleaseAllResources()``.

If at any time the obsState becomes FAULT, there has been an error somewhere in the steps.
Restart the subarray by running the ``d.Restart()`` command, which will take it back to
the ``EMPTY`` ``ObsState``. The ``Off()`` command can also be used. Then start again.

Information and meanings on the commands and attributes provided by
Tango can be found in the :external+ska-sdp-lmc:doc:`LMC Documentation <sdp-subarray>`.

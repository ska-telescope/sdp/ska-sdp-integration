.. _pointing:

SDP Pointing Calibration
========================

Pointing calibration must be performed for the MID telescope. This is achieved 
by observing "pointing"-type scans with the telescope - for example in a 5-point pattern 
around a pointing calibrator source.

These scans are processed by SDP using the 
:external+ska-sdp-wflow-pointing-offset:doc:`pointing offset pipeline <index>`,
which fits the beam of each dish to the measured data and returns the offsets (in cross-elevation and elevation) and their uncertainties.

Deployment of the pointing pipeline
-----------------------------------

The pointing offset calibration pipeline can be deployed either as a stand-alone pipeline or as an SDP processing script alongside the
`vis-receive script <https://developer.skatelescope.org/projects/ska-sdp-script/en/latest/scripts/vis-receive.html>`_.

These two methods are described in detail in the pointing documentation:

- :external+ska-sdp-wflow-pointing-offset:doc:`Pointing offset pipeline command line interface <api/pointing_offset_cli>`
- :external+ska-sdp-script:doc:`Pointing offset script <scripts/pointing-offset>`

The functionality that is only used when the pipeline is deployed in SDP is described
`here <https://developer.skao.int/projects/ska-sdp-wflow-pointing-offset/en/latest/sdp.html#running-the-pipeline-in-sdp>`_.

In addition, there is a 
`pointing calibration notebook <https://gitlab.com/ska-telescope/sdp/ska-sdp-notebooks/-/blob/main/src/ska-sdp-pointing-script.ipynb>`_
in the :ref:`jupyter`, which demonstrates the pointing offset script deployment using test data.


Configuring the pointing pipeline via the pointing script
---------------------------------------------------------

Configuration of the pointing offset script is described in the 
`script documentation <https://developer.skatelescope.org/projects/ska-sdp-script/en/latest/scripts/pointing-offset.html#configuring-the-script>`_.


Real-time operation
-------------------

During real-time operations, the following steps take place:

- AssignResources - configure and start a processing block for pointing-offset and vis-receive scripts
- Configure - configure a pointing observation, triggering the pointing pipeline to wait for expected number of scans to complete
- Scans - execute pointing scans, and start pointing pipeline when expected number of scans have completed

Once complete, the pointing pipeline publishes the resulting offsets for each dish to a Kafka topic. 
The QueueConnector device monitors the Kafka topic, and saves the offsets to tango attributes. Each dish has its own 
tango attribute containing the ID of the last scan of the given pointing observation and the offsets, given in degrees 
for cross-elevation (xel) and elevation (el).

In addition, the output data are written to an 
`HDF5 output file <https://developer.skao.int/projects/ska-sdp-wflow-pointing-offset/en/latest/data_product.html>`_.


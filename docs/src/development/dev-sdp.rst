.. _dev-sdp:

Developing the SDP Helm chart
=============================

Installing a local version of SDP
---------------------------------

The simplest way of installing SDP from a branch of the
ska-sdp-integration repository, is via make targets.
Please refer to the :ref:`using-make` section for details on
how you can set up SDP for local development using Make.

Testing a component locally
---------------------------

If you are developing one of the components of SDP, you may
want to check that your changes work in the integrated system.

Assuming you are using Minikube for the development, first
build a local version of the OCI image of the component with
your changes. If you are using a remote server, you can use
an OCI image from GitLab, which is built on every commit and
is saved in the repository's container registry (this will require
GitLab access).

In the following example, we pretend to have updated the SDP
Processing Controller and now we want to test it as part of SDP.
Once you have built the image, create a file called ``test.yaml``
containing the following:

.. code-block::

    proccontrol:
      image: <my-newly-developed-proccontrol-image>
      version: <version-of-new-image>
      imagePullPolicy: IfNotPresent

Update ``image`` and ``version`` accordingly. You may add other values
that are required for your testing. This file will be used to upgrade your
SDP deployment:

.. code-block:: console

    $ make install-sdp HELM_VALUES=test.yaml

You should see now, via kubectl or k9s, that the processing controller is
being replaced with the version you have built.

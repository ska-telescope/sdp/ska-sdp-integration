Developing SDP processing scripts
=================================

Instructions on how to develop and test SDP processing scripts can be found in
the :external+ska-sdp-script:doc:`Processing scripts <script-development>`
documentation.

# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = "Science Data Processor"
copyright = "2019-2025 SKA SDP Developers"
author = "SKA SDP Developers"
version = "1.0.0"
release = "1.0.0"

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "sphinx.ext.intersphinx",
    "sphinx_copybutton",
    "sphinx_new_tab_link",
]

exclude_patterns = []

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "ska_ser_sphinx_theme"

# -- Extension configuration -------------------------------------------------

# Copybutton configuration
copybutton_exclude = ".linenos, .go, .gp"
copybutton_prompt_text = "In \[\d*\]: "
copybutton_prompt_is_regexp = True
copybutton_copy_empty_lines = False

# Intersphinx configuration
intersphinx_mapping = {
    "pytango": ("https://pytango.readthedocs.io/en/stable", None),
    "ska-cicd-deploy-minikube": ("https://developer.skao.int/projects/ska-cicd-deploy-minikube/en/latest/", None),
    "ska-dataproduct-dashboard": ("https://developer.skao.int/projects/ska-dataproduct-dashboard/en/latest/", None),
    "ska-devportal": ("https://developer.skao.int/en/latest/", None),
    "ska-sdp-config": ("https://developer.skao.int/projects/ska-sdp-config/en/latest/", None),
    "ska-sdp-helmdeploy-charts": ("https://developer.skao.int/projects/ska-sdp-helmdeploy-charts/en/latest/", None),
    "ska-sdp-helmdeploy": ("https://developer.skao.int/projects/ska-sdp-helmdeploy/en/latest/", None),
    "ska-sdp-slurmdeploy": ("https://developer.skao.int/projects/ska-sdp-slurmdeploy/en/latest/", None),
    "ska-sdp-lmc": ("https://developer.skao.int/projects/ska-sdp-lmc/en/latest/", None),
    "ska-sdp-notebooks": ("https://developer.skao.int/projects/ska-sdp-notebooks/en/latest/", None),
    "ska-sdp-proccontrol": ("https://developer.skao.int/projects/ska-sdp-proccontrol/en/latest/", None),
    "ska-sdp-qa-display": ("https://developer.skao.int/projects/ska-sdp-qa-display/en/latest/", None),
    "ska-sdp-qa-data-api": ("https://developer.skao.int/projects/ska-sdp-qa-data-api/en/latest/", None),
    "ska-sdp-lmc-queue-connector": ("https://developer.skao.int/projects/ska-sdp-lmc-queue-connector/en/latest/", None),
    "ska-sdp-script": ("https://developer.skao.int/projects/ska-sdp-script/en/latest/", None),
    "ska-sdp-realtime-receive-modules": ("https://developer.skao.int/projects/ska-sdp-realtime-receive-modules/en/latest/", None),
    "ska-sdp-realtime-calibration": ("https://developer.skao.int/projects/ska-sdp-realtime-calibration/en/latest/", None),
    "ska-sdp-wflow-pointing-offset": ("https://developer.skao.int/projects/ska-sdp-wflow-pointing-offset/en/latest", None),
    "ska-sdp-wflow-selfcal": ("https://developer.skao.int/projects/ska-sdp-wflow-selfcal/en/latest/", None),
    "ska-sdp-distributed-self-cal-prototype": ("https://developer.skao.int/projects/ska-sdp-distributed-self-cal-prototype/en/latest/", None),
    "ska-sdp-continuum-imaging-pipeline": ("https://developer.skao.int/projects/ska-sdp-continuum-imaging-pipeline/en/latest/", None),
    "ska-sdp-spectral-line-imaging": ("https://developer.skao.int/projects/ska-sdp-spectral-line-imaging/en/latest/", None),
    "ska-sdp-batch-preprocess": ("https://developer.skao.int/projects/ska-sdp-batch-preprocess/en/latest/", None),
    "ska-tango-taranta-suite": ("https://developer.skao.int/projects/ska-tango-taranta-suite/en/latest/", None),
    "ska-tango-operator": ("https://developer.skao.int/projects/ska-tango-operator/en/latest/", None),
    "ska-tango-tangogql": ("https://developer.skao.int/projects/ska-tango-tangogql/en/latest", None),
    "ska-telmodel": ("https://developer.skao.int/projects/ska-telmodel/en/latest/", None),
    "webjive": ("https://developer.skao.int/projects/webjive/en/latest/", None),
}

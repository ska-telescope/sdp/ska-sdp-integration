.. _faq:

Frequently Asked Questions (FAQ)
================================

Here answers to common scenarios and problems can be found.

How to manage the SDP deployment on a standard platform?
--------------------------------------------------------

For installing the SDP, look at :ref:`installing_standalone` or :ref:`remote_cluster`.


How to interrogate the logs for SDP when its being used?
--------------------------------------------------------

You can find the logs in either Kibana or Grafana. More information on these can be found at :ref:`monitoring_sdp_namespace`. You can also monitor the logs via K9s. More information on how to use K9s can be found at this `website <https://www.civo.com/learn/k9s-the-tool-that-can-increase-your-productivity-with-kubernetes>`_.


How to interrogate the status of SDP?
---------------------------------------

How to query the Tango attributes can be found in :ref:`this documentation <interacting_sdp>`.


What pipelines can be run?
--------------------------

Information on what pipelines can be run are found at :ref:`use_console`.


How to find out what configuration options (processing block parameters) are available for the a given pipeline?
----------------------------------------------------------------------------------------------------------------

Information on the configuration options can be found in the :external+ska-sdp-script:doc:`Scripts documentation <scripts>`.


How do a start, or abort/cancel a pipeline?
--------------------------------------------

Information on subarray commands can be found :external+ska-sdp-lmc:doc:`here <sdp-subarray>`.


How do I know when a pipeline is complete and how do I get the data product from it?
------------------------------------------------------------------------------------

When a scan is complete or has been ended, the data product can be retrieved from either the Data Product Dashboard or directly from a pod within the SDP installation. More details on this can be found at :ref:`data_product_download`.

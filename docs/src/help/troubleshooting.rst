.. _troubleshooting:

Troubleshooting
===============

Obtaining test data with git LFS
--------------------------------

The test Measurement Set in the ``ska-sdp-integration`` repository is linked
to its data files using Git Large File Storage (Git LFS) and, without the
correct steps, the visibility receive test will fail with Pod failures and
errors in the Pod logs similar to the following

.. code-block:: console

    Traceback (most recent call last):
       File "/usr/local/lib/python3.7/dist-packages/cbf_sdp/utils.py", line 100, in __init__
       myMS = msutils.MeasurementSet.open(ms)
       File "/usr/local/lib/python3.7/dist-packages/cbf_sdp/msutils.py", line 297, in open
       ms._open()
       File "/usr/local/lib/python3.7/dist-packages/cbf_sdp/msutils.py", line 140, in _open
       self._t = tables.table(self._name, readonly=True, ack=False)
       File "/usr/local/lib/python3.7/dist-packages/casacore/tables/table.py", line 373, in __init__
       Table.__init__(self, tabname, lockopt, opt)
       RuntimeError: Table /mnt/data/AA05LOW.ms does not exist

To correct this problem use the following commands:

.. code-block:: console

    $ sudo apt-get install git-lfs
    $ git lfs pull

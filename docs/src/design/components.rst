.. _design_components:

Components
==========

.. figure:: ../images/sdp_components.svg
  :align: center

  SDP components and the connections between them.

The diagram shows the components of the SDP system and the connections between
them. The components, grouped by their function, are as follows.

**Execution Control**:

- The :external+ska-sdp-config:doc:`Configuration Database <index>`
  is the central store of configuration
  information in the SDP. It is the means by which the components communicate
  with each other and it is implemented using `etcd <https://etcd.io/>`_.

- The :external+ska-sdp-lmc:doc:`Controller Tango device <sdp-controller>`
  is intended to provide the top-level control of SDP services.
  Presently, it performs the state transitions in response to commands,
  reports the status and versions of the SDP system and its components  and
  updates the ``healthState`` based on components' status.

- The :external+ska-sdp-lmc:doc:`Subarray Tango devices <sdp-subarray>`
  control the processing associated with the SKA subarrays. They are
  the primary interface between the SKA software ecosystem and the SDP
  sub-system.

- The :external+ska-sdp-lmc-queue-connector:doc:`Queue Connector Tango devices <index>` provide
  the interface for exchanging real-time data with other telescope subsystems over Tango.
  Within the SDP, the data travel over the data queues to and from processing deployments.

- The :external+ska-devportal:ref:`Console <projects/area/sdp/execution-control:console>`
  provides a command-line interface to the configuration
  database to control and monitor the system. This is mainly a developer interface.

- The :external+ska-sdp-proccontrol:doc:`Processing Controller <index>`
  controls the execution of processing blocks. It
  detects them by monitoring the configuration database. To execute a
  processing block, it requests the deployment of the corresponding script by
  creating an entry in the configuration database.

**Quality Assessment (QA)**:

- The :external+ska-sdp-qa-display:doc:`Display Server <index>`
  serves the client with the HTML and JavaScript required to display QA
  data provided by the API server.

- The :external+ska-sdp-qa-data-api:doc:`API Server <index>`
  reads QA data from the data queues and serves them to the client to display.
  Information and statistics regarding the processing blocks are available at
  REST endpoints, and data for visualisation is provided by WebSocket endpoints.


**Data Queues**:

- The Data Queues are the means by which components of the system exchange
  data in real time. They are implemented using `Kafka <https://kafka.apache.org/>`_.

**Platform**:

- The :external+ska-sdp-helmdeploy:doc:`Helm Deployer <index>`
  is the service that the platform uses to respond to
  deployment requests in the configuration database. It makes deployments by
  installing :external+ska-sdp-helmdeploy-charts:doc:`Helm charts <index>`
  (a collection of files that describe a set of
  Kubernetes resources) into a Kubernetes cluster.

- `Kubernetes <https://kubernetes.io/>`_ is the underlying mechanism for making
  dynamic deployments to process data.

- The :external+ska-sdp-slurmdeploy:doc:`Slurm Deployer <index>` allows batch jobs to be 
  submitted to a Slurm cluster. It monitors the Configuration Database for deployment 
  entries that request a slurm script to be executed. When a Slurm deployment entry is 
  added, the Slurm Deployer submits the corresponding job to the Slurm cluster via the 
  REST api and monitors its state.

- To enable the slurm deployer and access to secrets, set ``enabled=true`` in the SDP Helm 
  chart's values for ``slurmDeploy`` and ``vaultStaticSecret``. The Vault Static Secret 
  is used to access the authentication JWT stored in the Vault.

**Processing Block Deployment**:

- A :external+ska-sdp-script:doc:`Processing Script <index>`
  orchestrates the execution of a processing block.
  It connects to the configuration database to retrieve the
  parameters of the processing block and to request the creation of
  processing deployments.

- A Processing Deployment is created by the script to process data. It can read
  and write data products in the data product store. It may also exchange data
  in real time with other components of the system via the data queues, including
  producing data for the QA display. Specialised deployments are used to receive
  visibility data from the Correlator Beamformer.

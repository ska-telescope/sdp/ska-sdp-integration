.. _design_overview:

Overview
========

This section gives an overview of the current design of the SDP system,
which follows the SDP software architecture as described in `Solution Intent
<https://confluence.skatelescope.org/display/SWSI/Solution+Intent+Home>`_ in
the SKA Community Confluence (wiki). The Solution Intent contains other
information about the intended behaviour of the system, such as the high-level
requirements. It also captures the behaviour of the system as currently
implemented, including the outcome of tests against the requirements.

The components of the SDP system and the relationships between them are
described in the :ref:`design_components` section. The software modules used to
build the components and deploy the SDP are described in the
:ref:`design_modules` section.

.. _minikube_doc:

Installing and developing in Minikube
=====================================

If you do not have access to a remote cluster, you might want to consider
using Minikube for working with the SDP. Note, that you may run into
resource problems, especially if you deploy the SDP with all of its components,
if you do not have a powerful enough machine, or enough CPU/memory allocated
to Minikube before you install the SDP.

For installing and using Minikube, follow the instructions in
:external+ska-cicd-deploy-minikube:doc:`SKA Minikube documentation <README>`
which will setup a Minikube cluster that resembles a standard SKA cluster.

In this page, we only mention a few, SDP-specific details, but we do not
provide a How-To for Minikube. In general, what is described in :ref:`installing_standalone`
is how you would want to install the SDP in Minikube as well, unless clearly
marked otherwise.

By default, the SDP Helm chart uses the :external+ska-tango-operator:doc:`Tango
operator <index>` to deploy the LMC components. The operator is not available
in a Minikube cluster, so the use of the operator must be disabled by setting
``global.operator`` to ``false``.

Using Docker images
-------------------

If you are developing SDP components and you would like to build and test them
in Minikube, you need to configure Docker to use the daemon inside the Virtual Machine.
This can be done by running the following command:

.. code-block:: console

    $ eval $(minikube docker-env)

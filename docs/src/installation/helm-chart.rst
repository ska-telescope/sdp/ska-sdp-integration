Helm chart
==========

This is a summary of the Helm chart parameters that can be used to customise
the SDP deployment. The current default values can be found in the chart's
`values file`_.

Global
------

These are settings that affect the entire deployment, including sub-charts.

.. list-table::
  :widths: auto
  :header-rows: 1

  * - Parameter
    - Description
    - Default
  * - ``global.sdp.processingNamespace``
    - Namespace for SDP dynamic deployments
    - ``sdp``
  * - ``global.data-product-pvc-name``
    - Name of data product PVC that is set as a global value to allow subcharts to access the same PVC
    - ``test-pvc``
  * - ``global.tango_host``
    - Host address and port for the Tango Database
    - ``databaseds-tango-base:10000``
  * - ``global.operator``
    - Use :external+ska-tango-operator:doc:`Tango operator <index>` to deploy LMC devices
    - ``true``

Tango infrastructure
--------------------

Parameters for the ska-tango-base subchart and Tango dsconfig. The
ska-tango-base subchart must be enabled to support the Tango devices when
running the SDP stand-alone.

.. list-table::
  :widths: auto
  :header-rows: 1

  * - Parameter
    - Description
    - Default
  * - ``ska-tango-base.enabled``
    - Enable the ska-tango-base subchart
    - ``true``
  * - ``ska-tango-base.itango.enabled``
    - Enable the ITango console in the ska-tango-base subchart
    - ``false``
  * - ``dsconfig.image.*``
    - Tango dsconfig container image settings
    - See `values file`_

SDP QA
------

Sub-chart for the `SDP QA data API`_

.. list-table::
  :widths: auto
  :header-rows: 1

  * - Parameter
    - Description
    - Default
  * - ``ska-sdp-qa.enabled``
    - Enable the QA data API
    - ``true``

Taranta and TangoGQL
--------------------

These two sub-charts need to be enabled together if one
wants to access the Taranta Dashboard for SDP

.. list-table::
  :widths: auto
  :header-rows: 1

  * - Parameter
    - Description
    - Default
  * - ``ska-tango-taranta.enabled``
    - Enable the Taranta Dashboard
    - ``false``
  * - ``ska-tango-tangogql.enabled``
    - Enable TangoGQL
    - ``false``

Kafka
-----

When enabled, `Apache Kafka`_ is installed.

.. list-table::
  :widths: auto
  :header-rows: 1

  * - Parameter
    - Description
    - Default
  * - ``kafka.enabled``
    - Enable the Kafka deployment
    - ``true``
  * - ``kafka.maxMessageBytes``
    - Maximum size of message in Bytes
    - ``"524288000"``
  * - ``kafka.socketRequestMaxBytes``
    - Maximum size of a request in Bytes that the socket server will accept
    - ``"524288000"``
  * - ``kafka.resources``
    - Resource requests and limits for Kafka
    - See `values file`_
  * - ``kafka.persistence.enabled``
    - Persist Kafka data to storage
    - ``false``
  * - ``kafka.zookeeper``
    - Zookeeper settings
    - See `values file`_
  * - ``kafka.clusterDomain``
    - The cluster's domain
    - ``"cluster.local"``

Configuration database
----------------------

The configuration database is implemented on top of `etcd`_.

.. list-table::
  :widths: auto
  :header-rows: 1

  * - Parameter
    - Description
    - Default
  * - ``etcd.image``
    - etcd container image
    - ``artefact.skao.int/ska-sdp-etcd``
  * - ``etcd.version``
    - etcd container version
    - See `values file`_
  * - ``etcd.imagePullPolicy``
    - etcd container image pull policy
    - ``IfNotPresent``
  * - ``etcd.replicas``
    - Number of etcd server replicas
    - ``1``
  * - ``etcd.logLevel``
    - Logging level for etcd
    - ``info``
  * - ``etcd.resources``
    - Resource requests and limits for the pod
    - ``{}``
  * - ``etcd.persistence.enabled``
    - Enable persistence of etcd database in persistent volume claim
    - ``false``
  * - ``etcd.persistence.size``
    - Size of persistent volume claim
    - ``1Gi``
  * - ``etcd.persistence.storageClassName``
    - Storage class name for persistent volume claim
    - ``standard``
  * - ``etcd.persistence.autoCompaction.retention``
    -  Auto compaction retention for key-value store in hours
    - ``0``
  * - ``etcd.persistence.autoCompaction.mode``
    -  Auto compaction mode
    - ``periodic``
  * - ``etcd.persistence.defrag.enabled``
    -  Enable periodic etcd defragmentation
    - ``false``
  * - ``etcd.persistence.defrag.cronjob.schedule``
    -  Schedule in Cron format for etcd defragmentation
    - ``30 23 * * 3,7``
  * - ``etcd.persistence.defrag.cronjob.historyLimit``
    -  Number of successful finished jobs to retain
    - ``1``
  * - ``etcd.maxTxnOps``
    -  Maximum number of operations per transaction
    - ``1024``

Console
-------

The console provides a command-line interface to monitor and control the SDP by
interacting with the configuration database.

.. list-table::
  :widths: auto
  :header-rows: 1

  * - Parameter
    - Description
    - Default
  * - ``console.enabled``
    - Enable the console
    - ``true``
  * - ``console.image``
    - Console container image
    - ``artefact.skao.int/ska-sdp-console``
  * - ``console.version``
    - Console container version
    - See `values file`_
  * - ``console.imagePullPolicy``
    - Console container image pull policy
    - ``IfNotPresent``
  * - ``console.replicas``
    - Number of console replicas
    - ``1``
  * - ``console.resources``
    - Resource requests and limits for the pod
    - ``{}``

Processing controller
---------------------

.. list-table::
  :widths: auto
  :header-rows: 1

  * - Parameter
    - Description
    - Default
  * - ``proccontrol.image``
    - Processing controller container image
    - ``artefact.skao.int/ska-sdp-proccontrol``
  * - ``proccontrol.version``
    - Processing controller container version
    - See `values file`_
  * - ``proccontrol.imagePullPolicy``
    - Processing controller container image pull policy
    - ``IfNotPresent``
  * - ``proccontrol.replicas``
    - Number of processing controller replicas
    - ``1``
  * - ``proccontrol.resources``
    - Resource requests and limits for the pod
    - ``{}``
  * - ``proccontrol.logLevel``
    - Logging level for processing controller
    - ``info``
  * - ``proccontrol.loopInterval``
    - Processing controller idle loop time (seconds)
    - See `values file`_
  * - ``proccontrol.livenessProbe``
    - Processing controller Kubernetes liveness probe configuration
    - See `values file`_
  * - ``proccontrol.readinessProbe``
    - Processing controller Kubernetes readiness probe configuration
    - See `values file`_

.. _helm-chart.helm-deployer:

Helm deployer
-------------

.. list-table::
  :widths: auto
  :header-rows: 1

  * - Parameter
    - Description
    - Default
  * - ``helmdeploy.image``
    - Helm deployer container image
    - ``artefact.skao.int/ska-sdp-helmdeploy``
  * - ``helmdeploy.version``
    - Helm deployer container version
    - See `values file`_
  * - ``helmdeploy.imagePullPolicy``
    - Helm deployer container image pull policy
    - ``IfNotPresent``
  * - ``helmdeploy.replicas``
    - Number of helm deployer replicas
    - ``1``
  * - ``helmdeploy.resources``
    - Resource requests and limits for the pod
    - ``{}``
  * - ``helmdeploy.logLevel``
    - Logging level for Helm deployer
    - ``info``
  * - ``helmdeploy.releasePrefix``
    - Prefix for Helm release names
    - ``""``
  * - ``helmdeploy.chartPrefix``
    - Prefix for Helm chart names
    - ``ska-sdp-helmdeploy``
  * - ``helmdeploy.chartRepo.url``
    - Chart repository URL
    - ``https://artefact.skao.int/repository/helm-internal``
  * - ``helmdeploy.chartRepo.refresh``
    - Chart repository refresh interval (in seconds)
    - ``0``
  * - ``helmdeploy.listMax``
    - Maximum number of releases to list at once
    - ``1024``
  * - ``helmdeploy.loopInterval``
    - Helm deployer idle loop time (seconds)
    - See `values file`_
  * - ``helmdeploy.livenessProbe``
    - Helm deployer Kubernetes liveness probe configuration
    - See `values file`_
  * - ``helmdeploy.readinessProbe``
    - Helm deployer Kubernetes readiness probe configuration
    - See `values file`_
  * - ``helmdeploy.enableNADClusterRole``
    - | If ``true`` then add credentials for helm deployer to list/get/watch
      | the network-attachment-definitions system CRD.
    - ``false``

LMC (Tango devices)
-------------------

.. list-table::
  :widths: auto
  :header-rows: 1

  * - Parameter
    - Description
    - Default
  * - ``lmc.enabled``
    - Enable the LMC. If set to ``false``, the SDP will run in headless mode
    - ``true``
  * - ``lmc.image``
    - LMC container image
    - ``artefact.skao.int/ska-sdp-lmc``
  * - ``lmc.version``
    - LMC container version
    - See `values file`_
  * - ``lmc.imagePullPolicy``
    - LMC container image pull policy
    - ``IfNotPresent``
  * - ``lmc.logLevel``
    - Logging level for LMC devices
    - ``info``
  * - ``lmc.nsubarray``
    - Number of subarrays to deploy
    - ``1``
  * - ``lmc.prefix``
    - Telescope prefix for Tango device names (e.g. ``low`` or ``mid``)
    - ``test``
  * - ``lmc.allCommandsHaveArgument``
    - Enable all Tango device commands to receive a transaction ID
    - ``false``
  * - ``lmc.strictValidation``
    - Enable strict validation of subarray command schemas
    - ``false``
  * - ``lmc.loopInterval``
    - LMC devices idle loop time (seconds)
    - See `values file`_
  * - ``lmc.livenessProbe``
    - LMC devices Kubernetes liveness probe configuration
    - See `values file`_
  * - ``lmc.readinessProbe``
    - LMC devices Kubernetes readiness probe configuration
    - See `values file`_
  * - ``lmc.controller``
    - Controller device settings
    - See `values file`_
  * - ``lmc.subarray``
    - Subarray device settings
    - See `values file`_
  * - ``lmc.subarray.timeoutAssignResources``
    - Timeout for AssignResources command (seconds)
    - ``60``
  * - ``lmc.subarray.timeoutConfigure``
    - Timeout for Configure command (seconds)
    - ``60``
  * - ``lmc.controller``
    - Controller device settings
    - See `values file`_
  * - ``lmc.subarray``
    - Subarray device settings
    - See `values file`_
  * - ``lmc.queueconnector``
    - QueueConnector device settings
    - See `values file`_


Slurm Deployer
--------------

.. list-table::
   :widths: auto
   :header-rows: 1

   * - Parameter
     - Description
     - Default
   * - ``slurmdeploy.enabled``
     - Enables the Slurm Deployer if ``True``.
     - ``false``
   * - ``slurmdeploy.image``
     - Slurm Deployer container image.
     - ``artefact.skao.int/ska-sdp-slurmdeploy``
   * - ``slurmdeploy.version``
     - Slurm Deployer container image version/tag
     - ``0.2.0``
   * - ``slurmdeploy.imagePullPolicy``
     - Slurm Deployer container image pull policy
     - ``IfNotPresent``
   * - ``slurmdeploy.slurmURL``
     - URL for the Slurm REST API endpoint
     - See ``values.yaml``
   * - ``slurmdeploy.replicas``
     - Number of Slurm Deployer replicas
     - ``1``
   * - ``slurmdeploy.logLevel``
     - Log level for the Slurm Deployer
     - ``info``
   * - ``slurmdeploy.resources``
     - Resource requests/limits for the Slurm Deployer Pod
     - ``{}``
   * - ``slurmdeploy.listMax``
     - Maximum number of Slurm deployments to list at once
     - ``1024``
   * - ``slurmdeploy.loopInterval``
     - Slurm Deployer idle loop time (seconds)
     - ``60``
   * - ``slurmdeploy.livenessProbe``
     - Kubernetes liveness probe for Slurm Deployer
     - See ``values.yaml``
   * - ``slurmdeploy.readinessProbe``
     - Kubernetes readiness probe for Slurm Deployer
     - See ``values.yaml``

Vault Static Secret
-------------------

This chart supports getting secrets from Vault and storing them in 
Kubernetes secrets.

.. list-table::
   :widths: auto
   :header-rows: 1

   * - Parameter
     - Description
     - Default
   * - ``vaultStaticSecret.enabled``
     - Enable syncing a static secret from Vault
     - ``false``
   * - ``vaultStaticSecret.mount``
     - The mount path in Vault
     - ``"dev/"``
   * - ``vaultStaticSecret.path``
     - The path to the secret in Vault
     - ``"orca/slurmdeploy-jwt"``
   * - ``vaultStaticSecret.secretName``
     - Name of the secret in K8s
     - ``"sdp-slurmdeploy-jwt"``
   * - ``vaultStaticSecret.refreshAfter``
     - How often to refresh the secret from Vault
     - ``"24h"``

This creates a Kubernetes Secret named ``vaultStaticSecret.secretName`` 
populated with the secret value in Vault at the specified path. The Slurm
Deployer will then use this K8s secret as its JWT for authenticating 
with the Slurm cluster in AWS.


Data Persistent Volume Claim (PVC)
-----------------------------------

Configure the SDP to use an existing volume or create a new one, and
configure the processing scripts to write the data to that volume. When
creating new PVCs, one will be created in the control system namespace and the
other in the processing namespace.

If the ``clone-pvc`` and ``clone-pvc-namespace`` values are set, then the PVCs
will be created as clones of an existing PVC in another namespace. This allows
the storage volume to be shared between namespaces. It depends on the
SKAO-specific shared volume infrastructure being available in the cluster.

.. list-table::
  :widths: auto
  :header-rows: 1

  * - Parameter
    - Description
    - Default
  * - ``global.data-product-pvc-name``
    - Name of data product PVC that is set as a global value to allow subcharts to access the same PVC
    - ``test-pvc``
  * - ``data-pvc.create.enabled``
    - Enable creation of PVCs
    - ``false``
  * - ``data-pvc.create.size``
    - Size of PVC
    - ``5Gi``
  * - ``data-pvc.create.storageClassName``
    - Storage class name for PVC
    - ``standard``
  * - ``data-pvc.create.clone-pvc``
    - Name of PVC to clone
    - Not set
  * - ``data-pvc.create.clone-pvc-namespace``
    - Namespace containing the PVC to clone
    - Not set
  * - ``data-pvc.pod.enabled``
    - Create a pod that connects to the data-pvc
    - ``false``

Processing scripts
------------------

Processing script definitions to be used by SDP. These map the script kind,
name and version to a container image. By default the definitions are read from
the `scripts repository`_ in GitLab at SDP installation time.
A different URL may be specified.
A schedule can be established to reload the definitions periodically.
Alternatively a list of script definitions can be passed to the chart.

.. list-table::
  :widths: auto
  :header-rows: 1

  * - Parameter
    - Description
    - Default
  * - ``scripts.url``
    - URL from which to read the script definitions
    - ``tmdata::gitlab://gitlab.com/ska-telescope/sdp/ska-sdp-script#tmdata``
  * - ``scripts.definitions``
    - List of script definitions. If present, used instead of the URL. See the example below
    - Not set
  * - ``scripts.updateSchedule``
    - The `cronjob schedule <https://kubernetes.io/docs/concepts/workloads/controllers/cron-jobs/#schedule-syntax>`_
      at which script definitions are re-loaded.
      Only relevant when not using ``scripts.definitions``
    - Not set

Example of script definitions in a values file:

.. code-block:: yaml

  scripts:
    definitions:
    - type: realtime
      id: test-realtime
      version: 0.3.0
      image: artefact.skao.int/ska-sdp-script-test-batch:0.3.0
    - type: batch
      id: test-batch
      version: 0.3.0
      image: artefact.skao.int/ska-sdp-script-test-realtime:0.3.0

SDP Job settings
----------------

Settings that control some of the aspects of the jobs that run
when SDP is deployed.

.. list-table::
  :widths: auto
  :header-rows: 1

  * - Parameter
    - Description
    - Default
  * - ``jobs.ttl``
    - Time-to-live of jobs after successful completion in seconds
    - ``30``

Proxy settings
--------------

Proxy settings are applied to the components that retrieve configuration data
via HTTPS: the script definitions and the Helm charts.

.. list-table::
  :widths: auto
  :header-rows: 1

  * - Parameter
    - Description
    - Default
  * - ``proxy.server``
    - Address of proxy server
    - Not set
  * - ``proxy.noproxy``
    - List of addresses or subnets for which the proxy should not be used
    - Not set

Example of proxy settings in a values file:

.. code-block:: yaml

  proxy:
    server: http://proxy.mydomain
    noproxy:
    - 192.168.0.1
    - 192.168.0.2


.. _values file: https://gitlab.com/ska-telescope/sdp/ska-sdp-integration/-/blob/master/charts/ska-sdp/values.yaml
.. _etcd: https://etcd.io
.. _scripts repository: https://gitlab.com/ska-telescope/sdp/ska-sdp-script
.. _SDP QA data API: https://gitlab.com/ska-telescope/sdp/ska-sdp-qa-data-api
.. _Apache Kafka: https://kafka.apache.org/

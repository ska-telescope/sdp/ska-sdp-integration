.. _running_requirements:

Requirements
============

To run the SDP, you need to have the `Kubernetes <https://kubernetes.io/>`_ CLI (kubectl) and
`Helm <https://helm.sh>`_ installed. You will also need access to a Kubernetes
cluster, either remote (e.g. :ref:`an SKA-provided one <remote_cluster>`) or
local (e.g. using :ref:`Minikube <minikube_doc>`).

Kubernetes CLI
--------------

Kubernetes (a.k.a. k8s) is a software system used for deploying, scaling,
and maintaining large-scale applications. The SKA uses k8s
as its software platform.

The user CLI of Kubernetes is kubectl. Follow the instructions here
to install it: `<https://kubernetes.io/docs/tasks/tools/#kubectl>`_

Command help guide: `<https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands>`_

Helm
----

Helm is a package manager for Kubernetes. The SKA software control system,
including the SDP, is packaged and deployed using Helm.

Helm is available from most typical package managers, see `Introduction to Helm
<https://helm.sh/docs/intro/>`_.

Command help guide: `<https://helm.sh/docs/helm/helm/>`_

K9s
---

`K9s <https://k9scli.io>`_ is terminal-based UI for Kubernetes clusters which
provides a convenient interactive interface. It is not required to run the SDP,
but it is recommended for its ease of use.

{{/*
If the control system namespace is used for the dynamic deployments (which is strongly discouraged),
this checks that the release prefix is set, and that it does not clash with this chart's release name.
*/}}
{{- if eq (include "ska-sdp.processingNamespace" .) .Release.Namespace }}
{{- if eq .Values.helmdeploy.releasePrefix "" }}
{{- fail "If control system namespace is used for dynamic deployments, helmdeploy.releasePrefix must not be empty" }}
{{- else if hasPrefix ( printf "%s-" .Values.helmdeploy.releasePrefix ) .Release.Name }}
{{- fail "If control system namespace is used for dynamic deployments, helmdeploy.releasePrefix must not be a prefix of this chart's release name" }}
{{- end }}
{{- end }}
# Dummy Service to ensure Pod is DNS addressable
apiVersion: v1
kind: Service
metadata:
  name: {{ include "ska-sdp.name" . }}-helmdeploy
  namespace: {{ .Release.Namespace }}
  labels:
    {{- include "ska-sdp.labels" . | indent 4 }}
    component: {{ .Values.helmdeploy.component }}
    subsystem: {{ .Values.helmdeploy.subsystem }}
    function: {{ .Values.helmdeploy.function }}
    domain: {{ .Values.helmdeploy.domain }}
    intent: production
spec:
  clusterIP: None
  selector:
    component: {{ .Values.helmdeploy.component }}
    subsystem: {{ .Values.helmdeploy.subsystem }}
---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: {{ include "ska-sdp.name" . }}-helmdeploy
  namespace: {{ .Release.Namespace }}
  labels:
    {{- include "ska-sdp.labels" . | indent 4 }}
    component: {{ .Values.helmdeploy.component }}
    subsystem: {{ .Values.helmdeploy.subsystem }}
    function: {{ .Values.helmdeploy.function }}
    domain: {{ .Values.helmdeploy.domain }}
    intent: production
spec:
  replicas: {{ .Values.helmdeploy.replicas }}
  selector:
    matchLabels:
      component: {{ .Values.helmdeploy.component }}
      subsystem: {{ .Values.helmdeploy.subsystem }}
  serviceName: {{ include "ska-sdp.name" . }}-helmdeploy
  template:
    metadata:
      labels:
        {{- include "ska-sdp.labels" . | indent 8 }}
        component: {{ .Values.helmdeploy.component }}
        subsystem: {{ .Values.helmdeploy.subsystem }}
        function: {{ .Values.helmdeploy.function }}
        domain: {{ .Values.helmdeploy.domain }}
        intent: production
    spec:
      initContainers:
      {{- include "ska-sdp.wait-for-etcd" . | nindent 6 }}
      containers:
      - name: helmdeploy
        image: {{ .Values.helmdeploy.image }}:{{ .Values.helmdeploy.version }}
        imagePullPolicy: {{ .Values.helmdeploy.imagePullPolicy }}
        env:
        - name: SDP_LOG_LEVEL
          value: {{ .Values.helmdeploy.logLevel | upper | quote }}
        - name: SDP_CONFIG_HOST
          value: {{ include "ska-sdp.etcd-host" . }}
        - name: SDP_HELM_NAMESPACE
          value: {{ include "ska-sdp.processingNamespace" . | quote }}
        - name: SDP_HELM_RELEASE_PREFIX
          value: {{ .Values.helmdeploy.releasePrefix | quote }}
        - name: SDP_HELM_CHART_PREFIX
          value: {{ .Values.helmdeploy.chartPrefix | quote }}
        - name: SDP_HELM_CHART_REPO_URL
          value: {{ .Values.helmdeploy.chartRepo.url | quote }}
        - name: SDP_HELM_CHART_REPO_REFRESH
          value: {{ .Values.helmdeploy.chartRepo.refresh | quote }}
        - name: SDP_HELM_LIST_MAX
          value: {{ .Values.helmdeploy.listMax | quote }}
        - name: SDP_HELMDEPLOY_LOOP_INTERVAL
          value: {{ .Values.helmdeploy.loopInterval | quote }}
        {{- include "ska-sdp.http-proxy" . | nindent 8 }}
        livenessProbe:
          exec:
            command:
            - /bin/sh
            - -c
            - '[ $(($(date +%s)-$(stat -c "%Y" /tmp/alive))) -lt {{ .Values.helmdeploy.livenessProbe.maxAgeSeconds }} ]'
          initialDelaySeconds: {{ .Values.helmdeploy.livenessProbe.initialDelaySeconds }}
          periodSeconds: {{ .Values.helmdeploy.livenessProbe.periodSeconds }}
          failureThreshold: 1
        readinessProbe:
          exec:
            command:
              - cat
              - /tmp/alive
          initialDelaySeconds: {{ .Values.helmdeploy.readinessProbe.initialDelaySeconds }}
          periodSeconds: {{ .Values.helmdeploy.readinessProbe.periodSeconds }}
        {{- if .Values.helmdeploy.resources }}
        resources: {{ toYaml .Values.helmdeploy.resources | nindent 10 }}
        {{- end }}
      serviceAccountName: {{ include "ska-sdp.name" . }}-helm
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: {{ include "ska-sdp.name" . }}-helm
  namespace: {{ .Release.Namespace }}
  labels:
    {{- include "ska-sdp.labels" . | indent 4 }}
    component: {{ .Values.helmdeploy.component }}
    subsystem: {{ .Values.helmdeploy.subsystem }}
    function: {{ .Values.helmdeploy.function }}
    domain: {{ .Values.helmdeploy.domain }}
    intent: production
---
kind: RoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: {{ include "ska-sdp.name" . }}-helm
  namespace: {{ include "ska-sdp.processingNamespace" . | quote }}
  labels:
    {{- include "ska-sdp.labels" . | indent 4 }}
    component: {{ .Values.helmdeploy.component }}
    subsystem: {{ .Values.helmdeploy.subsystem }}
    function: {{ .Values.helmdeploy.function }}
    domain: {{ .Values.helmdeploy.domain }}
    intent: production
subjects:
- kind: ServiceAccount
  name: {{ include "ska-sdp.name" . }}-helm
  namespace: {{ .Release.Namespace }}
roleRef:
  kind: Role
  name: {{ include "ska-sdp.name" . }}-helm
  apiGroup: rbac.authorization.k8s.io
---
kind: Role
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: {{ include "ska-sdp.name" . }}-helm
  namespace: {{ include "ska-sdp.processingNamespace" . | quote }}
  labels:
    {{- include "ska-sdp.labels" . | indent 4 }}
    component: {{ .Values.helmdeploy.component }}
    subsystem: {{ .Values.helmdeploy.subsystem }}
    function: {{ .Values.helmdeploy.function }}
    domain: {{ .Values.helmdeploy.domain }}
    intent: production
rules:
- apiGroups: [""]
  resources: ["namespaces"]
  verbs: ["list", "get"]
- apiGroups: [""]
  resources: ["pods", "configmaps", "persistentvolumeclaims", "services", "secrets"]
  verbs: ["list", "get", "watch", "create", "update", "patch", "delete"]
- apiGroups: ["apps"]
  resources: ["deployments", "statefulsets", "replicasets"]
  verbs: ["list", "get", "watch", "create", "update", "patch", "delete"]
- apiGroups: ["batch"]
  resources: ["jobs"]
  verbs: ["list", "get", "watch", "create", "update", "patch", "delete"]
{{- if .Values.helmdeploy.enableNADClusterRole }}
{{- $cluster_role_name := printf "%s-net-attach-def-monitoring-%s" (include "ska-sdp.name" .) .Release.Namespace }}
---
kind: ClusterRoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: {{ $cluster_role_name }}
  labels:
    {{- include "ska-sdp.labels" . | indent 4 }}
    component: {{ .Values.helmdeploy.component }}
    subsystem: {{ .Values.helmdeploy.subsystem }}
    function: {{ .Values.helmdeploy.function }}
    domain: {{ .Values.helmdeploy.domain }}
    intent: production
subjects:
  - kind: ServiceAccount
    name: {{ include "ska-sdp.name" . }}-helm
    namespace: {{ .Release.Namespace }}
roleRef:
  kind: ClusterRole
  name: {{ $cluster_role_name }}
  apiGroup: rbac.authorization.k8s.io
---
kind: ClusterRole
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: {{ $cluster_role_name }}
rules:
  - apiGroups: ["k8s.cni.cncf.io"]
    resources: ["network-attachment-definitions"]
    verbs: ["list", "get", "watch"]
{{- end }}

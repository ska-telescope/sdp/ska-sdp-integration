{{/*
Generate Job to configure Tango DB.
This template should be called with a context containing:
  - config: configuration to be written to Tango DB
  - root: the root context of the chart
*/}}
{{- define "ska-sdp.lmc-config" }}
{{- $tango_host := coalesce .root.Values.global.tango_host "databaseds-tango-base:10000" }}
---
# Configmap containing device server configuration
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ include "ska-sdp.name" .root }}-lmc-config
  namespace: {{ .root.Release.Namespace }}
  labels:
    {{- include "ska-sdp.labels" .root | indent 4 }}
    component: {{ .root.Values.lmc.component }}-configuration
    subsystem: enabling-system
    function: deployment
    domain: self-configuration
    intent: enabling
data:
  safe-dsconfig.sh: |
    {{- .root.Files.Get "data/safe-dsconfig.sh" | nindent 4 }}
  sdp-devices.json: |
    {{- toPrettyJson .config | nindent 4 }}
---
# Job to apply device server configuration to Tango database
apiVersion: batch/v1
kind: Job
metadata:
  name: {{ include "ska-sdp.name" .root }}-lmc-config
  namespace: {{ .root.Release.Namespace }}
  labels:
    {{- include "ska-sdp.labels" .root | indent 4 }}
    component: {{ .root.Values.lmc.component }}-configuration
    subsystem: enabling-system
    function: deployment
    domain: self-configuration
    intent: enabling
spec:
  ttlSecondsAfterFinished: {{ .root.Values.jobs.ttl }}
  template:
    spec:
      initContainers:
      - name: check-databaseds-ready
        image: {{ .root.Values.dsconfig.image.registry }}/{{ .root.Values.dsconfig.image.image }}:{{ .root.Values.dsconfig.image.tag }}
        imagePullPolicy: {{ .root.Values.dsconfig.image.pullPolicy }}
        command:
          - sh
          - -c
        args:
          - retry
          {{- if .root.Values.global.retry }}
          {{- range $retry_option := .root.Values.global.retry }}
          - {{ $retry_option }}
          {{- end }}
          {{- else }}
          - --max=60
          {{- end}}
          - --
          - tango_admin
          - --check-device
          - sys/database/2
        env:
        - name: TANGO_HOST
          value: {{ $tango_host }}
        {{- if (.root.Values.global.environment_variables) }}
        {{- range $index, $envvar := .root.Values.global.environment_variables }}
        - name: {{ $envvar.name }}
          value: {{ tpl ($envvar.value | toString) $ }}
        {{- end }}
        {{- end }}
      containers:
      - name: dsconfig
        image: {{ .root.Values.dsconfig.image.registry }}/{{ .root.Values.dsconfig.image.image }}:{{ .root.Values.dsconfig.image.tag }}
        imagePullPolicy: {{ .root.Values.dsconfig.image.pullPolicy }}
        command:
          - sh
          - -c
        args:
          - retry --sleep=1 --tries=60 -- /bin/bash data/safe-dsconfig.sh -w -a -u data/sdp-devices.json
        env:
        - name: TANGO_HOST
          value: {{ $tango_host }}
        volumeMounts:
          - name: configuration
            mountPath: data
            readOnly: true
      volumes:
        - name: configuration
          configMap:
            name: {{ include "ska-sdp.name" .root }}-lmc-config
      restartPolicy: Never
{{- end}}

{{/*
Generate StatefulSet containing a Tango device server.
This template should be called with a context containing:
  - device: the details of the device, with entries:
    + name: Name (e.g. controller)
    + tangoname: Tango device name (e.g. mid-sdp/control/0)
    + command: Command to run the device (e.g. ["SDPController"])
    + args: Arguments to pass to command (e.g. ["0", "-v4"])
    + function: function label (e.g. sdp-monitoring)
    + domain: domain label (e.g. sdp-central-control)
    + livenessProbe:
     - initialDelaySeconds - initial delay
     - periodSeconds - probe period
     - maxAgeSeconds - maximum age of test file
  - root: the root context of the chart
*/}}
{{- define "ska-sdp.lmc-device" }}
{{- $tango_host := coalesce .root.Values.global.tango_host "databaseds-tango-base:10000" }}
{{- $tango_server_port := coalesce .root.Values.global.device_server_port "45450" }}
{{- $tango_heartbeat_port := coalesce .root.Values.global.device_heartbeat_port "45460" }}
{{- $tango_event_port := coalesce .root.Values.global.device_event_port "45470" }}
{{- $cluster_domain := coalesce .root.Values.global.cluster_domain "cluster.local" }}
---
# Dummy Service to ensure Pod is DNS addressable
apiVersion: v1
kind: Service
metadata:
  name: {{ include "ska-sdp.name" .root }}-lmc-{{ .device.name }}
  namespace: {{ .root.Release.Namespace }}
  labels:
    {{- include "ska-sdp.labels" .root | indent 4 }}
    component: {{ .root.Values.lmc.component }}-{{ .device.name }}
    subsystem: {{ .root.Values.lmc.subsystem }}
    function: {{ .device.function }}
    domain: {{ .device.domain }}
    intent: production
spec:
  {{- if or .root.Values.global.exposeAllDS .device.loadBalancer }}
  type: LoadBalancer
  ports:
  - name: tango-server
    port: {{ $tango_server_port }}
    targetPort: {{ $tango_server_port }}
  - name: tango-heartbeat
    port: {{ $tango_heartbeat_port }}
    targetPort: {{ $tango_heartbeat_port }}
  - name: tango-event
    port: {{ $tango_event_port }}
    targetPort: {{ $tango_event_port }}
  {{- else}}
  clusterIP: None
  {{- end}}
  selector:
    component: {{ .root.Values.lmc.component }}-{{ .device.name }}
    subsystem: {{ .root.Values.lmc.subsystem }}
---
# StatefulSet with single Pod
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: {{ include "ska-sdp.name" .root }}-lmc-{{ .device.name }}
  namespace: {{ .root.Release.Namespace }}
  labels:
    {{- include "ska-sdp.labels" .root | indent 4 }}
    component: {{ .root.Values.lmc.component }}-{{ .device.name }}
    subsystem: {{ .root.Values.lmc.subsystem }}
    function: {{ .device.function }}
    domain: {{ .device.domain }}
    intent: production
spec:
  selector:
    matchLabels:
      component: {{ .root.Values.lmc.component }}-{{ .device.name }}
      subsystem: {{ .root.Values.lmc.subsystem }}
  serviceName: {{ include "ska-sdp.name" .root }}-lmc-{{ .device.name }}
  replicas: 1
  template:
    metadata:
      labels:
        {{- include "ska-sdp.labels" .root | indent 8 }}
        component: {{ .root.Values.lmc.component }}-{{ .device.name }}
        subsystem: {{ .root.Values.lmc.subsystem }}
        function: {{ .device.function }}
        domain: {{ .device.domain }}
        intent: production
    spec:
      initContainers:
      {{- include "ska-sdp.wait-for-etcd" .root | nindent 6 }}
      - name: wait-for-device-config
        image: {{ .root.Values.dsconfig.image.registry }}/{{ .root.Values.dsconfig.image.image }}:{{ .root.Values.dsconfig.image.tag }}
        imagePullPolicy: {{ .root.Values.dsconfig.image.pullPolicy }}
        command:
          - retry
          - --max=10
          - --
          - tango_admin
          - --check-device
          - {{ .device.tangoname | toString }}
        env:
        - name: TANGO_HOST
          value: {{ $tango_host }}
      containers:
      - name: {{ .device.name }}
        image: {{ .device.image }}
        imagePullPolicy: {{ .root.Values.lmc.imagePullPolicy }}
        command: {{ toYaml .device.command | nindent 8 }}
        args: {{ toYaml .device.args | nindent 8}}
        - -ORBendPoint
        - giop:tcp::$(TANGO_SERVER_PORT)
        - -ORBendPointPublish
        - giop:tcp:$(TANGO_SERVER_PUBLISH_HOSTNAME):$(TANGO_SERVER_PORT)
        env:
        - name: TANGO_HOST
          value: {{ $tango_host }}
        - name: TANGO_SERVER_PORT
          value: {{ $tango_server_port | quote }}
        - name: TANGO_ZMQ_HEARTBEAT_PORT
          value: {{ $tango_heartbeat_port | quote }}
        - name: TANGO_ZMQ_EVENT_PORT
          value: {{ $tango_event_port | quote }}
        - name: TANGO_SERVER_PUBLISH_HOSTNAME
          value: {{ include "ska-sdp.name" .root }}-lmc-{{ .device.name }}.{{ .root.Release.Namespace }}.svc.{{ $cluster_domain }}
        - name: SDP_LOG_LEVEL
          value: {{ .root.Values.lmc.logLevel | upper | quote }}
        - name: SDP_CONFIG_HOST
          value: {{ include "ska-sdp.etcd-host" .root }}
        - name: SDP_LMC_LOOP_INTERVAL
          value: {{ .root.Values.lmc.loopInterval | float64 | quote }}
        - name: SDP_LMC_ASSIGN_RESOURCES_TIMEOUT
          value: {{ .root.Values.lmc.subarray.timeoutAssignResources | float64 | quote }}
        - name: SDP_LMC_CONFIGURE_TIMEOUT
          value: {{ .root.Values.lmc.subarray.timeoutConfigure | float64 | quote }}
        - name: FEATURE_ALL_COMMANDS_HAVE_ARGUMENT
          value: {{ .root.Values.lmc.allCommandsHaveArgument | int | quote }}
        {{- if .root.Values.lmc.strictValidation }}
        - name: SKA_TELMODEL_ALLOW_STRICT_VALIDATION
          value: "1"
        {{- end }}
        - name: FEATURE_STRICT_VALIDATION
          value: {{ .root.Values.lmc.strictValidation | int | quote }}
        {{- if .device.envVars }}
        {{ toYaml .device.envVars | nindent 8 }}
        {{- end }}
        {{- if .device.livenessProbe }}
        livenessProbe:
          exec:
            command:
            - /bin/sh
            - -c
            - '[ $(($(date +%s)-$(stat -c "%Y" /tmp/alive))) -lt {{ .device.livenessProbe.maxAgeSeconds }} ]'
          initialDelaySeconds: {{ .device.livenessProbe.initialDelaySeconds }}
          periodSeconds: {{ .device.livenessProbe.periodSeconds }}
          failureThreshold: 1
        readinessProbe:
          exec:
            command:
              - cat
              - /tmp/alive
          initialDelaySeconds: {{ .device.readinessProbe.initialDelaySeconds }}
          periodSeconds: {{ .device.readinessProbe.periodSeconds }}
        {{- end }}
        {{- if .device.resources }}
        resources: {{ toYaml .device.resources | nindent 10 }}
        {{- end }}
{{- end }}

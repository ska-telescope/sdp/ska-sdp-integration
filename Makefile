PROJECT_NAME = ska-sdp-integration
PROJECT_PATH = ska-telescope/sdp/ska-sdp-integration
ARTEFACT_TYPE = helm
CHANGELOG_FILE = CHANGELOG.rst
PYTHON_LINT_TARGET = scripts/ tests/
DOCS_SPHINXOPTS = -W --keep-going

# Variables for tests
CLUSTER_DOMAIN ?= cluster.local
KUBE_NAMESPACE ?= test
KUBE_NAMESPACE_SDP ?= test-sdp
HELM_RELEASE ?= test
HELM_REPO ?= charts
HELM_VALUES ?= resources/values/test-external.yaml
HELM_TIMEOUT ?= 5m
HELM_UNINSTALL_HAS_WAIT ?= 1
DATA_PRODUCT_PVC_NAME ?= test-pvc
NSUBARRAY ?= 1
TEST_INGRESS ?=
TEST_TANGO_CLIENT ?= gql
TARANTA_AUTH_URL ?= https://k8s-services.skao.int/auth/login
TARANTA_USERNAME ?= user1
TARANTA_PASSWORD ?= abc123
TIMEOUT_ASSIGN_RESOURCES ?= 300
TIMEOUT_CONFIGURE ?= 300
TEST_MARKER ?= not extended_visibility_receive_test
PYTEST_FLAGS ?=
TEST_TIMEOUT_ASSIGN_RES := TIMEOUT_ASSIGN_RESOURCES
TEST_TIMEOUT_CONFIGURE := TIMEOUT_CONFIGURE

include .make/base.mk
include .make/python.mk
include .make/helm.mk
include .make/k8s.mk
include .make/dependencies.mk

# Export variables used in tests. If these variables are not set in the
# environment when running "make test", the default values defined above will
# be passed on to the pytest command.
export KUBE_NAMESPACE KUBE_NAMESPACE_SDP TEST_INGRESS TEST_TANGO_CLIENT \
	DATA_PRODUCT_PVC_NAME TARANTA_AUTH_URL TARANTA_USERNAME TARANTA_PASSWORD

.PHONY: help create-namespaces delete-namespaces recreate-namespaces \
	update-chart-dependencies install-sdp uninstall-sdp delete-etcd-pvc test

create-namespaces: ## Create namespaces for SDP deployment
	kubectl create namespace $(KUBE_NAMESPACE)
	kubectl create namespace $(KUBE_NAMESPACE_SDP)

delete-namespaces: ## Delete namespaces for SDP deployment
	kubectl delete namespace $(KUBE_NAMESPACE) --ignore-not-found
	kubectl delete namespace $(KUBE_NAMESPACE_SDP) --ignore-not-found

recreate-namespaces: delete-namespaces create-namespaces ## Recreate namespaces (delete and create again)

update-chart-dependencies: ## Update chart dependencies of the SDP
	helm dependency update charts/ska-sdp

install-sdp: ## Install the SDP
	# run `make install-sdp HELM_VALUES=resources/values/itango.yaml` to install SDP with itango
	helm upgrade --install $(HELM_RELEASE) $(HELM_REPO)/ska-sdp -n $(KUBE_NAMESPACE) \
		--set global.sdp.processingNamespace=$(KUBE_NAMESPACE_SDP) \
		--set global.data-product-pvc-name=$(DATA_PRODUCT_PVC_NAME) \
		--set lmc.nsubarray=$(NSUBARRAY) \
		--set lmc.subarray.timeoutAssignResources=$(TIMEOUT_ASSIGN_RESOURCES) \
		--set lmc.subarray.timeoutConfigure=$(TIMEOUT_CONFIGURE) \
		--set global.cluster_domain=$(CLUSTER_DOMAIN) \
		--set kafka.clusterDomain=$(CLUSTER_DOMAIN) \
		--set kafka.zookeeper.clusterDomain=$(CLUSTER_DOMAIN) \
		--set ska-sdp-qa.redis.clusterDomain=$(CLUSTER_DOMAIN) \
		$(foreach file,$(HELM_VALUES),--values $(file)) --wait --timeout=$(HELM_TIMEOUT)

uninstall-sdp: ## Uninstall the SDP
	helm uninstall $(HELM_RELEASE) -n $(KUBE_NAMESPACE) $(if $(filter 1,$(HELM_UNINSTALL_HAS_WAIT)),--wait)

install-alarm-handler: ## Install the SKA Alarm Handler
	helm upgrade --install test-ah skao/ska-tango-alarmhandler -n $(KUBE_NAMESPACE) \
	--set global.tango_host=databaseds-tango-base:10000 \
	--set global.operator=true \
	--set global.cluster_domain=$(CLUSTER_DOMAIN) \
	--set labels.app=ska-sdp \
	--set ska-tango-base.enabled=false \
	--wait --timeout=$(HELM_TIMEOUT)

delete-etcd-pvc: ## Delete PVC for etcd database
	kubectl -n $(KUBE_NAMESPACE) delete pvc data-ska-sdp-etcd-0 --ignore-not-found

test: ## Run the integration tests
	pytest -vv --gherkin-terminal-reporter --junitxml=unit-tests.xml --cucumber-json=test-results.json $(if $(TEST_MARKER),-m "$(TEST_MARKER)") $(PYTEST_FLAGS) tests

"""Visibility receive test."""

# pylint: disable=import-error
# pylint: disable=redefined-outer-name
# pylint: disable=too-many-locals
# pylint: disable=unused-argument
# pylint: disable=redefined-outer-name
# pylint: disable=duplicate-code


import logging
import random
import time

import pytest
from common import compare_data, compare_scan
from common.cbf_emulator import send_scans
from pytest_bdd import scenarios, then, when

LOG = logging.getLogger(__name__)

scenarios("visibility_receive.feature")


@pytest.fixture(name="assign_resources_config")
def read_assign_resources_config(telescope_metadata):
    """
    Read the AssignResources config for visibility receive.

    Substitutes randomly-generated IDs so the test can be run more than once in
    the same instance of SDP. The config is used in more than one step in the
    test, so it must be a fixture.

    """

    config = telescope_metadata.assign_resources

    return config


# ----------
# Given steps
# ----------

# In integration/conftest.py:
# Given I connect to an SDP subarray
# Given obsState is EMPTY
# Given I select the dataset for the ____ telescope
# Given the volumes are created and the CBF emulator input data is copied
# Given I deploy the visibility receive script


# ----------
# When steps
# ----------


@when(
    "SDP is commanded to capture data from 2 successive scans",
    target_fixture="scan_ids",
)
def command_two_scans(
    context,
    subarray_device,
    subarray_ready,
    k8s_element_manager,
    telescope_metadata,
):
    """
    Run a sequence of scans.

    :param context: context for the tests
    :param subarray_device: SDP subarray device client
    :param subarray_ready: subarray fixture
    :param k8s_element_manager: Kubernetes element manager
    :param telescope_metadata: fixture for storing and managing metadata
    """
    # send the same data for each scan
    ms_data = telescope_metadata.data_dir[0]

    commanded_scans = ((1, "science", ms_data), (2, "calibration", ms_data))
    scan_ids = [scan[0] for scan in commanded_scans]

    send_scans(
        context,
        subarray_device,
        subarray_ready,
        k8s_element_manager,
        telescope_metadata,
        commanded_scans,
    )

    return scan_ids


@when(
    "SDP is commanded to run scans for 20 minutes", target_fixture="scan_ids"
)
def command_scans_for_20min(
    context,
    subarray_device,
    subarray_ready,
    k8s_element_manager,
    telescope_metadata,
):
    """
    Run a alternating scans for a long period of time.

    :param context: context for the tests
    :param subarray_device: SDP subarray device client
    :param subarray_ready: subarray fixture
    :param k8s_element_manager: Kubernetes element manager
    :param telescope_metadata: fixture for storing and managing metadata
    """
    # send the same data for each scan
    ms_data = telescope_metadata.data_dir[0]

    # Keep track of the scan IDs from the scan generator
    scan_ids = []

    def scan_generator():
        # Multiple scan types
        # Random scan types are picked from the list and runs for 20 minutes
        scan_types = ["science", "calibration"]
        t_end = time.time() + 60 * 20
        scan_id = 1
        while time.time() < t_end:
            scan_ids.append(scan_id)
            yield scan_id, random.choice(scan_types), ms_data
            scan_id += 1

    send_scans(
        context,
        subarray_device,
        subarray_ready,
        k8s_element_manager,
        telescope_metadata,
        scan_generator(),
    )

    return scan_ids


# ----------
# Then steps
# ----------


@then("the data received matches with the data sent")
def check_measurement_set(
    context, dataproduct_directory, telescope_metadata, scan_ids
):
    """
    Check the data received are same as the data sent.

    :param context: context for the tests
    :param dataproduct_directory: The directory where outputs are written
    :param telescope_metadata: fixture for storing and managing metadata
    :param scan_ids: fixture returning list of commanded scans
    """

    # Wait 10 seconds before checking the measurement.
    # This gives enough time for the receiver for finish writing the data.
    time.sleep(10)

    receive_pod = f"receive-data-{context.sdp.subarray_id}"
    receive_container = "data-prep"
    input_data_name = telescope_metadata.data_dir[0]

    for scan_id in scan_ids:
        result = compare_data(
            receive_pod,
            receive_container,
            context.sdp.namespace_sdp,
            f"{dataproduct_directory}/output.scan-{scan_id}.ms",
            input_data_name,
        )
        assert result.returncode == 0
        LOG.info("Data sent matches the data received")


@then("each scan can be identified by its associated metadata")
def check_measurement_set_scan(
    context,
    dataproduct_directory,
    k8s_element_manager,
    scan_ids,
):
    """
    Check that each Measurement Set corresponds to the scan ID it indicates in
    its name.

    :param context: context for the tests
    :param dataproduct_directory: The directory where outputs are written
    :param k8s_element_manager: Kubernetes element manager
    :param scan_ids: fixture returning list of commanded scans
    """
    receive_pod = f"receive-data-{context.sdp.subarray_id}"
    receive_container = "data-prep"

    # Add data product directory to k8s element manager for cleanup
    parse_dir = dataproduct_directory.index("ska-sdp")
    data_eb_dir = dataproduct_directory[:parse_dir]
    k8s_element_manager.output_directory(
        data_eb_dir,
        receive_pod,
        receive_container,
        context.sdp.namespace_sdp,
    )

    for scan_id in scan_ids:
        compare_scan(
            receive_pod,
            receive_container,
            context.sdp.namespace_sdp,
            f"{dataproduct_directory}/output.scan-{scan_id}.ms",
            scan_id,
        )

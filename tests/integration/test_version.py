"""Test the LMC Controller Versions."""

# pylint: disable=too-many-locals

import json

import yaml
from pytest_bdd import scenarios, then, when

scenarios("version.feature")

# ----------
# Given steps
# ----------

# In tests/conftest.py:
# Given I connect to the SDP controller

# ----------
# When steps
# ----------


@when("I read the sdpVersion attribute", target_fixture="sdp_version")
def query_sdp_version(controller_device):
    """Retrieve the version info from the LMC device."""
    return json.loads(controller_device.read_attribute("sdpVersion"))


@when("I read the versionId attribute", target_fixture="version_id")
def query_device_version_id(controller_device):
    """Retrieve the version info from the LMC device."""
    return controller_device.read_attribute("versionID")


# ----------
# Then steps
# ----------


@then(
    "it contains the components and dependencies with the correct structure "
    "and versions"
)
def confirm_version_data(sdp_version):
    """Check that the expected details exist."""

    # This test is brittle because it assumes the values are read from the
    # chart's values file, but they could be overridden using an external
    # values file or on the command line. TODO: Work out if it is possible to
    # generalise it to work in those cases.

    with open(
        "charts/ska-sdp/values.yaml", "r", encoding="utf-8"
    ) as values_file_raw:
        values_file = yaml.safe_load(values_file_raw)

    with open(
        "charts/ska-sdp/Chart.yaml", "r", encoding="utf-8"
    ) as chart_file_raw:
        chart_file = yaml.safe_load(chart_file_raw)

    assert "version" in sdp_version
    assert "components" in sdp_version
    assert "dependencies" in sdp_version
    assert len(sdp_version["components"]) > 0

    for comp in sdp_version["components"].values():
        assert "image" in comp
        assert "version" in comp

    for depend in sdp_version["dependencies"].values():
        assert "repository" in depend
        assert "version" in depend

    components = sdp_version["components"]
    dependencies = sdp_version["dependencies"]
    chart_dependencies = {c["name"]: c for c in chart_file["dependencies"]}

    for comp in ["etcd", "proccontrol", "helmdeploy"]:
        assert comp in components
        assert values_file[comp]["image"] == components[comp]["image"]
        assert values_file[comp]["version"] == components[comp]["version"]

    # Check LMC controller
    assert "lmc-controller" in components
    assert values_file["lmc"]["image"] == components["lmc-controller"]["image"]
    assert (
        values_file["lmc"]["version"]
        == components["lmc-controller"]["version"]
    )

    # Check LMC subarrays
    for subarray in (
        comp for comp in components.keys() if comp.startswith("lmc-subarray")
    ):
        assert values_file["lmc"]["image"] == components[subarray]["image"]
        assert values_file["lmc"]["version"] == components[subarray]["version"]

    # Check LMC queue connectors
    for queueconnector in (
        comp
        for comp in components.keys()
        if comp.startswith("lmc-queueconnector")
    ):
        assert (
            values_file["lmc"]["queueconnector"]["image"]
            == components[queueconnector]["image"]
        )
        assert (
            values_file["lmc"]["queueconnector"]["version"]
            == components[queueconnector]["version"]
        )

    # Confirm DSCONFIG
    assert "dsconfig" in components
    dsconfig_image = values_file["dsconfig"]["image"]
    assert (
        f"{dsconfig_image['registry']}/{dsconfig_image['image']}"
        == components["dsconfig"]["image"]
    )
    assert (
        values_file["dsconfig"]["image"]["tag"]
        == components["dsconfig"]["version"]
    )

    # Check test deploy values:
    for chart in ["ska-sdp-qa", "ska-tango-base"]:
        assert chart in dependencies
        assert (
            chart_dependencies[chart]["repository"]
            == dependencies[chart]["repository"]
        )
        assert (
            chart_dependencies[chart]["version"]
            == dependencies[chart]["version"]
        )


@then("I will see the correctly deployed version string")
def confirm_version_id(version_id):
    """Confirm Version ID of the controller."""

    assert len(version_id.split(".")) == 3

    with open(
        "charts/ska-sdp/values.yaml", "r", encoding="utf-8"
    ) as values_file_raw:
        values_file = yaml.safe_load(values_file_raw)

    assert values_file["lmc"]["version"].startswith(version_id)

"""QA display tests."""

# pylint: disable=duplicate-code
# pylint: disable=too-many-locals
# pylint: disable=too-many-arguments

import asyncio
import gzip
import logging
import os
import re
import tempfile
import threading
import time

import msgpack
import pytest
import requests
import websockets
import yaml
from common.cbf_emulator import send_scans
from common.tango.base import DevError
from common.wait import wait_for_obs_state, wait_for_state
from pytest_bdd import given, parsers, scenarios, then, when

LOG = logging.getLogger(__name__)

scenarios("qa_display.feature")

# Mapping for the expected number of datasets
MAPPING_EXPECTED_TIMESTEPS = {"Mid": 1, "Low": 6}


@pytest.fixture(name="assign_resources_config")
def read_assign_resources_config(telescope_metadata):
    """
    Read the AssignResources config for visibility receive.

    Substitutes randomly-generated IDs so the test can be run more than once in
    the same instance of SDP. The config is used in more than one step in the
    test, so it must be a fixture.

    """

    config = telescope_metadata.assign_resources

    for pblock in config["processing_blocks"]:
        pblock["parameters"]["signal_display"] = {
            "metrics": ["all"],
            "nchan_avg": 25,
        }

    return config


# -----------
# Given steps
# -----------


# In integration/conftest.py:
# Given the volumes are created and the CBF emulator input data is copied
# Given I connect to an SDP subarray
# Given obsState is EMPTY
# Given I deploy the visibility receive script


@given("the SDP is running")
def sdp_is_running():
    """Do nothing."""


# In tests/conftest.py:
# Given I connect to the Queue Connector Device


@given(
    "the queue connector device is configured",
)
def queue_connector(context, subarray_device, queue_connector_device):
    """Check that the queue connector is configured"""
    wait_for_obs_state(
        subarray_device, "IDLE", timeout=context.timeouts.assign_res_timeout
    )
    wait_for_state(
        queue_connector_device,
        "ON",
        timeout=context.timeouts.device_on_off_timeout,
    )
    LOG.info("Started queue connector")


@given("the QA API is running")
def given_qa_api_servers_are_up(context):
    """Confirm the API is running."""
    confirm_qa_api_servers_are_up(context)


# ----------
# When steps
# ----------


@when("SDP is commanded to capture data from the Low telescope")
def start_scan_for_low_telescope(
    context,
    subarray_device,
    subarray_ready,
    k8s_element_manager,
    telescope_metadata,
):
    """Start a scan for the Low telescope."""
    send_scans(
        context,
        subarray_device,
        subarray_ready,
        k8s_element_manager,
        telescope_metadata,
        scans_and_ms=[(1, "science", telescope_metadata.data_dir[0])],
    )


@when("SDP is commanded to capture data from the Mid telescope")
def start_scan_for_tango_device(
    context,
    subarray_device,
    subarray_ready,
    k8s_element_manager,
    telescope_metadata,
):
    """
    Run the scan and configure the device."""
    send_scans(
        context,
        subarray_device,
        subarray_ready,
        k8s_element_manager,
        telescope_metadata,
        scans_and_ms=[(1, "science", telescope_metadata.data_dir[0])],
    )


@when(
    "I listen to the Queue Connector Device for QA Metrics",
    target_fixture="stats_thread",
)
def start_listening_for_qc_data(
    context,
    queue_connector_device,
    telescope_metadata,
):
    """Create a new thread to listen to the stats in the background."""

    def _internal_thread(
        context,
        queue_connector_device,
        telescope_metadata,
        thread_data,
    ):
        expected_time_steps = telescope_metadata.expected_time_steps
        run = True
        attributes = [
            "receiver_state",
            "last_update",
            "processing_block_id",
            "execution_block_id",
            "subarray_id",
            "scan_id",
            "payloads_received",
            "time_slices_received",
            "time_since_last_payload",
        ]
        start = time.time()
        first_stopped = None
        while (
            run
            and (time.time() - start)
            <= context.timeouts.wait_to_execute_timeout
        ):
            device_data = _get_all_attributes(
                queue_connector_device, attributes
            )
            missing = [attr for attr in attributes if attr not in device_data]
            if len(missing) > 0:
                time.sleep(0.5)
                continue

            # Ignore the initial data, if the state is currently "stopped".
            # This should be temporary until processing block specific topics
            # are used.
            if first_stopped is None:
                if device_data["receiver_state"] == "stopped":
                    first_stopped = device_data["processing_block_id"]
                else:
                    first_stopped = ""

            if device_data["processing_block_id"] == first_stopped:
                LOG.info("Ignoring this processing block")
                continue

            if device_data["receiver_state"] not in thread_data["data"]:
                thread_data["data"][device_data["receiver_state"]] = []

            thread_data["data"][device_data["receiver_state"]].append(
                device_data
            )

            run = device_data["time_slices_received"] < expected_time_steps
            if device_data["receiver_state"] != "unknown":
                LOG.info(
                    "Received %s with %d payloads, eb / pb = %s / %s",
                    device_data["receiver_state"],
                    device_data["payloads_received"],
                    device_data["execution_block_id"],
                    device_data["processing_block_id"],
                )
            time.sleep(0.1)

    LOG.info("Create background websocket task")
    thread_data = {"data": {}}
    thread = threading.Thread(
        target=_internal_thread,
        args=(
            context,
            queue_connector_device,
            telescope_metadata,
            thread_data,
        ),
    )
    thread.start()
    return thread, thread_data


@when("I wait for stats to finish collecting", target_fixture="device_data")
def wait_for_qc_stats_to_finish(stats_thread):
    """Wait for the stats thread to finish."""
    LOG.info("Waiting for stats thread to finish...")
    stats_thread[0].join()
    LOG.info("Stats thread finished")
    return stats_thread[1]["data"]


@when(
    parsers.parse("I query the config db endpoints for {config_type:S}"),
    target_fixture="api_output",
)
def get_config_db_data_for(context, config_type):
    """Get the data for a specific type from the Config DB"""
    output = _get_content_of_api(
        f"{context.sdp.qa_metric_host}/config/{config_type}"
    )

    data = {
        "list": output,
        "content": {
            d_id: _get_content_of_api(
                f"{context.sdp.qa_metric_host}/config/{config_type}/{d_id}"
            )
            for d_id in output
        },
    }
    return data


def _get_content_of_api(url, required_status=200):
    output = requests.get(url, timeout=30)
    assert output.status_code == required_status
    return output.json()


@when("I access each websocket", target_fixture="websocket_test_data")
def connect_to_each_websocket(context):
    """Attempt to connect to each websocket.

    This is a work around to Kafka not creating the topic on first use."""

    websocket_names = [
        "amplitude",
        "phase",
        "spectrum",
        "lagplot",
        "bandaveragedxcorr",
        "uvcoverage",
    ]

    websocket_output_data = {}

    for websocket_name in websocket_names:
        asyncio.run(
            _read_websocket_connection(
                context,
                websocket_name,
                websocket_output_data,
            )
        )
    return websocket_output_data


async def _read_websocket_connection(
    context, websocket_name, websocket_output_data
):
    base_url = f"ws://{context.sdp.qa_metric_host[7:]}"
    subarray_id = context.sdp.subarray_id
    url = f"{base_url}/ws/metrics-{websocket_name}-{subarray_id}"
    async with websockets.connect(
        url,
        max_size=1_000_000_000,
    ) as websocket:
        data = await websocket.recv()
        websocket_output_data[websocket_name] = msgpack.unpackb(data)


# ----------
# Then steps
# ----------


@then("the QA API is running")
def check_qa_api_servers_are_up(context):
    """Confirm the API is running."""
    confirm_qa_api_servers_are_up(context)


@then("QA metric data is available")
def check_data_exists(signal_display_websocket_data):
    """Confirm that some data was returned."""
    for name, data in signal_display_websocket_data["data"].items():
        assert (
            data["messages"] is not None
        ), f"Websocket data contains no data for {name}"
        assert (
            len(data["messages"]) > 0
        ), f"Websocket data contains no data for {name}"


@then(
    parsers.parse(
        "adheres to a schema that describes the format of the "
        "{graph_type:S} metric data and its associated metadata"
    )
)
def check_websocket_data(
    signal_display_websocket_data, graph_type
):  # pylint: disable=too-many-branches
    """Check the output of the websocket."""
    websocket_output_raw, timeout = (
        signal_display_websocket_data["data"][graph_type]["messages"],
        signal_display_websocket_data["data"][graph_type]["timeout"],
    )
    websocket_output_dict = [
        msgpack.unpackb(data) for data in websocket_output_raw
    ]
    for data in websocket_output_dict:
        del data["timestamp"]
        del data["processing_block_id"]

    expected_short_data = _get_expected_data(graph_type)
    len_actual, len_expected = len(websocket_output_dict), len(
        expected_short_data
    )

    # If the timeout happened, make sure that all the data received is from
    # the expected data set. Ignoring the order. And that we received at least
    # one piece of data.
    # There is currently a bug that is causing us to lose the first message,
    # but only on the first test, this should be able to be resolvable by
    # creating the topic earlier
    if timeout is True and len_actual != len_expected:
        LOG.warning("A timeout occurred so not checking all data")
        assert len_actual > 0
        checked = [False for i in range(len_actual)]
        for index, item in enumerate(websocket_output_dict):
            for expected_index, expected in enumerate(expected_short_data):
                if item == expected:
                    LOG.info("Received record %d of expected", expected_index)
                    checked[index] = True
        assert all(
            checked
        ), f"Not all Websocket Content was validated as correct [{graph_type}]"
    else:
        assert (
            len_actual == len_expected
        ), f"Websocket Content has a mismatch in length [{graph_type}]"

        # Note that this check is on purpose not to be in the assert.
        # There is a known issue in PyTest, where comparing 2 large sets of
        # data will cause pytest to hang on a failure.
        # https://github.com/pytest-dev/pytest/issues/8998
        check_sameness = websocket_output_dict == expected_short_data
        assert (
            check_sameness
        ), f"Websocket Content does not match expected content [{graph_type}]"


@then("the device responds with appropriate initial data")
def confirm_attributes_on_device(context, device_data, telescope_metadata):
    """Confirm whether we get the defaults."""

    expected_time_steps = telescope_metadata.expected_time_steps

    # get unique values:
    eb_id = set()
    pb_id = set()
    scan_id = set()
    subarrays = set()
    for items in device_data.values():
        eb_id.update(
            [
                item["execution_block_id"]
                for item in items
                if len(item["execution_block_id"]) > 0
            ]
        )
        pb_id.update(
            [
                item["processing_block_id"]
                for item in items
                if len(item["processing_block_id"]) > 0
            ]
        )
        scan_id.update(
            [item["scan_id"] for item in items if int(item["scan_id"]) > 0]
        )
        subarrays.update(
            [
                item["subarray_id"]
                for item in items
                if item["subarray_id"] != "-1"
            ]
        )

    # check that we got a start
    if "start" not in device_data and "new" not in device_data:
        LOG.warning("We didn't receive a new/start message")

    assert len(eb_id) == 1, "There must only be 1 Execution Block ID"
    assert len(pb_id) == 1, "There must only be 1 Processing Block ID"
    assert len(scan_id) == 1, "There must only be 1 Scan ID"
    assert len(subarrays) == 1, "There must only be 1 subarray"
    assert eb_id.pop().startswith("eb-test-"), "The EB ID is not expected"
    assert pb_id.pop().startswith("pb-test-"), "The PB ID is not expected"
    assert scan_id.pop() == 1, "The Scan ID is not expected"
    assert (
        subarrays.pop() == context.sdp.subarray_id
    ), "The Subarray ID is not expected"

    assert "receiving" in device_data
    assert len(device_data["receiving"]) > 0, "We must receive at least one"
    data = device_data["receiving"][-1]

    assert data["scan_id"] == 1
    assert data["subarray_id"] == context.sdp.subarray_id

    assert data["payloads_received"] > 0
    assert data["time_slices_received"] == expected_time_steps
    assert data["time_since_last_payload"] > 0.0


@then(
    parsers.parse("The correct data layouts are retrieved for {config_type:S}")
)
def check_api_config_output(config_type, api_output):
    """Check that the structure of the config DB endpoints looks correct"""
    if config_type == "subarrays":
        for subarray_id in api_output["list"]:
            assert len(subarray_id) == 2
            assert re.match(r"^[0-9]{2}$", subarray_id) is not None

        for subarray_id, content in api_output["content"].items():
            assert len(subarray_id) == 2
            assert re.match(r"^[0-9]{2}$", subarray_id) is not None
            assert list(content.keys()) == [
                "eb_id",
                "last_command",
                "last_command_time",
                "obs_state_commanded",
                "resources",
                "state_commanded",
            ]
    elif config_type == "execution_blocks":
        for eb_id in api_output["list"]:
            assert eb_id.startswith("eb-test-20")

        for eb_id, content in api_output["content"].items():
            assert eb_id.startswith("eb-test-20")
            assert list(content.keys()) == [
                "key",
                "beams",
                "channels",
                "context",
                "fields",
                "max_length",
                "pb_batch",
                "pb_realtime",
                "polarisations",
                "resources",
                "scan_types",
                "subarray_id",
            ]
    elif config_type == "processing_blocks":
        for pb_id in api_output["list"]:
            assert pb_id.startswith("pb-test-20")

        for pb_id, content in api_output["content"].items():
            assert pb_id.startswith("pb-test-20")
            assert list(content.keys()) == [
                "eb_id",
                "script",
                "channels_per_port",
                "processors",
                "signal_display_config",
            ]
    elif config_type == "deployments":
        for dep_id in api_output["list"]:
            assert dep_id.startswith("proc-pb-test-20")

        for dep_id, content in api_output["content"].items():
            assert dep_id.startswith("proc-pb-test-20")
            assert list(content.keys()) == ["key", "kind", "args"]


@then("Each websocket responds with a connected message")
def check_websocket_connect_data(websocket_test_data):
    """Check that for each topic we have a connected message."""

    websocket_names = [
        "amplitude",
        "phase",
        "spectrum",
        "lagplot",
        "bandaveragedxcorr",
        "uvcoverage",
    ]

    for name, data in websocket_test_data.items():
        assert name in websocket_names
        assert data == {"status": "connected"}


def _get_all_attributes(data_device, attributes):
    output = {}
    for attr in attributes:
        try:
            output[attr] = data_device.read_attribute(attr)
        except DevError:
            pass
    return output


def _create_yaml_file(values):
    """Convert a python dict to a yaml file."""
    with tempfile.NamedTemporaryFile(
        mode="w", suffix=".yaml", delete=False
    ) as file:
        file.write(yaml.dump(values))
    return file.name


def _get_expected_data(file_name):
    path = os.path.join("tests", "resources", "data", "qa-api", file_name)
    with gzip.open(f"{path}.msgpack.gz", mode="r") as infile:
        return msgpack.unpack(infile)


def confirm_qa_api_servers_are_up(context):
    """Confirm the API is running."""

    output = requests.get(f"{context.sdp.qa_metric_host}/ping", timeout=30)

    assert output.status_code == 200
    assert output.json() == {"ping": "live"}

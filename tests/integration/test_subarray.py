# pylint: disable=duplicate-code

"""Subarray device tests."""

import json
import time

from common.device_operations import (
    call_command_exception,
    read_command_argument,
    read_json_data,
    subarray_trigger_error_messages,
)
from common.wait import TIMEOUT, wait_for_admin_mode, wait_for_obs_state
from pytest_bdd import given, parsers, scenarios, then, when
from ska_control_model import AdminMode

CONFIGURED_TIME_TO_READY = 10


scenarios("subarray.feature")

# -----------
# Given steps
# -----------

# In tests/conftest.py:
# Given I connect to an SDP subarray


@given(parsers.parse("adminMode is {admin_mode:S}"))
def set_admin_mode(subarray_device, admin_mode):
    """
    Set the device admin mode.

    :param subarray_device: SDP subarray device client
    :param admin_mode: admin mode value

    """
    assert admin_mode in AdminMode.__members__
    subarray_device.write_attribute("adminMode", AdminMode[admin_mode])
    wait_for_admin_mode(subarray_device, admin_mode)

    yield subarray_device

    subarray_device.write_attribute("adminMode", AdminMode.ONLINE)
    wait_for_admin_mode(subarray_device, "ONLINE")


@given(parsers.parse("the state is {state:S}"))
def set_state(subarray_device, state):
    """
    Set the device state to the desired value.

    This function sets the obsState to EMPTY.

    :param subarray_device: SDP subarray device client
    :param state: desired device state

    """
    set_state_and_obs_state(subarray_device, state, "EMPTY")


@given(parsers.parse("obsState is {obs_state:S}"))
def set_obs_state(context, subarray_device, obs_state):
    """
    Set the obsState to the desired value.

    This function sets the device state to ON.

    :param context: context defined in conftest.py
    :param subarray_device: SDP subarray device client
    :param obs_state: desired obsState
    """
    set_state_and_obs_state(
        subarray_device,
        "ON",
        obs_state,
        assignres_timeout=context.timeouts.assign_res_timeout,
    )


# ----------
# When steps
# ----------


@when(parsers.parse("I call {command:S}"), target_fixture="command_exception")
def call_command(subarray_device, command, test_configure=False):
    """
    Call a device command.

    :param subarray_device: SDP subarray device client
    :param command: name of command to call
    :param test_configure: switch to set time_to_ready param
        for test-receive-addresses script, when Configure
        command transition from CONFIGURING to READY is tested
    """
    # Check command is present
    assert command in subarray_device.get_command_list()
    # Get the command argument
    if command in [
        "AssignResources",
        "ReleaseResources",
        "Configure",
        "Scan",
    ]:
        config = read_command_argument(command)
        if command == "AssignResources" and test_configure:
            # Only used with the test-receive-addresses script (v0.6.1+)
            config["processing_blocks"][0]["parameters"][
                "time_to_ready"
            ] = CONFIGURED_TIME_TO_READY

        argument = json.dumps(config)
    else:
        argument = None

    # Remember the EB ID
    if command == "AssignResources":
        subarray_device.eb_id = config["execution_block"]["eb_id"]
    elif command == "End":
        subarray_device.eb_id = None

    # Call the command and return the exception if raised
    return call_command_exception(subarray_device, command, argument)


@when(
    parsers.parse("I call {command:S} with an invalid JSON configuration"),
    target_fixture="command_exception",
)
def call_command_with_invalid_json(subarray_device, command):
    """
    Call a command with an invalid configuration.

    :param subarray_device: SDP subarray device client
    :param command: the name of the command

    """
    # Check command is present
    assert command in subarray_device.get_command_list()

    # Call the command and return the exception if raised
    return call_command_exception(subarray_device, command, argument="{}")


@when(
    parsers.parse("I call AssignResources with an invalid {entity:S} ID"),
    target_fixture="command_exception",
)
def call_assignres_with_invalid_id(subarray_device, entity):
    """
    Call the AssignResources command with an invalid ID.

    :param subarray_device: SDP subarray device client
    :param entity: the entity with invalid ID

    """
    command = "AssignResources"

    # Get the command argument and inject an invalid ID
    config = read_command_argument(command)
    if entity == "execution-block":
        config["execution_block"]["eb_id"] = "eb-wrong-eb-id-00000"
    elif entity == "processing-block":
        config["processing_blocks"][0]["pb_id"] = "pb-wrong-pb-id-00000"
    argument = json.dumps(config)

    # Call the command and return the exception if raised
    return call_command_exception(subarray_device, command, argument)


@when(
    parsers.parse(
        "I call AssignResources with malformed script of {script_problem}"
    ),
    target_fixture="command_exception",
)
def call_assignres_with_bad_script(subarray_device, script_problem):
    """
    Call the AssignResources command with
    incorrect processing script definition.

    :param subarray_device: SDP subarray device client
    :param script_problem: the problem we are testing

    """
    command = "AssignResources"

    # Get the command argument and inject a non-existent script
    config = read_command_argument(command)

    match script_problem:
        case "non-existent script definition":
            bad_script = {
                "kind": "batch",
                "name": "nothing",
                "version": "none",
            }
            params = {}
        case "incompatibly-versioned script":
            bad_script = {
                "kind": "realtime",
                "name": "vis-receive",
                "version": "4.2.0",
            }
            params = {}
        case "incompatible script parameters":
            bad_script = {
                "kind": "realtime",
                "name": "test-receive-addresses",
                "version": "1.0.0",
            }
            params = {"not-allowed-param": 10}

    config["processing_blocks"][0]["script"] = bad_script
    config["processing_blocks"][0]["parameters"] = params
    argument = json.dumps(config)

    # Call the command and return the exception if raised
    return call_command_exception(subarray_device, command, argument)


@when(
    "I call AssignResources with a script for testing the Configure transition"
)
def set_obs_state_configure(context, subarray_device):
    """
    Set the obsState to IDLE and set the time it takes
    for deployments_ready to reach "true" to TIME_TO_READY.

    :param context: context defined in conftest.py
    :param subarray_device: SDP subarray device client
    """
    call_command(subarray_device, "AssignResources", test_configure=True)
    wait_for_obs_state(
        subarray_device, "IDLE", timeout=context.timeouts.assign_res_timeout
    )


@when(parsers.parse("I call AssignResources and the processing block {mode}"))
def pb_run_with_errors(context, subarray_device, mode):
    """
    Run test-realtime script for different failure scenarios
    """
    match mode:
        case "is RUNNING but has error messages":
            simulated_mode = "failed_app"
        case "state becomes FAILED":
            simulated_mode = "failed_engine"

    subarray_trigger_error_messages(
        subarray_device, simulated_mode, context.timeouts.assign_res_timeout
    )


# ----------
# Then steps
# ----------


# In integration/conftest.py
# Then the state is {state:S}
# Then obsState is {obs_state:S}
# Then the device raises an {reason:S} exception


@then(parsers.parse("obsState becomes {obs_state:S}"))
def obs_state_becomes(context, subarray_device, obs_state):
    """
    Check the obsState becomes the expected value.

    :param context: context defined in conftest.py
    :param subarray_device: SDP subarray device client
    :param obs_state: the expected obsState

    """
    if obs_state == "IDLE":
        # AssignResources can take long to download script images and reach
        # IDLE obsState
        wait_for_obs_state(
            subarray_device,
            obs_state,
            timeout=context.timeouts.assign_res_timeout,
        )

    elif obs_state == "READY":
        start = time.monotonic()
        wait_for_obs_state(
            subarray_device, obs_state, timeout=CONFIGURED_TIME_TO_READY + 10
        )
        time_took_ready = time.monotonic() - start
        # this test that the transition to READY is not instantaneous
        assert CONFIGURED_TIME_TO_READY / 3.0 < time_took_ready

    else:
        wait_for_obs_state(subarray_device, obs_state)


@then("resources has the expected value")
def resources_expected(subarray_device):
    """
    Check resources has the expected value.

    :param subarray_device: SDP subarray device client

    """
    expected = read_command_argument("AssignResources")["resources"]
    assert json.loads(subarray_device.read_attribute("resources")) == expected


@then("resources is empty")
def resources_empty(subarray_device):
    """
    Check resources is empty.

    :param subarray_device: SDP subarray device client

    """
    assert json.loads(subarray_device.read_attribute("resources")) == {}


@then("ebID has the expected value")
def eb_id_expected(subarray_device):
    """
    Check ebID has the expected value.

    :param subarray_device: SDP subarray device client
    :param eb_id: Execution block ID

    """
    expected = subarray_device.eb_id
    assert subarray_device.read_attribute("ebID") == expected


@then("ebID is null")
def eb_id_null(subarray_device):
    """
    Check ebID is null.

    :param subarray_device: SDP subarray device client

    """
    assert subarray_device.read_attribute("ebID") == "null"


@then("receiveAddresses has the expected value")
def receive_addresses_expected(subarray_device):
    """
    Check receiveAddresses has the expected value.

    :param subarray_device: SDP subarray device client

    """
    # Get the expected receive addresses from the data file
    expected = read_receive_addresses()
    actual = json.loads(subarray_device.read_attribute("receiveAddresses"))
    assert actual.keys() == expected.keys()
    for scan_type_id, expected_scan_type in expected.items():
        if scan_type_id == "interface":
            continue
        actual_scan_type = actual[scan_type_id]
        assert actual_scan_type.keys() == expected_scan_type.keys()
        for beam_id, expected_beam in expected_scan_type.items():
            actual_beam = actual_scan_type[beam_id]
            assert actual_beam.keys() == expected_beam.keys()
            assert expected_beam["function"] == actual_beam["function"]
            expected_host = expected_beam["host"]
            actual_host = actual_beam["host"]
            assert len(actual_host) == len(expected_host)
            for i, host in enumerate(expected_host):
                assert actual_host[i][0] == host[0]
            assert expected_beam["port"] == actual_beam["port"]


@then("receiveAddresses is empty")
def receive_addresses_empty(subarray_device):
    """
    Check receiveAddresses is empty.

    :param subarray_device: SDP subarray device client

    """
    assert json.loads(subarray_device.read_attribute("receiveAddresses")) == {}


@then("scanType has the expected value")
def scan_type_expected(subarray_device):
    """
    Check the scanType attribute has the expected value.

    :param subarray_device: SDP subarray device client

    """
    # Get the expected scan type from the Configure argument
    expected = read_command_argument("Configure")["scan_type"]
    assert subarray_device.read_attribute("scanType") == expected


@then("scanType is null")
def scan_type_null(subarray_device):
    """
    Check the scanType attribute is null.

    :param subarray_device: SDP subarray device client

    """
    assert subarray_device.read_attribute("scanType") == "null"


@then("scanID has the expected value")
def scan_id_expected(subarray_device):
    """
    Check the scanID attribute has the expected value.

    :param subarray_device: SDP subarray device client

    """
    # Get the expected scan ID from the Scan argument
    expected = read_command_argument("Scan")["scan_id"]
    assert subarray_device.read_attribute("scanID") == expected


@then("scanID is 0")
def scan_id_zero(subarray_device):
    """
    Check the scanType attribute is empty.

    :param subarray_device: SDP subarray device client
    """
    assert subarray_device.read_attribute("scanID") == 0


@then("the errorMessages tango attribute is populated")
def error_msg_attribute(subarray_device):
    """
    Check the errorMessages tango attribute contains data

    :param subarray_device: SDP subarray device client
    """
    assert subarray_device.read_attribute("errorMessages")


# -------------------
# Ancillary functions
# -------------------


def read_receive_addresses():
    """Read receive addresses from JSON file."""
    return read_json_data("receive_addresses.json")


def transition_commands(current, target):
    """
    Get list of commands to transition from current state to target state.

    :param current: tuple of current state and obs_state
    :param target: tuple of target state and obs_state
    :returns: list of commands

    """
    # Mapping of state and obs_state to state number
    state_number = {
        ("OFF", "EMPTY"): 0,
        ("ON", "EMPTY"): 1,
        ("ON", "IDLE"): 2,
        ("ON", "READY"): 3,
        ("ON", "SCANNING"): 4,
    }
    # Command to transition to previous state number
    command_prev = [None, "Off", "ReleaseResources", "End", "EndScan"]
    # Command to transition to next state number
    command_next = ["On", "AssignResources", "Configure", "Scan", None]

    current_number = state_number[current]
    target_number = state_number[target]

    if target_number < current_number:
        commands = [
            command_prev[i] for i in range(current_number, target_number, -1)
        ]
    elif target_number > current_number:
        commands = [
            command_next[i] for i in range(current_number, target_number)
        ]
    else:
        commands = []

    return commands


def set_state_and_obs_state(
    device, state, obs_state, assignres_timeout=TIMEOUT
):
    """
    Set subarray device state and observing state.

    :param device: subarray device proxy
    :param state: the desired device state
    :param obs_state: the desired observing state
    :param assignres_timeout: timeout specifically set for AssignResources [s]
    """
    current = (
        device.read_attribute("State"),
        device.read_attribute("obsState"),
    )
    target = (state, obs_state)
    commands = transition_commands(current, target)

    for command in commands:
        call_command(device, command)
        if command == "AssignResources":
            wait_for_obs_state(device, "IDLE", timeout=assignres_timeout)

    assert device.read_attribute("State") == state
    assert device.read_attribute("obsState") == obs_state

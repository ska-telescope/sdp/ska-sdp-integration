"""Component status tests."""

import json

from common.device_operations import (
    call_command_exception,
    read_command_argument,
)
from common.kubernetes import scale_component
from pytest_bdd import parsers, scenarios, then, when

scenarios("component_status.feature")

# ----------
# Given steps
# ----------

# In integration/conftest.py:
# Given I connect to the SDP controller
# Given I connect to an SDP subarray
# Given obsState is EMPTY

# ----------
# When steps
# ----------


@when(parsers.parse("the SDP component {component:S} is unavailable"))
def set_component(context, component):
    """
    Set the state of an SDP processing component.

    :param component: name of processing component
    """
    with scale_component(
        component, context.sdp.namespace, context.timeouts.wait_for_pod_timeout
    ):
        yield


# ----------
# Then steps
# ----------

# in integration/contest.py
# Then the subarray healthState is <health_state>


@then(parsers.parse("the component {component:S} has the status {status:S}"))
def component_status(controller_device, component, status):
    """
    Check the SDP processing component has the expected status.

    :param controller_device: SDP controller device client
    :param component: SDP component to check
    :param status: expected status

    """
    json_status = json.loads(controller_device.read_attribute("components"))
    assert json_status[component]["status"] == status


@then(parsers.parse("the controller healthState is {state:S}"))
def controller_health_state_is(controller_device, state):
    """
    Check the device attribute healthState.

    :param controller_device: SDP controller device client
    :param state: expected healthState value
    """
    assert controller_device.read_attribute("healthState") == state


@then(
    parsers.parse("the AssignResources command raises an {reason:S} exception")
)
def assign_resources_refused(subarray_device, reason):
    """
    Check that the AssignResources command is rejected.

    :param subarray_device: SDP subarray device client
    :param reason: reason for exception
    """
    command = "AssignResources"
    config = read_command_argument(command)
    argument = json.dumps(config)
    command_exception = call_command_exception(
        subarray_device, command, argument
    )
    assert command_exception is not None
    assert command_exception.reason == reason

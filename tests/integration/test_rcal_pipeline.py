"""Realtime calibration pipeline tests"""

# pylint: disable=import-error
# pylint: disable=unused-import
# pylint: disable=duplicate-code

import logging
import math
import threading
import time

import msgpack
import numpy
import pytest
from common.cbf_emulator import send_scans
from common.tango import DevError, TangoClient, tango_client
from common.wait import wait_for_obs_state, wait_for_predicate, wait_for_state
from pytest_bdd import given, scenario, then, when

LOG = logging.getLogger(__name__)

BANDPASS_TOPIC = "bandpass-gains"
GAIN_QC_TANGO_ATTRIBUTE = "gain_attr"


@pytest.mark.skip("RCAL needs to adopt data flows")
@scenario(
    "rcal_pipeline.feature", "Visibility receive with bandpass calibration"
)
def test_rcal_pipeline():
    """Test running RCAL and vis-receive together."""


class GainValuePoller(threading.Thread):
    """
    A thread that polls for the gain attribute of the Queue Connector and keeps
    a copy of the last value seen before the attribute disappears.
    """

    def __init__(self, queue_connector: TangoClient):
        self.stop = False
        self.queue_connector = queue_connector
        self.gains = []
        super().__init__()

    def gain_attr_exists(self) -> bool:
        """Whether the gain attribute exists"""
        return (
            GAIN_QC_TANGO_ATTRIBUTE
            in self.queue_connector.get_attribute_list()
        )

    def run(self) -> None:
        while not self.stop:
            try:
                gains = self.queue_connector.read_attribute(
                    GAIN_QC_TANGO_ATTRIBUTE
                )
            except DevError:
                # doesn't exist anymore
                break
            self.gains = gains
            time.sleep(0.1)


@pytest.fixture(name="gain_values_poller")
def _gain_values_poller_fixture(queue_connector):
    poller = GainValuePoller(queue_connector)
    poller.start()
    yield poller
    poller.join()


@pytest.fixture(name="assign_resources_config")
def read_assign_resources_config(
    context, telescope_metadata
):  # pylint: disable=unused-argument
    """
    Read the AssignResources config for visibility receive and RCal processor

    """

    config = telescope_metadata.assign_resources

    kafka_topic = f"{BANDPASS_TOPIC}-{context.sdp.subarray_id}"

    for pblock in config["processing_blocks"]:
        pblock["parameters"]["queue_connector_configuration"] = {
            "exchanges": [
                _build_calibration_exchange_config(
                    context, GAIN_QC_TANGO_ATTRIBUTE, kafka_topic, config
                )
            ],
        }
        pblock["parameters"]["processors"]["rcal"]["args"][
            1
        ] = f"--kafka-topic={kafka_topic}"

    return config


# -----------
# Given steps
# -----------

# In integration/conftest.py:
# Given I connect to an SDP subarray
# Given obsState is EMPTY
# Given I select the dataset for the low rcal pipeline
# Given the volumes are created and the CBF emulator input data is copied


@given("I deploy the visibility receive script configured to calibrate")
# pylint: disable-next=unused-argument
def deploy_script(context, vis_receive_script, subarray_device):
    """
    Deploy visibility receive script with calibration support.

    This uses the vis_receive_script fixture to automatically end the
    script when the test is finished.

    :param context: test context defined in conftest.py
    :param vis_receive_script: visibility receive script fixture
    :param subarray_device: SDP subarray device client
    """
    # Check the obsState becomes the expected value
    wait_for_obs_state(
        subarray_device, "IDLE", timeout=context.timeouts.assign_res_timeout
    )


@given(
    "the queue connector is configured to receive cal solutions from"
    " appropriate data queues",
    target_fixture="queue_connector",
)
def queue_connector_fxt(context):
    """Configure the queue connector to receive cal solutions from Kafka"""
    client = tango_client(
        context.sdp.namespace,
        context.sdp.queue_connector_device_name,
    )
    wait_for_state(
        client, "ON", timeout=context.timeouts.device_on_off_timeout
    )
    LOG.info("Started queue connector")
    return client


def _build_calibration_exchange_config(
    context, source_attr: str, target_topic: str, config
):
    # get the expected shape of the buffer
    # complex gains are sent as floats, so double the length of the last dim
    shape = _gains_shape(config)
    shape[2] *= 2
    return {
        "dtype": "float64",
        "shape": shape,
        "source": {
            "type": "KafkaConsumerSource",
            "servers": context.sdp.kafka_host,
            "topic": target_topic,
            "encoding": "npy",
        },
        "sink": {
            "type": "TangoArrayScatterAttributeSink",
            "attribute_names": [source_attr],
            "default_value": 0.0,
        },
    }


# -----------
# When steps
# -----------


@when(
    "SDP is commanded to capture data from the Low telescope",
    target_fixture="gains",
)
# pylint: disable-next=too-many-positional-arguments,too-many-arguments
def command_calibration_scan(
    context,
    subarray_device,
    subarray_ready,
    k8s_element_manager,
    gain_values_poller,
    telescope_metadata,
):
    """
    Run a sequence of scans.

    :param context: context for the tests
    :param telescope: the SKA telescope for which to emulate data sending
    :param subarray_device: SDP subarray device client
    :param subarray_ready: subarray fixture
    :param k8s_element_manager: Kubernetes element manager

    """
    LOG.info("Sending scans")
    assert gain_values_poller.gain_attr_exists()

    # send the same data for each scan
    ms_data = telescope_metadata.data_dir[0]

    # pylint: disable-next=duplicate-code
    send_scans(
        context,
        subarray_device,
        subarray_ready,
        k8s_element_manager,
        telescope_metadata,
        [(1, "calibration", ms_data)],  # pylint: disable=duplicate-code
        sender_version="5.0.0",
    )
    # After the End command is issued the queue connector configuration is
    # cleared from the config DB, which results in the attribute disappearing
    wait_for_predicate(
        lambda: not gain_values_poller.gain_attr_exists(),
        "gain attribute removal",
        timeout=10,
        interval=0.05,
    )
    return gain_values_poller.gains


# -----------
# Then steps
# -----------


@then("the published gains are correct")
def check_bandpass_solutions(gains, assign_resources_config):
    """
    Check final bandpass solutions.

    :param gains: fixture for the gains output of the test step which
    captures data
    :param assign_resources_config: fixture for the assign resources
    config string
    """
    LOG.info("Testing received bandpass data")

    gains_buffer = gains

    # get the expected shape of the buffer
    shape = _gains_shape(assign_resources_config)
    n_ant, n_freq, n_pol = shape

    # reform gain matrices as follows:
    gains = numpy.array(gains_buffer).view(numpy.complex128).reshape(shape)

    assert len(gains_buffer) == n_ant * n_freq * n_pol * 2
    assert len(gains_buffer) % n_pol * 2 == 0
    assert n_pol == 4  # this is assumed in the loop below
    for ant in range(shape[0]):
        # skip the first and last channel, as they can be affected by
        # rechannelisation effects
        for chan in range(1, shape[1] - 1):
            for pol in range(0, shape[2]):
                k = (ant * shape[1] + chan) * shape[2] * 2 + pol * 2
                # check that the real part of the diagonal elements is 1
                if pol in (0, 3):
                    assert abs(gains_buffer[k] - 1) < 1e-12
                else:
                    assert abs(gains_buffer[k]) < 1e-12
                # check that the imag part of all elements is 0
                assert abs(gains_buffer[k + 1]) < 1e-12


def _gains_shape(config):
    """
    Determine the bandpass calibration table shape from the AssignResources
    config.

    """

    num_antennas = len(config["resources"]["receptors"])
    nun_pol = len(config["execution_block"]["polarisations"][0]["corr_type"])
    spw = config["execution_block"]["channels"][0]["spectral_windows"][0]
    freq_min = spw["freq_min"]
    freq_max = spw["freq_max"]

    # re-channelisation output as in ska_sdp_func_python function
    # calibration.beamformer_utils.set_beamformer_frequencies
    dfrequency_bf = 781.25e3
    num_freq_bf = int(math.ceil((freq_max - freq_min) / dfrequency_bf))

    gains_shape = [num_antennas, num_freq_bf, nun_pol]
    LOG.info("Expected gains shape: %r", gains_shape)
    return gains_shape


@then(
    "QA metric data adheres to a schema that describes the format of the "
    "metric data"
)
def check_websocket_data(signal_display_websocket_data):
    """Check the output of the websocket."""
    assert signal_display_websocket_data is not None
    websocket_output_raw = signal_display_websocket_data["data"][
        "gain_calibration_out"
    ]["messages"]
    assert len(websocket_output_raw) > 0
    websocket_output_dict = [
        msgpack.unpackb(data) for data in websocket_output_raw
    ]

    for data in websocket_output_dict:
        assert "time" in data
        assert "gains" in data
        assert "phases" in data

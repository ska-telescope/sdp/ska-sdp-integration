"""
SDP tests with SKA Tango Alarm Handler

Chart repository: https://gitlab.com/ska-telescope/ska-tango-alarmhandler
Elettra Alarm Handler docs:
https://gitlab.elettra.eu/cs/ds/alarm-handler/-/blob/master/docs/AlarmHandler_Doc.pdf
"""

import logging

from common.device_operations import (
    set_state_and_obs_state,
    subarray_trigger_error_messages,
)
from common.kubernetes import scale_component
from common.tango import tango_client
from common.wait import wait_for_predicate
from pytest_bdd import given, parsers, scenarios, then, when

LOG = logging.getLogger(__name__)

CONTROLLER_ALARM = "sdp_healthstate_degraded"
SUBARRAY_HEALTHSTATE_ALARM = "sdp_subarray_healthstate_degraded"
SUBARRAY_OBSSTATE_ALARM = "sdp_subarray_obsstate_fault"

scenarios("alarm_handler.feature")


@given(
    parsers.parse(
        "I connect to the SKA alarm handler and configure "
        "it for {alarm_attribute:S} alarm"
    ),
    target_fixture="alarm_handler",
)
def connect_to_alarm_handler(context, alarm_attribute):
    """
    Connect to the Alarm Handler device.

    :param context: context for tests
    :param alarm_attribute: attribute on the alarm handler
        that needs configuring
    :returns: Alarm handler device client
    """

    alarm_handler = tango_client(
        context.sdp.namespace,
        context.alarm_handler.alarm_handler_device_name,
    )

    alarm_settings = {
        CONTROLLER_ALARM: f"tag={CONTROLLER_ALARM};formula="
        f"{context.sdp.controller_device_name}/healthstate == 1;"
        "priority=warning;group=none;message=SDP health state is DEGRADED",
        SUBARRAY_HEALTHSTATE_ALARM: f"tag={SUBARRAY_HEALTHSTATE_ALARM};"
        f"formula={context.sdp.subarray_device_name}/"
        f"healthstate == 1;priority=warning;group=none;"
        "message=SDP subarray health state is DEGRADED",
        SUBARRAY_OBSSTATE_ALARM: f"tag={SUBARRAY_OBSSTATE_ALARM};formula="
        f"{context.sdp.subarray_device_name}/obsstate == 9;"
        "priority=fault;group=none;"
        "message=SDP subarray observing state is FAULT",
    }

    handler_attributes = alarm_handler.get_attribute_list()
    if alarm_attribute not in handler_attributes:
        alarm_handler.execute_command(
            "Load", argument=alarm_settings[alarm_attribute]
        )
        assert alarm_handler.read_attribute(alarm_attribute) == 0

    yield alarm_handler

    alarm_handler.execute_command("Ack", argument=[alarm_attribute])
    alarm_handler.execute_command("Remove", argument=alarm_attribute)


@when(parsers.parse("the controller's {attribute:S} is {status:S}"))
def controller_attribute(
    context,
    controller_device,
    attribute,
    status,
):
    """Trigger a change that moves the controller healthState to DEGRADED"""
    with scale_component(
        "helmdeploy",
        context.sdp.namespace,
        context.timeouts.wait_for_pod_timeout,
    ):
        assert controller_device.read_attribute(attribute) == status
        yield


@when(parsers.parse("the subarray's {attribute:S} is {status:S}"))
def subarray_attribute(context, subarray_device, attribute, status):
    """Trigger a change that moves the subarray obsState to FAULT"""
    if status == "DEGRADED":
        component = "helmdeploy"

        with scale_component(
            component,
            context.sdp.namespace,
            context.timeouts.wait_for_pod_timeout,
        ):
            assert subarray_device.read_attribute(attribute) == status
            yield

    else:
        set_state_and_obs_state(subarray_device, "ON", "EMPTY")
        subarray_trigger_error_messages(
            subarray_device,
            "failed_engine",
            context.timeouts.assign_res_timeout,
        )
        assert subarray_device.read_attribute(attribute) == status
        yield


@then(
    parsers.parse(
        "the alarm handler's {alarm_attribute:S} alarm "
        "is raised with priority {priority:S}"
    )
)
def ah_attr(alarm_handler, alarm_attribute, priority):
    """
    Relevant alarm states:
    NORM - 0
    UNACK - 1 : when an alarm is raised but not yet acknowledged
    ACKED - 2: in alarm but acknowledged
    RTNUN - 3: alarm returned to normal without being acknowledged
    For more information, see link to Elettra docs in the module docstrings
    """
    _wait_for_alarm(alarm_handler, alarm_attribute)
    assert alarm_handler.read_attribute(alarm_attribute) == 1

    # there should be only one alarm, for the given attribute
    alarm_data = alarm_handler.read_attribute("alarm")[0].split("\t")

    assert alarm_data[2] == alarm_attribute
    assert alarm_data[3] == "ALARM"
    assert alarm_data[5] == priority


def _wait_for_alarm(alarm_handler, attr):
    def predicate():
        return alarm_handler.read_attribute(attr) == 1

    description = f"Alarm handler {attr} alarm"
    wait_for_predicate(predicate, description)

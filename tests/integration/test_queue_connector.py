"""Tests for Data Queue Connector Tango Device."""

import logging

from pytest_bdd import scenarios, then, when

LOG = logging.getLogger(__name__)

scenarios("queue_connector.feature")

# -----------
# Given steps
# -----------

# In tests/conftest.py:
# Given I connect to the Queue Connector Device

# -----------
# When steps
# -----------


@when("I query list of available commands", target_fixture="command_list")
def get_command_list(queue_connector_device):
    """Get list of available commands."""
    return queue_connector_device.get_command_list()


# -----------
# Then steps
# -----------


@then("the expected command list is returned")
def check_expected_command_list(command_list):
    """Check that required commands exist."""
    correct_commands = [
        "Init",
        "State",
        "Status",
    ]
    assert set(correct_commands) == set(command_list)

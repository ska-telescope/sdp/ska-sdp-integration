"""Controller device tests."""

import logging

from common.device_operations import call_command_exception
from common.wait import wait_for_admin_mode
from pytest_bdd import given, parsers, scenarios, then, when
from ska_control_model import AdminMode

LOG = logging.getLogger(__name__)

scenarios("controller.feature")

# -----------
# Given steps
# -----------

# In integration/conftest.py:
# Given I connect to the SDP controller


@given(parsers.parse("adminMode is {admin_mode:S}"))
def set_admin_mode(controller_device, admin_mode):
    """
    Set the device admin mode.

    :param controller_device: SDP controller device client
    :param admin_mode: admin mode value

    """
    assert admin_mode in AdminMode.__members__
    controller_device.write_attribute("adminMode", AdminMode[admin_mode])
    wait_for_admin_mode(controller_device, admin_mode)

    yield controller_device

    controller_device.write_attribute("adminMode", AdminMode.ONLINE)
    wait_for_admin_mode(controller_device, "ONLINE")


@given(parsers.parse("the state is {state:S}"))
def set_state(controller_device, state):
    """
    Set the device state.

    :param controller_device: SDP controller device client
    :param initial_state: desired device state

    """
    # Set the device state if incorrect
    LOG.info("set state to %s", state)
    if controller_device.read_attribute("State") != state:
        # Call command to put device into the desired state
        controller_device.execute_command(state)


# ----------
# When steps
# ----------


@when(parsers.parse("I call {command:S}"), target_fixture="command_exception")
def call_command(controller_device, command):
    """
    Call a command.

    :param controller_device: SDP controller device client
    :param command: name of command to call

    """
    # Check command is present
    assert command in controller_device.get_command_list()
    # Call the command and return exception if raised
    return call_command_exception(controller_device, command)


# ----------
# Then steps
# ----------


# In integration/conftest.py
# Then the device raises an {reason:S} exception


@then(parsers.parse("the state is {state:S}"))
def state_is(controller_device, state):
    """
    Check the device state.

    :param controller_device: SDP controller device client
    :param state: expected state value

    """
    assert controller_device.read_attribute("State") == state

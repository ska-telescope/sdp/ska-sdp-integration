"""Pointing pipeline tests"""

import logging
import math
import tempfile

import msgpack
import numpy
import pytest
import yaml
from common.cbf_emulator import send_scans
from common.device_operations import create_id
from common.kubernetes import consume_response, k8s_pod_exec
from common.tango import tango_client
from common.wait import wait_for_predicate, wait_for_state
from pytest_bdd import given, scenario, then, when

LOG = logging.getLogger(__name__)

COMMANDED_POINTINGS_TOPIC = "commanded-pointings"
ACTUAL_POINTINGS_TOPIC = "actual-pointings"
SOURCE_OFFSETS_TOPIC = "source-offsets"
POINTING_OFFSET_TOPIC = "pointing-offsets"
POINTING_OFFSET_ATTR = "pointing_offset"


@pytest.mark.xfail(
    reason="Intermittent failures caused by various things: "
    "pointing pipeline, queue connector, qa api. "
    "Under investigation in PI25."
)
@scenario("pointing_pipeline.feature", "Visibility receive with pointings")
def test_pointing_pipeline():
    """Test running pointing and vis-receive together."""


@pytest.fixture(name="assign_resources_config")
def read_assign_resources_config(context, telescope_metadata):
    """
    Read the AssignResources config for visibility receive.

    Substitutes randomly-generated IDs so the test can be run more than once in
    the same instance of SDP. The config is used in more than one step in the
    test, so it must be a fixture.

    """
    config = telescope_metadata.assign_resources
    point_pb_id = create_id("pb")

    # there should be only one pb in the default string, which is vis-receive
    pblock = config["processing_blocks"][0]
    vis_rec_id = pblock["pb_id"]
    mswriter_processor_overrides = {
        "args": [
            "realtime.receive.processors.sdp."
            "mswriter_processor.MSWriterProcessor",
            "--plasma_socket",
            "/plasma/socket",
            "--readiness-file=/tmp/processor_ready",
            f"--commanded-pointing-topic={COMMANDED_POINTINGS_TOPIC}",
            f"--actual-pointing-topic={ACTUAL_POINTINGS_TOPIC}",
            f"--source-offset-topic={SOURCE_OFFSETS_TOPIC}",
            "output.ms",
        ]
    }
    pblock["parameters"]["processors"] = {
        "mswriter": mswriter_processor_overrides
    }
    pblock["parameters"]["telstate"] = {
        "target_fqdn": "tango://test-sdp/mockdishleafnode/"
        "{dish_id}/desiredPointing",
        "source_offset_fqdn": "tango://test-sdp/mockdishleafnode/"
        "{dish_id}/sourceOffset",
        "direction_fqdn": "tango://test-sdp/mockdishmaster/"
        "{dish_id}/achievedPointing",
    }
    pblock["dependencies"] = [
        {"pb_id": point_pb_id, "kind": ["pointing-offset"]}
    ]

    # add pointing processing block
    pointing_pb = _add_pointing_pb(
        context.sdp.subarray_id, vis_rec_id, point_pb_id
    )
    config["processing_blocks"].append(pointing_pb)

    return config


def _add_pointing_pb(subarray_id, vis_rec_id, pointing_pb_id):
    pointing_pb = {
        "pb_id": pointing_pb_id,
        "script": {
            "kind": "realtime",
            "name": "pointing-offset",
            "version": "1.0.0",
        },
        "parameters": {
            "version": "1.0.0",
            "num_scans": 5,
            "additional_args": [
                "--thresh_width",
                "1.15",
                "--bw_factor",
                "0.95",
                "0.95",
                "--num_chunks",
                "8",
                "--use_modelvis",
                "--use_source_offset_column",
            ],
            "kafka_topic": f"{POINTING_OFFSET_TOPIC}-{subarray_id}",
        },
        "dependencies": [{"pb_id": vis_rec_id, "kind": ["vis-receive"]}],
    }
    return pointing_pb


# -----------
# Given steps
# -----------

# in integration/conftest.py
# Given I connect to an SDP subarray
# Given I select the dataset for the Mid pointing pipeline
# Given the volumes are created and the CBF emulator input data is copied
# Given obsState is EMPTY
# Given I deploy the visibility receive script configured to read pointings


@given(
    "mock dish devices are deployed with pointing data",
    target_fixture="mock_dish_list",
)
# pylint: disable-next=too-many-locals
def deploy_mock_dishes(context, k8s_element_manager, assign_resources_config):
    """Deploys mock dish devices with pointing data"""
    chart_name = "mock-dish"
    chart_dir = "tests/resources/charts/mock-dish"
    LOG.info("Deploying chart from %s as %s", chart_dir, chart_name)
    # Make sure mock-dish is installed with the correct domain and pvc
    domain = context.sdp.subarray_device_name.split("/subarray")[0]
    values = {
        "pvc": {"name": context.sdp.pvc_name},
        "mock_dish": {
            "prefix": domain.removesuffix("-sdp"),
            "receptors": assign_resources_config["resources"]["receptors"],
        },
    }
    with tempfile.NamedTemporaryFile(
        mode="w", suffix=".yaml", delete=False
    ) as file:
        file.write(yaml.dump(values))
    k8s_element_manager.helm_install(
        chart_name, chart_dir, context.sdp.namespace, file.name
    )

    # data are copied together with MS data and extracted
    # from a single tar file during the
    # "Given the volumes are created and the CBF
    # emulator input data is copied" step

    # Persistent volume location is shared by all mocks
    antenna_ids = assign_resources_config["resources"]["receptors"]
    # Device names given without domain (which is read from the TangoDB later)
    devices = [
        tango_client(
            context.sdp.namespace,
            f"{domain}/mockdishmaster/{antenna_id}",
        )
        for antenna_id in antenna_ids
    ]
    devices += [
        tango_client(
            context.sdp.namespace,
            f"{domain}/mockdishleafnode/{antenna_id}",
        )
        for antenna_id in antenna_ids
    ]
    for device in devices:
        wait_for_state(
            device, "OFF", timeout=context.timeouts.device_on_off_timeout
        )

    return devices


@given(
    "the queue connector is configured to write pointings to"
    " appropriate data queues",
    target_fixture="queue_connector_device",
)
def queue_connector_fxt(context):
    """Configure the queue connector to write pointing data to Kafka"""
    client = tango_client(
        context.sdp.namespace,
        context.sdp.queue_connector_device_name,
    )
    wait_for_state(
        client, "ON", timeout=context.timeouts.device_on_off_timeout
    )
    LOG.info("Started queue connector")
    return client


# -----------
# When steps
# -----------


@when(
    "SDP is commanded to capture data from the Mid telescope",
    target_fixture="pointing_results",
)
# pylint: disable=too-many-positional-arguments
def command_pointing_scan(  # pylint: disable=too-many-arguments
    context,
    subarray_device,
    subarray_ready,  # noqa: F811, pylint: disable=redefined-outer-name
    k8s_element_manager,
    telescope_metadata,
    mock_dish_list,
    queue_connector_device,
):
    """
    Run a sequence of scans.

    :param context: context for the tests
    :param subarray_device: SDP subarray device client
    :param subarray_ready: subarray fixture
    :param k8s_element_manager: Kubernetes element manager
    :param telescope_metadata: fixture for storing and managing metadata
    :param mock_dish_list: fixture containing list of mock dish devices
    :param queue_connector_device: queue connector device fixture
    """

    def _get_pointings():
        attributes = [
            f"{POINTING_OFFSET_ATTR}_{dish_id}"
            for dish_id in telescope_metadata.receptors
        ]
        # need to wait for pointing pipeline to finish processing
        # when that is done, the QC device attribute will have the correct data
        pointing_data = [queue_connector_device.read_attribute(attributes[0])]

        # the default values on the queue connector attribute are nan
        # once the values change and are no longer nan, we can load them
        if (
            isinstance(pointing_data[0], numpy.ndarray) or pointing_data[0]
        ) and not math.isnan(pointing_data[0][0]):
            for attr in attributes[1:]:
                pointing_data.append(
                    queue_connector_device.read_attribute(attr)
                )
            LOG.info(
                "Pointing offsets on the QueueConnector device: %s",
                pointing_data,
            )

        else:
            # if we do not yet have the right data,
            # reset it to None for predicate purposes
            pointing_data = None

        return pointing_data

    def _wait_for_valid_pointings():
        return wait_for_predicate(
            _get_pointings, "Obtaining data from QueueConnector attribute"
        )

    scans = [
        (scan_id + 1, "pointing", ms_data)
        for scan_id, ms_data in enumerate(telescope_metadata.data_dir)
    ]

    return send_scans(
        context,
        subarray_device,
        subarray_ready,
        k8s_element_manager,
        telescope_metadata,
        scans,
        dish_list=mock_dish_list,
        before_end_callback=_wait_for_valid_pointings,
    )


# -----------
# Then steps
# -----------


@then("the data received contains the correct pointing data")
def check_measurement_set_pointings(
    context,
    dataproduct_directory,  # noqa: F811, pylint: disable=redefined-outer-name
    k8s_element_manager,
):
    """
    Check that each Measurement Set corresponds to the scan ID it indicates in
    its name.

    :param context: context for the tests
    :param dataproduct_directory: The directory where outputs are written
    :param k8s_element_manager: Kubernetes element manager
    """
    receive_pod = f"receive-data-{context.sdp.subarray_id}"
    receive_container = "data-prep"

    # Add data product directory to k8s element manager for cleanup
    # pylint: disable=duplicate-code
    parse_dir = dataproduct_directory.index("ska-sdp")
    data_eb_dir = dataproduct_directory[:parse_dir]
    k8s_element_manager.output_directory(
        data_eb_dir,
        receive_pod,
        receive_container,
        context.sdp.namespace_sdp,
    )

    # Note that this code will actually run before the receiver has completed
    # its cleanup, but since we use casacore to open the MS in this script
    # it will block until the write lock is removed (when the MS is closed)
    with open(
        "tests/resources/scripts/print_pointings.py", encoding="utf-8"
    ) as file:
        script = file.read()

    cmd = [
        "python",
        "-c",
        script,
        f"/mnt/data/{dataproduct_directory}/output.scan-1.ms",
        "/mnt/data/test_data/pointing-hdfs/source_offset_scan-1.hdf",
        # shape to resize pointing data in MS to;
        # for some reason the mock dishes send an extra timestamp with
        # close-to zero offset values for every dish;
        # 4 -> nantennas; 6 -> ntimes, including the additional one,
        # 2 = (xel, el)
        "4, 6, 2",
    ]
    resp = k8s_pod_exec(
        cmd,
        receive_pod,
        receive_container,
        context.sdp.namespace_sdp,
        stdin=False,
    )
    consume_response(resp)
    assert resp.returncode == 0


@then(
    "the queue connector device contains the correct "
    "pointing calibration results"
)
def check_queueconnector_pointings(pointing_results, telescope_metadata):
    """
    Check pointing results on the QueueConnector device

    The result's dimensions are [nant, 13]; comes in as a list
    where nant is the number of antennas (receptors).
    The 13 attributes per antenna are explained here:
    https://developer.skao.int/projects/ska-sdp-wflow-pointing-offset/en/latest/#pipeline-outputs

    With the test data set none of the values should be zero
    """
    receptors = telescope_metadata.receptors
    assert len(pointing_results) == len(receptors)

    for pointing_data in pointing_results:
        # expected data: [scan_id, xel_offset, el_offset]
        assert len(pointing_data) == 3
        # the first value is the ID of the last scan, which
        # is 5 for every dish in this test
        assert pointing_data[0] == 5.0
        assert all(float(point) != 0.0 for point in pointing_data[1:])

        # convert to arcsec, data in tango attribute is in degrees
        xel_arcsec = 3600.0 / (1.0 / pointing_data[1])
        el_arcsec = 3600.0 / (1.0 / pointing_data[2])
        el_rad = numpy.deg2rad(pointing_data[2])

        # convert xel offset to azimuth offset, which is input for sims
        # input az_offset = 7 arcsec; input el_offset = 4.5 arcsec
        az_arcsec = xel_arcsec / numpy.cos(el_rad)

        assert math.isclose(
            abs(az_arcsec), 7.0, rel_tol=0.001
        ), f"Computed az_offset [arcsec]: {az_arcsec}"
        assert math.isclose(
            abs(el_arcsec), 4.5, rel_tol=0.001
        ), f"Computed el_offset [arcsec]: {el_arcsec}"


@then(
    "QA metric data adheres to a schema that describes the format of the "
    "metric data"
)
def check_websocket_data(signal_display_websocket_data):
    """Check the output of the websocket."""
    assert signal_display_websocket_data is not None
    LOG.info(signal_display_websocket_data)
    websocket_output_raw = signal_display_websocket_data["data"][
        "pointing_offset_out"
    ]["messages"]
    assert len(websocket_output_raw) > 0
    websocket_output_dict = [
        msgpack.unpackb(data) for data in websocket_output_raw
    ]

    for data in websocket_output_dict:
        assert "antenna_name" in data
        assert "xel_offset" in data
        assert "xel_offset_std" in data
        assert "el_offset" in data
        assert "el_offset_std" in data
        assert "expected_width_h" in data
        assert "expected_width_v" in data
        assert "fitted_width_h" in data
        assert "fitted_width_h_std" in data
        assert "fitted_width_v" in data
        assert "fitted_width_v_std" in data
        assert "fitted_height" in data
        assert "fitted_height_std" in data

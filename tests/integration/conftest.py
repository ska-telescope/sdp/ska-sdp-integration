"""Test configuration and shared test steps."""

# pylint: disable=unused-argument

import asyncio
import json
import logging
import threading

import pytest
import websockets
from common.device_operations import TRANSLATIONS, set_state_and_obs_state
from common.kubernetes import local_volume
from common.tango import tango_client
from common.telescope_metadata import MetadataConfiguration
from common.wait import wait_for_obs_state
from pytest_bdd import given, parsers, then, when

LOG = logging.getLogger(__name__)


@pytest.fixture(scope="module", name="signal_display_websocket_data")
def signal_display_websocket_data_fixt():
    """This fixture will act as a cache for the websocket thread data."""
    data = {"threads": {}, "data": {}}
    return data


@given("I connect to an SDP subarray", target_fixture="subarray_device")
def connect_to_subarray(context):
    """
    Connect to the subarray device.

    :param context: context for the tests
    :returns: SDP subarray device client

    """
    device = tango_client(
        context.sdp.namespace,
        context.sdp.subarray_device_name,
        translations=TRANSLATIONS,
    )
    LOG.info(
        "Connecting to subarray device %s", context.sdp.subarray_device_name
    )
    yield device
    # Teardown of subarray device
    LOG.info(
        "Teardown of subarray device %s", context.sdp.subarray_device_name
    )
    if device.read_attribute("State") == "ON":
        device.execute_command("Off")


@given("I connect to the SDP controller", target_fixture="controller_device")
def connect_to_controller(context):
    """
    Connect to the controller device.

    :param context: context for tests
    :returns: SDP controller device client

    """
    return tango_client(
        context.sdp.namespace,
        context.sdp.controller_device_name,
        translations=TRANSLATIONS,
    )


@given(
    "I connect to the Queue Connector Device",
    target_fixture="queue_connector_device",
)
def connect_to_queueconnector(context):
    """
    Connect to the queue connector device.

    :param context: context for tests
    :returns: Queue Connector Device

    """
    return tango_client(
        context.sdp.namespace,
        context.sdp.queue_connector_device_name,
    )


@given(
    parsers.parse("I select the dataset for the {mode:S} telescope"),
    target_fixture="telescope_metadata",
)
def set_config_params(mode):
    """
    Set parameters to determine test configuration
    """

    config = MetadataConfiguration(mode, pipeline=None)
    return config


@given(
    parsers.parse(
        "I select the dataset for the {mode:S} {pipeline:S} pipeline"
    ),
    target_fixture="telescope_metadata",
)
def set_pipeline_config_params(mode, pipeline):
    """
    Set parameters to determine test configuration
    """

    config = MetadataConfiguration(mode, pipeline)
    return config


@given("the volumes are created and the CBF emulator input data is copied")
def create_local_volume(context, k8s_element_manager, telescope_metadata):
    """
    Create the volumes and copy the data

    :param context: context for the tests
    :param k8s_element_manager: Kubernetes element manager
    """
    LOG.info("Creating local volume")
    local_volume(context, k8s_element_manager, telescope_metadata)


@given(parsers.parse("obsState is {obs_state:S}"))
def set_obs_state(subarray_device, obs_state):
    """
    Set the obsState to the desired value.

    This function sets the device state to ON.

    :param subarray_device: SDP subarray device client
    :param obs_state: desired obsState
    """
    set_state_and_obs_state(subarray_device, "ON", obs_state)


@given("I deploy the visibility receive script")
@given("I deploy the visibility receive script configured to read pointings")
@given(
    "I deploy the visibility receive and pointing offset "
    "scripts configured to use telescope model data"
)
def deploy_script(
    vis_receive_script,
    context,
    subarray_device,
    assign_resources_config,
):
    """
    Deploy visibility receive script.

    This uses the vis_receive_script fixture to automatically end the
    script when the test is finished.

    :param vis_receive_script: visibility receive script fixture
    :param context: context for the tests
    :param subarray_device: SDP subarray device client
    :param assign_resources_config: AssignResources configuration
    :param mode: mode for telescope metadata

    """

    # Wait for the obsState to become the expected value
    wait_for_obs_state(
        subarray_device, "IDLE", timeout=context.timeouts.assign_res_timeout
    )

    # Check the assign resources config and subarray receive addresses
    pb_id = assign_resources_config["processing_blocks"][0]["pb_id"]
    deployment_name = f"proc-{pb_id}-vis-receive"
    receiver_pod_name = f"{deployment_name}-00-0"
    receiver_service_name = deployment_name
    receive_addresses_expected = (
        f"{receiver_pod_name}.{receiver_service_name}."
        f"{context.sdp.namespace_sdp}"
    )

    # Get the DNS hostname from receive addresses attribute
    receive_addresses = json.loads(
        subarray_device.read_attribute("receiveAddresses")
    )
    host = receive_addresses["science"]["vis0"]["host"][0][1]
    # had to set up a partial check because test_qa_metric_receive.py
    # doesn't use the latest vis-receive script, which no longer
    # appends `svc.cluster.local` to the end of the host dns
    assert receive_addresses_expected in host


@then(parsers.parse("the state is {state:S}"))
def state_is(subarray_device, state):
    """
    Check the device state.

    :param subarray_device: SDP subarray device client
    :param final_state: expected state value

    """
    assert subarray_device.read_attribute("State") == state


@then(parsers.parse("obsState is {obs_state:S}"))
def obs_state_is(subarray_device, obs_state):
    """
    Check the obsState.

    :param subarray_device: SDP subarray device client
    :param obs_state: the expected obsState

    """
    assert subarray_device.read_attribute("obsState") == obs_state


@then(parsers.parse("the device raises an {reason:S} exception"))
def raised_exception(command_exception, reason):
    """
    Check that device has raised a Tango exception.

    :param command_exception: exception raised by device command
    :param reason: reason for exception

    """
    assert command_exception is not None
    assert command_exception.reason == reason


@then(parsers.parse("the subarray healthState is {state:S}"))
def subarray_health_state_is(subarray_device, state):
    """
    Check the device attribute healthState.

    :param subarray_device: SDP subarray device client
    :param state: expected healthState value
    """
    assert subarray_device.read_attribute("healthState") == state


# Signal Display Related shared steps:


@when(
    parsers.parse(
        "I listen for the Signal Metrics {graph_type:S} websocket with "
        "{count_steps:Number} messages",
        extra_types={"Number": int},
    )
)
def listen_to_the_websocket_with_specified_steps(
    context,
    graph_type,
    telescope_metadata,
    signal_display_websocket_data,
    count_steps,
):
    """Start a new thread to listen to the websocket data with specified step
    count."""

    _listen_to_websocket_shared_code(
        context,
        graph_type,
        telescope_metadata,
        signal_display_websocket_data,
        count_steps,
    )


@when(
    parsers.parse("I listen for the Signal Metrics {graph_type:S} websocket")
)
def listen_to_the_websocket(
    context,
    graph_type,
    telescope_metadata,
    signal_display_websocket_data,
    count_steps=None,
):
    """Get data from Signal Metrics websockets with configured step count"""
    _listen_to_websocket_shared_code(
        context,
        graph_type,
        telescope_metadata,
        signal_display_websocket_data,
        count_steps,
    )


def _listen_to_websocket_shared_code(
    context,
    graph_type,
    telescope_metadata,
    signal_display_websocket_data,
    count_steps=None,
):
    """Start a new thread to listen to the websocket data."""

    def _run_thread(
        context, graph_type, telescope_metadata, websocket_data, count_steps
    ):
        asyncio.run(
            read_websocket_and_run_script(
                context,
                graph_type,
                telescope_metadata,
                websocket_data,
                count_steps,
            )
        )

    LOG.info("Create background websocket task")
    websocket_data = {"messages": [], "timeout": False}
    thread = threading.Thread(
        target=_run_thread,
        args=(
            context,
            graph_type,
            telescope_metadata,
            websocket_data,
            count_steps,
        ),
    )
    thread.start()

    signal_display_websocket_data["threads"][graph_type] = thread
    signal_display_websocket_data["data"][graph_type] = websocket_data


@when("I wait for all websockets to finish")
def wait_for_websocket(signal_display_websocket_data):
    """Wait for the websocket thread to finish, and collect the data."""
    LOG.info("Waiting for websocket thread to finish...")
    for _, thread in signal_display_websocket_data["threads"].items():
        thread.join()
    LOG.info("Websocket thread finished")


async def read_websocket_and_run_script(
    context, test_type, telescope_metadata, websocket_data, count_steps=None
):
    """Connect to the websocket, and get the data.

    :param context: The context for this test
    """
    # pylint: disable=no-member

    expected_time_steps = (
        telescope_metadata.expected_time_steps
        if count_steps is None
        else count_steps
    )

    base_url = f"ws://{context.sdp.qa_metric_host[7:]}"
    subarray_id = context.sdp.subarray_id
    url = f"{base_url}/ws/metrics-{test_type}-{subarray_id}"

    LOG.info("Using URL: %s", url)

    messages = []
    timeout = False

    async with websockets.connect(
        url,
        max_size=1_000_000_000,
    ) as websocket:
        try:
            _ = await websocket.recv()  # {"status": "connected"}

            for i in range(expected_time_steps):
                messages.append(
                    await asyncio.wait_for(
                        websocket.recv(),
                        # pylint: disable=line-too-long
                        timeout=context.timeouts.wait_to_execute_timeout,  # noqa: E501
                    )
                )
                LOG.info(
                    "Received messages %d/%d",
                    i + 1,
                    expected_time_steps,
                )
        except asyncio.TimeoutError:
            LOG.warning("Timeout occurred")
            timeout = True
        except websockets.exceptions.ConnectionClosedOK:
            LOG.warning("Connection Closed with OK")
            timeout = True
        except websockets.exceptions.ConnectionClosedError:
            LOG.warning("Connection Closed with ERROR")
            timeout = True

    websocket_data["messages"] = messages
    websocket_data["timeout"] = timeout

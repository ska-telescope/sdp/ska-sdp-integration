{{/*
Generate Job to configure Tango DB.
This template should be called with a context containing:
  - config: configuration to be written to Tango DB
  - tangohost: the location of the Tango DB
  - root: the root context of the chart
*/}}
{{- define "ska-sdp.mock-dish-config" }}
---
# Configmap containing device server configuration
apiVersion: v1
kind: ConfigMap
metadata:
  name: {{ include "mock-dish.name" .root }}-config
  namespace: {{ .root.Release.Namespace }}
  labels:
    {{- include "mock-dish.labels" .root | indent 4 }}
    component: {{ .root.Values.mock_dish.component }}-configuration
    subsystem: enabling-system
    function: deployment
    domain: self-configuration
    intent: enabling
data:
  {{- (.root.Files.Glob "data/safe-dsconfig.sh").AsConfig | nindent 2 }}
  mock-dish-devices.json: |
    {{- toPrettyJson .config | nindent 4 }}
---
# Job to apply device server configuration to Tango database
apiVersion: batch/v1
kind: Job
metadata:
  name: {{ include "mock-dish.name" .root }}-config
  namespace: {{ .root.Release.Namespace }}
  labels:
    {{- include "mock-dish.labels" .root | indent 4 }}
    component: {{ .root.Values.mock_dish.component }}-configuration
    subsystem: enabling-system
    function: deployment
    domain: self-configuration
    intent: enabling
spec:
  ttlSecondsAfterFinished: {{ .root.Values.jobs.ttl }}
  template:
    spec:
      initContainers:
      - name: check-databaseds-ready
        image: {{ .root.Values.dsconfig.image.registry }}/{{ .root.Values.dsconfig.image.image }}:{{ .root.Values.dsconfig.image.tag }}
        imagePullPolicy: {{ .root.Values.dsconfig.image.pullPolicy }}
        command:
          - sh
          - -c
        args:
          - retry
          {{- if .root.Values.global.retry }}
          {{- range $retry_option := .root.Values.global.retry }}
          - {{ $retry_option }}
          {{- end }}
          {{- else }}
          - --max=60
          {{- end}}
          - --
          - tango_admin
          - --check-device
          - sys/database/2
        env:
        - name: TANGO_HOST
          value: {{ .tangohost }}
        {{- if (.root.Values.global.environment_variables) }}
        {{- range $index, $envvar := .root.Values.global.environment_variables }}
        - name: {{$envvar.name}}
          value: {{tpl ($envvar.value | toString) $ }}
        {{- end }}
        {{- end }}
      containers:
      - name: dsconfig
        image: {{ .root.Values.dsconfig.image.registry }}/{{ .root.Values.dsconfig.image.image }}:{{ .root.Values.dsconfig.image.tag }}
        imagePullPolicy: {{ .root.Values.dsconfig.image.pullPolicy }}
        command:
          - sh
          - -c
        args:
          - retry --sleep=1 --tries=60 -- /bin/bash data/safe-dsconfig.sh -w -a -u data/mock-dish-devices.json
        env:
        - name: TANGO_HOST
          value: {{ .tangohost }}
        volumeMounts:
          - name: configuration
            mountPath: data
            readOnly: true
      volumes:
        - name: configuration
          configMap:
            name: {{ include "mock-dish.name" .root }}-config
      restartPolicy: Never
{{- end }}

{{/*
Generate StatefulSet containing a Tango device server.
This template should be called with a context containing:
  - device: the details of the device, with entries:
    + name: Name (e.g. controller)
    + tangoname: Tango device name (e.g. mid-sdp/control/0)
    + command: Command to run the device (e.g. ["SDPController"])
    + args: Arguments to pass to command (e.g. ["0", "-v4"])
    + function: function label (e.g. sdp-monitoring)
    + domain: domain label (e.g. sdp-central-control)
    + livenessProbe:
     - initialDelaySeconds - initial delay
     - periodSeconds - probe period
     - maxAgeSeconds - maximum age of test file
  - tangohost: location of the Tango DB
  - root: the root context of the chart
*/}}
{{- define "ska-sdp.mock-dish-device" }}
---
# Dummy Service to ensure Pod is DNS addressable
apiVersion: v1
kind: Service
metadata:
  name: {{ include "mock-dish.name" .root }}-{{ .device.name }}
  namespace: {{ .root.Release.Namespace }}
  labels:
    {{ include "mock-dish.labels" .root | indent 4 }}
    component: {{ .root.Values.mock_dish.component }}-{{ .device.name }}
    subsystem: {{ .root.Values.mock_dish.subsystem }}
    function: {{ .device.function }}
    domain: {{ .device.domain }}
    intent: production
spec:
  clusterIP: None
  selector:
    component: {{ .root.Values.mock_dish.component }}-{{ .device.name }}
    subsystem: {{ .root.Values.mock_dish.subsystem }}
---
# StatefulSet with single Pod
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: {{ include "mock-dish.name" .root }}-{{ .device.name }}
  namespace: {{ .root.Release.Namespace }}
  labels:
    {{ include "mock-dish.labels" .root | indent 4 }}
    component: {{ .root.Values.mock_dish.component }}-{{ .device.name }}
    subsystem: {{ .root.Values.mock_dish.subsystem }}
    function: {{ .device.function }}
    domain: {{ .device.domain }}
    intent: production
spec:
  selector:
    matchLabels:
      component: {{ .root.Values.mock_dish.component }}-{{ .device.name }}
      subsystem: {{ .root.Values.mock_dish.subsystem }}
  serviceName: {{ include "mock-dish.name" .root }}-{{ .device.name }}
  replicas: 1
  template:
    metadata:
      labels:
        {{- include "mock-dish.labels" .root | indent 8 }}
        component: {{ .root.Values.mock_dish.component }}-{{ .device.name }}
        subsystem: {{ .root.Values.mock_dish.subsystem }}
        function: {{ .device.function }}
        domain: {{ .device.domain }}
        intent: production
    spec:
      initContainers:
      - name: wait-for-device-config
        image: {{ .root.Values.dsconfig.image.registry }}/{{ .root.Values.dsconfig.image.image }}:{{ .root.Values.dsconfig.image.tag }}
        imagePullPolicy: {{ .root.Values.dsconfig.image.pullPolicy }}
        command:
          - retry
          - --max=10
          - --
          - tango_admin
          - --check-device
          - {{ .device.tangoname | toString }}
        env:
        - name: TANGO_HOST
          value: {{ .tangohost }}
      containers:
      - name: {{ .device.name }}
        image: {{ .device.image }}
        imagePullPolicy: {{ .root.Values.mock_dish.imagePullPolicy }}
        command: {{ toJson .device.command }}
        args: {{ toJson .device.args}}
        env:
        - name: SDP_LMC_LOOP_INTERVAL
          value: {{ .root.Values.mock_dish.loopInterval | int | quote }}
        - name: TANGO_HOST
          value: {{ .tangohost }}
        - name: FEATURE_ALL_COMMANDS_HAVE_ARGUMENT
          value: {{ .root.Values.mock_dish.allCommandsHaveArgument | int | quote }}
        - name: FEATURE_STRICT_VALIDATION
          value: {{ .root.Values.mock_dish.strictValidation | int | quote }}
        # Setting OMP_NUM_THREADS=1 prevents numpy from creating threads at import time,
        # which interfers with cppTango's signal handling logic, and thus prevents a clean
        # shutdown upon SIGTERM when the pod is killed.
        # See https://gitlab.com/tango-controls/cppTango/-/issues/927#note_960601299 for details
        - name: OMP_NUM_THREADS
          value: "1"
        {{- if .device.livenessProbe }}
        livenessProbe:
          exec:
            command:
            - /bin/sh
            - -c
            - '[ $(($(date +%s)-$(stat -c "%Y" /tmp/alive))) -lt {{ .device.livenessProbe.maxAgeSeconds }} ]'
          initialDelaySeconds: {{ .device.livenessProbe.initialDelaySeconds }}
          periodSeconds: {{ .device.livenessProbe.periodSeconds }}
          failureThreshold: 1
        {{- end }}
        readinessProbe:
          exec:
            command:
            - python
            - -c
            - |
              import sys
              import tango
              try:
                  proxy = tango.DeviceProxy("{{ .device.tangoname }}")
                  print("Got device status: %s", proxy.Status())
              except:
                  print("Can't contact device to get status")
                  sys.exit(1)
        volumeMounts:
        - name: {{ .root.Values.pvc.name }}
          mountPath: {{ .root.Values.pvc.path }}
      volumes:
      - name: {{ .root.Values.pvc.name }}
        persistentVolumeClaim:
          claimName: {{ .root.Values.pvc.name }}
{{- end }}

## Data sets for testing pointing calibration

Source of MS data:
https://console.cloud.google.com/storage/browser/ska1-simulation-data/simulations/pointing-offset/orc-2052/reordered

Source of HDF5 files:
https://console.cloud.google.com/storage/browser/ska1-simulation-data/simulations/pointing-offset/orc-2052/pointing-hdfs

A .tar file can also be found in GCP containing all the pointing data used in the integration test:
https://console.cloud.google.com/storage/browser/_details/ska1-simulation-data/simulations/pointing-offset/orc-2052/reordered/pointing-data.tar

Once downloaded with GitLFS, run the following command to extract the data::

     tar -xvf pointing-data.tar 

The dataset can be found in the integration repository as well (actually used by the tests) at:
``tests/resources/data/pointing-data``

MS data:
    channels: 8
        freq_min: 1.300e+09 Hz
        freq_max: 1.405e+09 Hz
        Note: in ``tests/resources/subarray-json/telescope_metadata.json`` these values are set to
              ```"freq_min": 1.2925e9``` and ```"freq_max": 1.4125e9``` because this way the mswriter 
              data's frequencies will match the input MS frequencies
    antennas: 4
        ``["SKA001", "SKA036", "SKA063", "SKA100"]``

HDF data in pointing-hdfs:
    actual, requested, source_offset
    all contain 5 time samples each

"""
Compares the SOURCE_OFFSET data from
resulting measurement set and HDF5 input file

Notes:

    If the test fails at this point it's most likely due to running this
    multiple times without redeploying SDP. Since subsequent runs contains
    identical data timestamps but ascending Kafka timestamps the pointings
    processor will error due to out of order data timestamps. This will
    require manually tearing down the Kafka pod for it to reset. The
    pointings processor additionally does not filter a window of Kafka
    timestamps, thus may result in any number of pointing rows in the
    resulting measurement set.

    Reshaping of result source offset data will only work if the test is
    run using a freshly installed kafka pod if it is rerun on an existing
    deployment and kafka wasn't restarted, the number of data points will
    be duplicated and the test will likely fail. (see also above comment)
"""

# This doesn't run in the sdp-integration context, so it's useless
# to check the imports exist.
# pylint: disable=import-error

import logging
import sys

import h5py
from casacore import tables
from numpy.testing import assert_almost_equal

TIMEOUT = 10
RETRY_INTERVAL = 0.5

MEASUREMENT_SET = sys.argv[1]
TEST_HDF = sys.argv[2]
RESHAPE = sys.argv[3].split(",")

LOG = logging.getLogger(__name__)
LOG.setLevel(logging.DEBUG)


def read_result_data():
    """Read pointing (SOURCE_OFFSET) data from MS"""
    LOG.info("Attempting to open %s", MEASUREMENT_SET)
    table = tables.table(MEASUREMENT_SET)

    LOG.info("Opening pointing table")
    pointing_table = tables.table(table.getkeyword("POINTING"))
    LOG.info("Successfully opened pointing table")

    source_offset_data = pointing_table.getcol("SOURCE_OFFSET")

    # data needs to be reshaped and transposed before comparing with the input
    to_shape = [int(shape) for shape in RESHAPE]
    source_offset_data = source_offset_data.reshape(to_shape)[
        :, :, 0
    ].transpose()
    return source_offset_data[1:]


def read_expected_data():
    """Read pointing (SOURCE_OFFSET) data from HFD5 file"""
    LOG.info("Reading SOURCE_OFFSET data from HDF file.")
    with h5py.File(TEST_HDF, "r") as hdf_file:
        pointing_table = hdf_file["PointingTable0"]
        source_offset_data = pointing_table["data_pointing"][()]
        # dims [ntimes, nants, 1, 1, 2] -> we remove the last 3 dimensions
        # data shape: [5, 4, 1, 1, 2]
        source_offset_data = source_offset_data[:, :, 0, 0, 0]

    LOG.info("Successfully read data from HDF file.")
    return source_offset_data


def main():
    """Compare SOURCE_OFFSET data"""
    result_source_offset_data = read_result_data()
    expected_source_offset_data = read_expected_data()

    LOG.debug(
        "Expected source offset data from HDF5: %s",
        expected_source_offset_data,
    )
    LOG.debug(
        "Result source offset data from MS: %s", result_source_offset_data
    )
    assert_almost_equal(
        result_source_offset_data, expected_source_offset_data, decimal=8
    )


if __name__ == "__main__":
    main()

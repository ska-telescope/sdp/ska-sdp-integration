"""Test scan ID from Measurement Set matches expected value."""

# pylint: disable=import-error
# pylint: disable=invalid-name
# pylint: disable=protected-access

import sys

from realtime.receive.core.msutils import MeasurementSet

ms = MeasurementSet.open(sys.argv[1])
expected_scan_id = int(sys.argv[2])
ms_scan_id = ms._model.scan_number
print(f"scan ID expected v/s MS: {expected_scan_id} / {ms_scan_id}")
sys.exit(int(expected_scan_id != ms_scan_id))

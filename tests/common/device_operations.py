"""Common functions related to controller and subarray device operations"""

import json
import logging
import os
import random
from datetime import date

from common import TIMEOUT
from common.tango import DevError
from common.wait import wait_for_obs_state
from ska_control_model import AdminMode, HealthState, ObsState

LOG = logging.getLogger(__name__)

# Enum definitions to be used to translate controller and subarray device
# attribute values from integers into strings
TRANSLATIONS = {
    "adminMode": AdminMode,
    "healthState": HealthState,
    "obsState": ObsState,
}

CONFIGURED_TIME_TO_READY = 10


def create_id(prefix):
    """
    Create an ID with the given prefix.

    The ID will contain today's date and a random 5-digit number.

    :param prefix: the prefix

    """
    generator = "test"
    today = date.today().strftime("%Y%m%d")
    number = random.randint(0, 99999)
    return f"{prefix}-{generator}-{today}-{number:05d}"


def read_json_data(filename):
    """
    Read data from JSON file in the data directory.

    :param filename: name of the file to read

    """
    path = os.path.join("tests", "resources", "subarray-json", filename)
    with open(path, "r", encoding="utf-8") as file_n:
        data = json.load(file_n)
    return data


def set_state_and_obs_state(
    device, state, obs_state, assignres_timeout=TIMEOUT
):
    """
    Set subarray device state and observing state.

    :param device: subarray device proxy
    :param state: the desired device state
    :param obs_state: the desired observing state
    :param assignres_timeout: timeout specifically set for AssignResources [s]
    """
    current = (
        device.read_attribute("State"),
        device.read_attribute("obsState"),
    )
    target = (state, obs_state)
    commands = transition_commands(current, target)

    for command in commands:
        call_command(device, command)
        if command == "AssignResources":
            wait_for_obs_state(device, "IDLE", timeout=assignres_timeout)

    assert device.read_attribute("State") == state
    assert device.read_attribute("obsState") == obs_state


def transition_commands(current, target):
    """
    Get commands to transition subarray from current state to target state.

    :param current: tuple of current state and obs_state
    :param target: tuple of target state and obs_state
    :returns: list of commands

    """
    # Mapping of state and obs_state to state number
    state_number = {
        ("OFF", "EMPTY"): 0,
        ("ON", "EMPTY"): 1,
        ("ON", "IDLE"): 2,
        ("ON", "READY"): 3,
        ("ON", "SCANNING"): 4,
    }
    # Command to transition to previous state number
    command_prev = [None, "Off", "ReleaseResources", "End", "EndScan"]
    # Command to transition to next state number
    command_next = ["On", "AssignResources", "Configure", "Scan", None]

    current_number = state_number[current]
    target_number = state_number[target]

    if target_number < current_number:
        commands = [
            command_prev[i] for i in range(current_number, target_number, -1)
        ]
    elif target_number > current_number:
        commands = [
            command_next[i] for i in range(current_number, target_number)
        ]
    else:
        commands = []

    return commands


def call_command(subarray_device, command, test_configure=False):
    """
    Call a subarray device command.

    :param subarray_device: SDP subarray device client
    :param command: name of command to call
    :param test_configure: switch to set time_to_ready param
        for test-receive-addresses script, when Configure
        command transition from CONFIGURING to READY is tested
    """
    # Check command is present
    assert command in subarray_device.get_command_list()
    # Get the command argument
    if command in [
        "AssignResources",
        "ReleaseResources",
        "Configure",
        "Scan",
    ]:
        config = read_command_argument(command)
        if command == "AssignResources" and test_configure:
            # Only used with the test-receive-addresses script (v0.6.1+)
            config["processing_blocks"][0]["parameters"][
                "time_to_ready"
            ] = CONFIGURED_TIME_TO_READY

        argument = json.dumps(config)
    else:
        argument = None

    # Remember the EB ID
    if command == "AssignResources":
        subarray_device.eb_id = config["execution_block"]["eb_id"]
    elif command == "End":
        subarray_device.eb_id = None

    # Call the command
    subarray_device.execute_command(command, argument=argument)


def call_command_exception(device, command, argument=None):
    """
    Call a device command and return exception if raised.

    :param device: device proxy
    :param command: command to call
    :param argument: argument to command
    :returns: exception if raised, None otherwise

    """
    try:
        device.execute_command(command, argument=argument)
        return None
    except DevError as exc:
        return exc


def read_command_argument(name):
    """
    Read command argument from JSON file.

    :param name: name of command

    """
    config = read_json_data(f"command_{name}.json")

    if name == "AssignResources":
        # Insert new IDs into configuration
        config["execution_block"]["eb_id"] = create_id("eb")
        for pblock in config["processing_blocks"]:
            pblock["pb_id"] = create_id("pb")

    return config


def subarray_trigger_error_messages(subarray, simulated_mode, ar_timeout):
    """
    Use test-realtime script to trigger changes in the
    errorMessages attribute of the subarray device.

    :param subarray: SDP subarray device client
    :param simulated_mode: options are:
        - "failed_app": the processing script simulates the case
          when an execution engine starts correctly, but its
          application fails with an error (not breaking the pod itself)
        - "failed_engine": the processing script simulates the case
          when an execution engine pod fails to start
    :param ar_timeout: AssignResources timeout in seconds
    """
    command = "AssignResources"
    config = read_command_argument(command)
    script_def = {
        "kind": "realtime",
        "name": "test-realtime",
        "version": "1.0.0",
    }
    config["processing_blocks"][0]["script"] = script_def

    match simulated_mode:
        case "failed_app":
            config["processing_blocks"][0]["parameters"] = {
                "simulate_failed_engine_app": True
            }
            obs_state = "IDLE"
        case "failed_engine":
            config["processing_blocks"][0]["parameters"] = {
                "simulate_failed_engine_start": True
            }
            obs_state = "FAULT"
        case _:
            raise ValueError("Simulated mode unknown")

    argument = json.dumps(config)
    # Call the command; note that this will not raise an exception
    call_command_exception(subarray, command, argument)

    wait_for_obs_state(subarray, obs_state, timeout=ar_timeout)

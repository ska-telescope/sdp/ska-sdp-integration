"""Functions to wait for conditions to be satisfied."""

import time

import pytest

TIMEOUT = 60.0
INTERVAL = 0.5


def wait_for_predicate(
    predicate,
    description,
    timeout=TIMEOUT,
    interval=INTERVAL,
    break_clause=None,
):
    """
    Wait for predicate to return a truthy value, also returning it.

    :param predicate: callable to test
    :param description: description to use if test fails
    :param timeout: timeout in seconds
    :param interval: interval between tests of the predicate in seconds
    :param break_clause: a callable that returns a string describing
        why predicate cannot carry on. If this happens, pytest raises an error.
    """
    start = time.time()
    while True:
        result = predicate()
        if result:
            return result

        if time.time() >= start + timeout:
            pytest.fail(f"{description} not achieved after {timeout} seconds")

        if break_clause:
            if cause_to_break := break_clause():
                pytest.fail(
                    f"{description} not achieved because of {cause_to_break}"
                )

        time.sleep(interval)


def wait_for_state(device, state, timeout=TIMEOUT):
    """
    Wait for device state to have the expected value.

    :param device: device client
    :param state: the expected state
    :param timeout: timeout in seconds

    """

    def predicate():
        return device.read_attribute("State") == state

    description = f"Device state {state}"
    wait_for_predicate(predicate, description, timeout=timeout)


def wait_for_admin_mode(device, admin_mode, timeout=TIMEOUT):
    """
    Wait for adminMode to have the expected value.

    :param device: device client
    :param admin_mode: the expected state
    :param timeout: timeout in seconds

    """

    def predicate():
        return device.read_attribute("adminMode") == admin_mode

    description = f"adminMode {admin_mode}"
    wait_for_predicate(predicate, description, timeout=timeout)


def wait_for_obs_state(device, obs_state, timeout=TIMEOUT):
    """
    Wait for obsState to have the expected value.

    :param device: device proxy
    :param obs_state: the expected value
    :param timeout: timeout in seconds
    """

    def predicate():
        return device.read_attribute("obsState") == obs_state

    # pylint: disable-next=inconsistent-return-statements
    def except_clause():
        if device.read_attribute("obsState") == "FAULT":
            return "Error: Subarray obsState is FAULT"

    description = f"obsState {obs_state}"
    wait_for_predicate(
        predicate, description, timeout=timeout, break_clause=except_clause
    )

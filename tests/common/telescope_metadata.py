"""Functions for handling telescope metadata"""

import logging

from common.device_operations import create_id, read_json_data

LOG = logging.getLogger(__name__)


# pylint: disable-next=too-few-public-methods, too-many-instance-attributes
class MetadataConfiguration:
    """
    Class to configure the telescope metadata
    """

    def __init__(self, mode, pipeline=None):
        """
        Set the metadata according to the test pre-sets

        param mode: The test mode which determines how the assign resources
                    string is contructed and how other metadata are set.
        param pipeline: Test pipeline
        """

        config_mapping = read_json_data("telescope_metadata.json")

        # Obtain and apply the mode for the test, converting to lowercase
        mode = mode.lower()

        self.telescope = mode

        if pipeline:
            self.mode = pipeline.lower()
        else:
            self.mode = mode

        LOG.info("mode: %s", self.mode)

        config = config_mapping[self.mode]

        # Set the mode dependent variables for use in the test steps
        self.data_dir = config["ms_files"]
        self.num_streams = config["num_streams"]
        self.local_volume_dirs = [
            f"{config['extra_data_dir']}/{ms_file}"
            for ms_file in config["ms_files"]
        ]

        tar_file = config.get("data_tar_file")
        self.data_tar_file = (
            f"{config['extra_data_dir']}/{tar_file}" if tar_file else None
        )
        self.expected_time_steps = config["expected_time_steps"]
        self.receptors = config["receptors"]

        self.telescope_model = config["telescope_model"]

        # Adapt the assign resources string template for the test
        self.assign_resources = self._create_assign_resources_string(config)

    def _create_assign_resources_string(self, config):
        """
        Edit the base assign resources string based on the mode
        """

        assign_res_string = read_json_data("assign_resources_base.json")

        # set eb_id
        assign_res_string["execution_block"]["eb_id"] = create_id("eb")

        # set receptors
        assign_res_string["resources"]["receptors"] = self.receptors

        # set count
        assign_res_string["execution_block"]["channels"][0][
            "spectral_windows"
        ][0]["count"] = config["count"]

        # set channels per port
        assign_res_string["processing_blocks"][0]["parameters"][
            "channels_per_port"
        ] = config["channels_per_port"]

        # set telmodel key
        assign_res_string["processing_blocks"][0]["parameters"][
            "extra_helm_values"
        ]["receiver"]["options"]["telescope_model"] = self.telescope_model

        # set freq_min and freq_max
        assign_res_string["execution_block"]["channels"][0][
            "spectral_windows"
        ][0]["freq_min"] = config["freq_min"]

        assign_res_string["execution_block"]["channels"][0][
            "spectral_windows"
        ][0]["freq_max"] = config["freq_max"]

        for pblock in assign_res_string["processing_blocks"]:
            # set pb_id
            pblock["pb_id"] = create_id("pb")

            # Set parameters for the rcal test
            if self.mode == "rcal":
                pblock["parameters"]["processors"] = {
                    "rcal": config["rcal_processor_overrides"]
                }

        if corr_type := config.get("polarisation", {}).get("corr_type"):
            assign_res_string["execution_block"]["polarisations"][0][
                "corr_type"
            ] = corr_type

        return assign_res_string

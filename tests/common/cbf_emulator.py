# pylint: disable=too-many-positional-arguments
"""Functions for deploying and using the CBF emulator"""

import json
import logging
import tempfile
from typing import Iterable

import yaml
from common.kubernetes import Comparison, wait_for_pod

LOG = logging.getLogger(__name__)


def deploy_sender(  # pylint: disable=too-many-arguments, too-many-locals,
    endpoint,
    scan_id,
    ms_data,
    context,
    k8s_element_manager,
    telescope_metadata,
    sender_version: str | None = None,
):
    """
    Deploy the cbf sender and check the cbf sender finished sending the data.

    :param endpoint: A 2-tuple `(host, port)` indicating the first endpoint
        where the receiver is expected to be listening on
    :param scan_id: Scan ID to associated with the data stream
    :param ms_data: MS data to send
    :param context: context for the tests - parameters used are
        sdp: pvc_name, subarray_id, namespace
        timeouts: wait_for_pod_timeout
    :param k8s_element_manager: Kubernetes element manager
    :param telescope_metadata: fixture for storing and managing metadata
    :param sender_version: version of the emulator to use
    """
    # Collect telescope metadata variations
    num_streams = telescope_metadata.num_streams
    telescope = telescope_metadata.telescope

    # Construct command for the sender
    host, port = endpoint
    command = [
        "emu-send",
        f"/mnt/data/test_data/{ms_data}",
        "-o",
        "transmission.transport_protocol=tcp",
        "-o",
        "transmission.method=spead2_transmitters",
        "-o",
        f"transmission.num_streams={num_streams}",
        "-o",
        "transmission.rate=10416667",
        "-o",
        "reader.num_repeats=1",
        "-o",
        f"transmission.target_host={host}",
        "-o",
        f"transmission.target_port_start={port}",
        "-o",
        f"sender.scan_id={scan_id}",
        "-o",
        f"transmission.telescope={telescope.lower()}",
    ]
    values = {
        "command": command,
        "receiver": {"hostname": host, "address_resolution_timeout": 60},
        "pvc": {"name": context.sdp.pvc_name},
    }
    if sender_version:
        values["cbf_sdp_emulator"] = {"version": sender_version}

    # Deploy the sender
    with tempfile.NamedTemporaryFile(
        mode="w", suffix=".yaml", delete=False
    ) as file:
        file.write(yaml.dump(values))
    filename = file.name
    sender_name = f"cbf-send-{context.sdp.subarray_id}-scan-{scan_id}"
    LOG.info(
        "Deploying CBF sender for Scan %d on chart %s", scan_id, sender_name
    )
    k8s_element_manager.helm_install(
        sender_name,
        "tests/resources/charts/cbf-sender",
        context.sdp.namespace,
        filename,
    )
    assert wait_for_pod(
        sender_name,
        context.sdp.namespace,
        "Succeeded",
        context.timeouts.wait_for_pod_timeout,
        name_comparison=Comparison.CONTAINS,
    )


# pylint: disable-next=inconsistent-return-statements
def send_scans(  # pylint: disable=too-many-arguments,too-many-locals
    context,
    subarray_device,
    subarray_ready,
    k8s_element_manager,
    telescope_metadata,
    scans_and_ms: Iterable[tuple[int, str, str]],
    dish_list: list | None = None,
    before_end_callback=None,
    **kwargs,
):
    """
    Deploys a sender per scan serially, specified by id and type

    :param context: test context fixture
    :param subarray_device: subarray tango device fixture
    :param subarray_ready: subarray_ready fixture
    :param k8s_element_manager: K8sElementManager fixture
    :param telescope_metadata: telescope metadata fixture
    :param scans_and_ms: tuple of (scan_id, scan_type, ms_data)
        ms_data for the specific scan
    :param dish_list: list of tango devices representing dishes (optional)
    :param before_end_callback: function to run before End() is
        executed (optional)
    """
    prev_scan_type = None
    receive_addresses = json.loads(
        subarray_device.read_attribute("receiveAddresses")
    )

    with subarray_ready() as do_scan:
        for scan_id, scan_type, ms_data in scans_and_ms:
            host = receive_addresses[scan_type]["vis0"]["host"][0][1]
            port = receive_addresses[scan_type]["vis0"]["port"][0][1]
            with do_scan(
                scan_id, scan_type, prev_scan_type, dish_list=dish_list
            ):
                deploy_sender(
                    (host, port),
                    scan_id,
                    ms_data,
                    context,
                    k8s_element_manager,
                    telescope_metadata,
                    **kwargs,
                )
            prev_scan_type = scan_type

        # functions that need to run before we execute End()
        # e.g. load data from the QueueConnector device
        if before_end_callback:
            return before_end_callback()

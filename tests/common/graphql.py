"""GraphQL client."""

import requests


class GraphQLClient:
    """
    Simple GraphQL client.

    :param url: URL of GraphQL endpoint
    :param cookies: Cookies to authorise use of endpoint
    """

    # pylint: disable=too-few-public-methods

    def __init__(self, url: str, cookies: dict | None = None):
        self._url = url
        self._cookies = cookies

    def execute(self, query: str, variables: dict | None = None):
        """
        Execute query on GraphQL server.

        :param query: query to execute (can be query or mutation)
        :param variables: variables for query

        """
        data = {"query": query, "variables": variables}
        response = requests.post(
            url=self._url,
            cookies=self._cookies,
            json=data,
            timeout=10.0,
        )
        response.raise_for_status()
        return response.json()

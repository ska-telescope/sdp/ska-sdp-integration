"""TangoGQL-based client."""

from typing import Any

from ..graphql import GraphQLClient
from .base import DevError, TangoClient


def _raise_query_error(response):
    if "errors" in response and response["errors"]:
        # take the first one, there might be many
        error = response["errors"][0]
        raise DevError(desc=error["desc"], reason=error["reason"])


class TangoClientGQL(TangoClient):
    """
    Client for a Tango device using TangoGQL.

    :param url: URL of TangoGQL server
    :param device_family_member: device name with just family/member parts
    :param translations: optional translations for attribute values
    """

    query_get_command_list = """
        query GetCommandList($device: String!) {
            device(name: $device) {
                commands {
                    name
                }
            }
        }
    """

    query_get_attribute_list = """
        query GetAttributeList($device: String!) {
          device(name: $device) {
            attributes {
              name
              label
              dataformat
              datatype
            }
          }
        }
    """

    query_read_attribute = """
        query ReadAttribute($device: String!, $attribute: String!) {
            device(name: $device) {
                attributes(pattern: $attribute) {
                    value
                }
            }
        }
    """

    mutation_write_attribute = """
        mutation WriteAttribute($device: String!, $attribute: String!, $value: ScalarTypes!) {
            setAttributeValue(device: $device, name: $attribute, value: $value) {
                ok
                message
            }
        }
    """  # noqa: E501

    mutation_execute_command = """
        mutation ExecuteCommand($device: String!, $command: String!, $argument: ScalarTypes) {
            executeCommand(device: $device, command: $command, argin: $argument) {
                ok
                message
                output
            }
        }
    """  # noqa: E501

    def __init__(
        self,
        url: str,
        device_name: str,
        translations: dict | None = None,
        cookies: dict | None = None,
    ):
        self._client = GraphQLClient(url, cookies=cookies)
        super().__init__(device_name, translations=translations)

    def get_command_list(self) -> list[str] | None:
        """
        Get list of commands from device.

        :returns: list of command names

        """
        variables = {"device": self._device}
        response = self._client.execute(self.query_get_command_list, variables)

        _raise_query_error(response)

        commands = [
            command["name"]
            for command in response["data"]["device"]["commands"]
        ]
        if not commands:
            raise DevError(
                desc=f"Cannot get command list from {self._device}",
                reason="Unknown",
            )
        return commands

    def get_attribute_list(self) -> list[str]:
        """
        Get list of attributes from device.

        :returns: list of attribute names

        """
        variables = {"device": self._device}
        response = self._client.execute(
            self.query_get_attribute_list, variables
        )

        _raise_query_error(response)

        attributes = [
            attribute["name"]
            for attribute in response["data"]["device"]["attributes"]
        ]
        if not attributes:
            raise DevError(
                desc=f"Cannot get attribute list from {self._device}",
                reason="Unknown",
            )
        return attributes

    def read_attribute(self, attribute: str) -> Any:
        """
        Read value of device attribute.

        :param attribute: name of attribute
        :returns: value of attribute

        """
        variables = {"device": self._device, "attribute": attribute}
        response = self._client.execute(self.query_read_attribute, variables)

        _raise_query_error(response)

        attributes = response["data"]["device"]["attributes"]
        if not attributes:
            raise DevError(
                desc=f"Cannot read attribute {attribute} from {self._device}",
                reason="Unknown",
            )

        return self._translate_attribute(attribute, attributes[0]["value"])

    def write_attribute(self, attribute: str, value: Any) -> None:
        """
        Write value of device attribute.

        :param attribute: name of attribute
        :param value: value of attribute

        """
        variables = {
            "device": self._device,
            "attribute": attribute,
            "value": value,
        }
        response = self._client.execute(
            self.mutation_write_attribute, variables=variables
        )
        if not response["data"]["setAttributeValue"]["ok"]:
            desc, reason = response["data"]["setAttributeValue"]["message"]
            raise DevError(desc=desc, reason=reason)

    def execute_command(
        self, command: str, argument: str | None = None
    ) -> Any:
        """
        Execute command.

        :param command: name of command
        :param argument: optional argument for command
        :returns: return value of command

        """
        variables = {
            "device": self._device,
            "command": command,
            "argument": argument,
        }
        response = self._client.execute(
            self.mutation_execute_command, variables=variables
        )
        if not response["data"]["executeCommand"]["ok"]:
            desc, reason = response["data"]["executeCommand"]["message"]
            raise DevError(desc=desc, reason=reason)
        return response["data"]["executeCommand"]["output"]


def get_device_domain_gql(
    url: str,
    cookies: dict | None = None,
) -> str:
    """
    Get the SDP device domain.
    """
    client = GraphQLClient(url, cookies=cookies)
    query_get_device_domain = """
            query GetDeviceDomain($pattern: String!) {
                domains(pattern: $pattern) {
                    name
                }
            }
        """
    variables = {"pattern": "*-sdp"}

    response = client.execute(query_get_device_domain, variables=variables)
    _raise_query_error(response)

    return response["data"]["domains"][0]["name"]

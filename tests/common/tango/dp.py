"""Tango device proxy-based client."""

from typing import Any

try:
    import tango
except ImportError:
    tango = None

from .base import DevError, TangoClient


class TangoClientDP(TangoClient):
    """
    Client for a Tango device using Python Tango DeviceProxy

    :param device_family_member: device name with just family/member parts
    :param translations: optional translations for attribute values
    """

    def __init__(
        self,
        device_name: str,
        translations: dict | None = None,
    ):
        super().__init__(device_name, translations=translations)
        self._proxy = tango.DeviceProxy(device_name)

    def get_command_list(self) -> list[str]:
        """
        Get list of commands from device.

        :returns: list of command names

        """
        try:
            return self._proxy.get_command_list()
        except tango.DevFailed as exc:
            raise DevError(
                desc=exc.args[0].desc, reason=exc.args[0].reason
            ) from exc

    def get_attribute_list(self) -> list[str]:
        """
        Get list of attributes from device.

        :returns: list of attribute names

        """
        try:
            return self._proxy.get_attribute_list()
        except tango.DevFailed as exc:
            raise DevError(
                desc=exc.args[0].desc, reason=exc.args[0].reason
            ) from exc

    def read_attribute(self, attribute: str) -> Any:
        """
        Read value of device attribute.

        :param attribute: name of attribute
        :returns: value of attribute

        """
        try:
            value = self._proxy.read_attribute(attribute)
            if attribute.lower() == "state":
                value = value.value.name
            else:
                value = self._translate_attribute(attribute, value.value)
            return value
        except tango.DevFailed as exc:
            raise DevError(
                desc=exc.args[0].desc, reason=exc.args[0].reason
            ) from exc

    def write_attribute(self, attribute: str, value: Any) -> None:
        """
        Write value of device attribute.

        :param attribute: name of attribute
        :param value: value of attribute

        """
        try:
            self._proxy.write_attribute(attribute, value)
        except tango.DevFailed as exc:
            raise DevError(
                desc=exc.args[0].desc, reason=exc.args[0].reason
            ) from exc

    def execute_command(
        self, command: str, argument: str | None = None
    ) -> Any:
        """
        Execute command.

        :param command: name of command
        :param argument: optional argument for command
        :returns: return value of command

        """
        try:
            return self._proxy.command_inout(command, cmd_param=argument)
        except tango.DevFailed as exc:
            raise DevError(
                desc=exc.args[0].desc, reason=exc.args[0].reason
            ) from exc

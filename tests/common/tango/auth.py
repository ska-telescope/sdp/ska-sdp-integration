"""Authorisation."""

import requests


def get_taranta_auth_cookies(url: str, username: str, password: str) -> dict:
    """Get cookies from Taranta auth service."""
    response = requests.post(
        url=url,
        json={"username": username, "password": password},
        timeout=10.0,
    )
    auth_token = response.cookies.get("taranta_jwt")
    if auth_token is None:
        raise RuntimeError("Unable to get auth token from Taranta")
    return {"taranta_jwt": auth_token}

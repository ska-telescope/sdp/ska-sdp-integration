"""Tango client base class."""

from typing import Any


class DevError(Exception):
    """
    Tango device error exception.

    :param desc: description of exception
    :param reason: reason for exception
    """

    def __init__(self, desc=None, reason=None):
        super().__init__()
        self.desc = desc
        self.reason = reason

    def __str__(self):
        return f'DevError: desc="{self.desc}", reason="{self.reason}"'


class TangoClient:
    """
    Base class for Tango device clients.

    :param device: name of device
    :param translations: optional translations for attribute values
    """

    def __init__(self, device: str, translations: dict | None = None):
        self._device = device
        self._translations = translations
        self._exception = None

    def _translate_attribute(self, attribute: str, value: Any) -> Any:
        """
        Translate attribute value.

        :param attribute: attribute name
        :param value: attribute value
        :returns: translated attribute value

        """
        if self._translations and attribute in self._translations:
            try:
                value = self._translations[attribute](value).name
            except ValueError:
                value = None
        return value

    def get_command_list(self) -> list[str] | None:
        """
        Get list of commands from device.

        :returns: list of command names

        """
        raise NotImplementedError()

    def get_attribute_list(self) -> list[str]:
        """
        Get list of attributes from device.

        :returns: list of attribute names

        """
        raise NotImplementedError()

    def read_attribute(self, attribute: str) -> Any:
        """
        Read value of device attribute.

        :param attribute: name of attribute
        :returns: value of attribute

        """
        raise NotImplementedError()

    def write_attribute(self, attribute: str, value: Any) -> None:
        """
        Write value of device attribute.

        :param attribute: name of attribute
        :param value: value of attribute

        """
        raise NotImplementedError()

    def execute_command(
        self, command: str, argument: str | None = None
    ) -> Any:
        """
        Execute command.

        :param command: name of command
        :param argument: optional argument for command
        :returns: return value of command

        """
        raise NotImplementedError()

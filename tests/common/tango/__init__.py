"""Tango device client."""

import os

from .auth import get_taranta_auth_cookies
from .base import DevError, TangoClient
from .dp import TangoClientDP
from .gql import TangoClientGQL, get_device_domain_gql

__all__ = [
    "DevError",
    "TangoClient",
    "TangoClientDP",
    "TangoClientGQL",
    "tango_client",
]

# Tango device client (specific to SDP integration):
# "dp" = use PyTango device proxy
# "gql" = use TangoGQL
TANGO_CLIENT = os.environ.get("TEST_TANGO_CLIENT", "dp")

if TANGO_CLIENT == "gql":
    INGRESS = os.environ.get("TEST_INGRESS")
    AUTH_URL = os.environ.get("TARANTA_AUTH_URL")
    USERNAME = os.environ.get("TARANTA_USERNAME")
    PASSWORD = os.environ.get("TARANTA_PASSWORD")
    COOKIES = get_taranta_auth_cookies(AUTH_URL, USERNAME, PASSWORD)


def tango_client(
    namespace: str,
    device_name: str,
    translations: dict | None = None,
) -> TangoClient:
    """
    Create Tango device client based on test context.

    :param namespace: SDP namespace
    :param device_name: device name (domain/family/member)
    :param translations: optional translations for attribute values

    """
    if TANGO_CLIENT == "dp":
        return TangoClientDP(
            device_name,
            translations=translations,
        )

    if TANGO_CLIENT == "gql":
        if INGRESS:
            # Test is running outside cluster, so use ingress
            tangogql_url = f"{INGRESS}/{namespace}/taranta/db"

            return TangoClientGQL(
                tangogql_url,
                device_name,
                translations=translations,
                cookies=COOKIES,
            )

        # Test is running inside cluster
        raise RuntimeError("Cannot use TangoGQL client without ingress")

    raise RuntimeError(f"Unknown tango client: {TANGO_CLIENT}")


def get_device_domain(namespace: str) -> str:
    """
    Return the domain of a tango device

    :param namespace: namespace where the tango db is located
    """
    if TANGO_CLIENT == "dp":
        try:
            # pylint: disable=import-outside-toplevel
            import tango
        except ImportError:
            tango = None

        tango_db = tango.Database()
        return tango_db.get_device_domain("*-sdp/*")[0]

    if TANGO_CLIENT == "gql":
        if INGRESS:
            # Test is running outside cluster, so use ingress
            tangogql_url = f"{INGRESS}/{namespace}/taranta/db"
            device_domain = get_device_domain_gql(
                tangogql_url, cookies=COOKIES
            )
            return device_domain

        # Test is running inside cluster
        raise RuntimeError("Cannot use TangoGQL client without ingress")

    raise RuntimeError(f"Unknown tango client: {TANGO_CLIENT}")

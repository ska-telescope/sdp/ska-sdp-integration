"""Test configuration and shared test fixtures"""

# pylint: disable=attribute-defined-outside-init
# pylint: disable=too-few-public-methods
# pylint: disable=too-many-instance-attributes

import contextlib
import json
import logging
import os
from types import SimpleNamespace

import pytest
from common import K8sElementManager
from common.device_operations import read_command_argument
from common.tango import get_device_domain
from common.wait import wait_for_obs_state, wait_for_state

LOG = logging.getLogger(__name__)


class Context(SimpleNamespace):
    """Generic holder for context information."""


@pytest.fixture(scope="session", name="context")
def context_fixt():
    """Get context for tests from environment."""

    # Create the context to be passed to the tests
    test_context = Context()

    test_context.sdp = sdp_context()
    test_context.timeouts = timeouts_context()
    test_context.alarm_handler = Context(
        alarm_handler_device_name="alarm/handler/01"
    )

    return test_context


def sdp_context():
    """Fill context information for SDP parameters."""

    # Create the context to be passed to the tests
    sdp_context_obj = Context()

    # ==============
    # Environment variables to set up context (for internal use only)

    # Ingress to connect to the cluster - if not set, the test is assumed to be
    # running inside the cluster
    ingress = os.environ.get("TEST_INGRESS")

    # ==============
    # SDP specific environment variables

    # Which SDP Subarray device to connect to
    sdp_context_obj.subarray_id = os.environ.get("SUBARRAY_ID", "01")
    # Kubernetes namespace
    sdp_context_obj.namespace = os.environ.get("KUBE_NAMESPACE")
    sdp_context_obj.namespace_sdp = os.environ.get("KUBE_NAMESPACE_SDP")

    sdp_context_obj.pvc_name = os.environ.get("DATA_PRODUCT_PVC_NAME")

    # =====
    # DEVICE NAMES
    device_domain = get_device_domain(sdp_context_obj.namespace)

    sdp_context_obj.subarray_device_name = (
        f"{device_domain}/subarray/{sdp_context_obj.subarray_id}"
    )
    sdp_context_obj.controller_device_name = f"{device_domain}/control/0"
    sdp_context_obj.queue_connector_device_name = (
        f"{device_domain}/queueconnector/{sdp_context_obj.subarray_id}"
    )

    # =====
    # HOSTS

    if ingress:
        sdp_context_obj.qa_metric_host = (
            f"{ingress}/{sdp_context_obj.namespace}/signal/api"
        )
    else:
        sdp_context_obj.qa_metric_host = (
            f"http://ska-sdp-qa-api.{sdp_context_obj.namespace}:8002"
        )

    sdp_context_obj.kafka_host = (
        f"ska-sdp-kafka.{sdp_context_obj.namespace}:9092"
    )

    return sdp_context_obj


def timeouts_context():
    """Fill context information for timeout parameters."""

    # Create the context to be passed to the tests
    timeouts_context_obj = Context()

    # ========
    # TIMEOUTS

    # Custom timeouts
    timeouts_context_obj.assign_res_timeout = int(
        os.environ.get("TEST_TIMEOUT_ASSIGN_RES", 120)
    )
    timeouts_context_obj.configure_timeout = int(
        os.environ.get("TEST_TIMEOUT_CONFIGURE", 300)
    )
    timeouts_context_obj.wait_for_pod_timeout = int(
        os.environ.get("TEST_TIMEOUT_WAIT_POD", 300)
    )
    timeouts_context_obj.device_on_off_timeout = int(
        os.environ.get("TEST_TIMEOUT_DEVICE_ON_OFF", 20)
    )
    timeouts_context_obj.wait_to_execute_timeout = int(
        os.environ.get("TEST_TIMEOUT_WAIT_TO_EXECUTE", 180)
    )

    return timeouts_context_obj


@pytest.fixture(name="k8s_element_manager")
def k8s_element_manager_fixt():
    """Allow easy creation, and later automatic destruction, of k8s elements"""
    manager = K8sElementManager()
    yield manager
    manager.cleanup()


@pytest.fixture(name="dataproduct_directory")
def dataproduct_directory_fixt(assign_resources_config):
    """
    The directory where output files will be written.

    :param assign_resources_config: AssignResources configuration
    """
    eb_id = assign_resources_config["execution_block"]["eb_id"]
    pb_id = assign_resources_config["processing_blocks"][0]["pb_id"]
    return f"/product/{eb_id}/ska-sdp/{pb_id}"


@pytest.fixture(name="subarray_ready")
def subarray_ready_fixt(subarray_device, context):
    """
    Execute a scan.

    :param subarray_device: SDP subarray device client
    :param context: test context defined in conftest.py
    """
    configure_command = read_command_argument("Configure")
    scan_command = read_command_argument("Scan")

    @contextlib.contextmanager
    def subarray_ready():
        # We don't Configure() here as each scan will do their own
        # configuration depending on the scan type.
        wait_for_obs_state(
            subarray_device,
            "IDLE",
            timeout=context.timeouts.assign_res_timeout,
        )

        yield subarray_do_scan

        # Can't be called in fixture as we need this to terminate the receive
        # containers so all files are flushed and closed by the time we verify
        # their contents.
        LOG.info("Calling End")
        subarray_device.execute_command("End")
        wait_for_obs_state(subarray_device, "IDLE")

    @contextlib.contextmanager
    def subarray_do_scan(
        scan_id, scan_type_id, prev_scan_type=None, dish_list=None
    ):
        # If previous scan_type is same as the current scan type
        # then don't need to run Configure command
        if scan_type_id != prev_scan_type:
            # Configure
            LOG.info("Calling Configure(scan_type=%s)", scan_type_id)
            configure_command["scan_type"] = scan_type_id
            subarray_device.execute_command(
                "Configure",
                argument=json.dumps(configure_command),
            )
            wait_for_obs_state(
                subarray_device,
                "READY",
                timeout=context.timeouts.configure_timeout,
            )

        # Scan
        LOG.info("Calling Scan(scan_id=%d)", scan_id)
        scan_command["scan_id"] = scan_id

        # Set the mock dishes, if provided, to Scan for pointing
        if dish_list is None:
            dish_list = []
        for dish in dish_list:
            dish.execute_command("Scan", scan_id)
        for dish in dish_list:
            wait_for_state(
                dish, "ON", timeout=context.timeouts.device_on_off_timeout
            )

        subarray_device.execute_command("Scan", json.dumps(scan_command))
        wait_for_obs_state(subarray_device, "SCANNING")

        try:
            yield
        finally:
            # Revert back to IDLE
            LOG.info("Calling EndScan")
            subarray_device.execute_command("EndScan")

            for dish in dish_list:
                dish.execute_command("EndScan")
            for dish in dish_list:
                wait_for_state(
                    dish, "OFF", timeout=context.timeouts.device_on_off_timeout
                )

            wait_for_obs_state(subarray_device, "READY")

    return subarray_ready


@pytest.fixture(name="vis_receive_script")
def run_vis_receive_script(subarray_device, assign_resources_config):
    """
    Start the visibility receive script and end it after the test.

    This uses the k8s_element_manager fixture to ensure the script is ended
    before the PVCs are removed.

    :param subarray_device: SDP subarray device client
    :param assign_resources_config: AssignResources configuration

    """

    # Start the script
    LOG.info("Calling AssignResources")
    subarray_device.execute_command(
        "AssignResources", argument=json.dumps(assign_resources_config)
    )

    yield

    # End the script
    LOG.info("Calling ReleaseAllResources")
    subarray_device.execute_command("ReleaseAllResources")

Feature: SDP Integration - Visibility Receive

    Background: Common setup for visibility receive
        Given I connect to an SDP subarray
        And obsState is EMPTY

    @XTP-28695 @XTP-5424 @visibility_receive @SKA_low
    Scenario: Execute visibility receive script for the Low telescope
        Given I select the dataset for the Low telescope
        And the volumes are created and the CBF emulator input data is copied
        And I deploy the visibility receive script
        When SDP is commanded to capture data from 2 successive scans
        Then the data received matches with the data sent
        And each scan can be identified by its associated metadata

    @XTP-28696 @XTP-5424 @visibility_receive @SKA_mid
    Scenario: Execute visibility receive script for the Mid telescope
        Given I select the dataset for the Mid telescope
        And the volumes are created and the CBF emulator input data is copied
        And I deploy the visibility receive script
        When SDP is commanded to capture data from 2 successive scans
        Then the data received matches with the data sent
        And each scan can be identified by its associated metadata

    @extended_visibility_receive_test
    Scenario: Execute visibility receive script for extended period
        Given I select the dataset for the Mid telescope
        And the volumes are created and the CBF emulator input data is copied
        And I deploy the visibility receive script
        When SDP is commanded to run scans for 20 minutes
        Then the data received matches with the data sent
        And each scan can be identified by its associated metadata

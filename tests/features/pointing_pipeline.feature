@SKA_mid @pointings
Feature: SDP Integration - Pointing Calibration

    @XTP-33673 @XTP-33652
    Scenario: Visibility receive with pointings
        Given I connect to an SDP subarray
        And obsState is EMPTY
        And I select the dataset for the Mid pointing pipeline
        And the volumes are created and the CBF emulator input data is copied
        And mock dish devices are deployed with pointing data
        And I deploy the visibility receive script configured to read pointings
        And the queue connector is configured to write pointings to appropriate data queues
        When I listen for the Signal Metrics pointing_offset_out websocket with 1 messages
        And SDP is commanded to capture data from the Mid telescope
        And I wait for all websockets to finish
        Then the data received contains the correct pointing data
        And the queue connector device contains the correct pointing calibration results
        And QA metric data adheres to a schema that describes the format of the metric data

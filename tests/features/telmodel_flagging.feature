Feature: SDP Integration - Telescope Model and Flagging Control

    @XTP-75251 @XTP-76805
    Scenario: SDP loads processing script definitions from telescope model
        Given the SDP script definitions are available as telescope model data
        When I read the script definitions from the configuration database
        Then the script definitions from the two sources are the same

    @XTP-75233 @XTP-76805 @SKA_mid
    Scenario: Pointing pipeline uses RFI mask from telescope model data
        Given I connect to an SDP subarray
        And obsState is EMPTY
        And I select the dataset for the Mid telescope
        And the volumes are created and the CBF emulator input data is copied
        And telescope model data for telescope layout and static RFI mask are available
        And I deploy the visibility receive and pointing offset scripts configured to use telescope model data
        When SDP is commanded to capture data for pointing scans
        Then the receiver is using the Mid layout configuration from telescope model
        And the pointing offset calibration pipeline is using the RFI mask from telescope model
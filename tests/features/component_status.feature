Feature: SDP Integration - Component Status

    @XTP-24549 @XTP-5425
    Scenario Outline: Component status is ONLINE
        Given I connect to the SDP controller
        Then the component <component> has the status ONLINE

        Examples:
        | component       |
        | helmdeploy      |
        | proccontrol     |
        | lmc-subarray-01 |

    @XTP-24550 @XTP-5425
    Scenario Outline: Controller health state reflects status of components
        Given I connect to the SDP controller
        When the SDP component <component> is unavailable
        Then the controller healthState is <health_state>
        And the component <component> has the status OFFLINE

        Examples:
        | component       | health_state |
        | helmdeploy      | DEGRADED     |
        | proccontrol     | DEGRADED     |

    @XTP-49803 @XTP-5425
    Scenario Outline: Subarray health state reflects status of components
        Given I connect to an SDP subarray
        And obsState is EMPTY
        When the SDP component <component> is unavailable
        Then the subarray healthState is <health_state>
        And the AssignResources command raises an API_CommandFailed exception

        Examples:
        | component   | health_state |
        | helmdeploy  | DEGRADED     |
        | proccontrol | DEGRADED     |

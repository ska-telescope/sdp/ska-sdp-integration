@qa-metrics
Feature: SDP Integration - QA Display

    @XTP-49808 @XTP-5781
    Scenario Outline: Retrieve the current configuration
        Given the QA API is running
        When I query the config db endpoints for <config_type>
        Then the correct data layouts are retrieved for <config_type>

        Examples:
        | config_type       |
        | subarrays         |
        | execution_blocks  |
        | processing_blocks |
        | deployments       |

    Scenario: Confirm Websockets are accessible
        Given the QA API is running
        When I access each websocket
        Then Each websocket responds with a connected message

    @XTP-27059 @XTP-5781 @SKA_low
    Scenario: Generation of Visibility Receive Graphs
        Given I select the dataset for the Low telescope
        And I connect to an SDP subarray
        And obsState is EMPTY
        And the volumes are created and the CBF emulator input data is copied
        And I deploy the visibility receive script
        When I listen for the Signal Metrics amplitude websocket
        When I listen for the Signal Metrics phase websocket
        When I listen for the Signal Metrics spectrum websocket
        When I listen for the Signal Metrics lagplot websocket
        When I listen for the Signal Metrics bandaveragedxcorr websocket
        When I listen for the Signal Metrics uvcoverage websocket
        And SDP is commanded to capture data from the Low telescope
        And I wait for all websockets to finish
        Then QA metric data is available
        And adheres to a schema that describes the format of the amplitude metric data and its associated metadata
        And adheres to a schema that describes the format of the phase metric data and its associated metadata
        And adheres to a schema that describes the format of the spectrum metric data and its associated metadata
        And adheres to a schema that describes the format of the lagplot metric data and its associated metadata
        And adheres to a schema that describes the format of the bandaveragedxcorr metric data and its associated metadata
        And adheres to a schema that describes the format of the uvcoverage metric data and its associated metadata

    @XTP-27060 @XTP-5781 @SKA_mid
    Scenario: QA Metrics Stats are sent via the Tango Queue Connector Device
        Given I select the dataset for the Mid telescope
        And I connect to an SDP subarray
        And obsState is EMPTY
        And the volumes are created and the CBF emulator input data is copied
        And I connect to the Queue Connector Device
        And I deploy the visibility receive script
        And the queue connector device is configured
        When I listen to the Queue Connector Device for QA Metrics
        And SDP is commanded to capture data from the Mid telescope
        And I wait for stats to finish collecting
        Then the device responds with appropriate initial data


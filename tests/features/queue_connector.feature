Feature: SDP Integration - Queue Connector

    Scenario: Queue Connector Device Available Commands
        Given I connect to the Queue Connector Device
        When I query list of available commands
        Then the expected command list is returned

Feature: SDP Integration - Controller

    @XTP-31248 @XTP-5415
    Scenario: State is DISABLE when admin mode is OFFLINE
        Given I connect to the SDP controller
        And adminMode is OFFLINE
        Then the state is DISABLE

    @XTP-31249 @XTP-5415
    Scenario Outline: Command is rejected when admin mode is OFFLINE
        Given I connect to the SDP controller
        And adminMode is OFFLINE
        When I call <command>
        Then the device raises an API_CommandNotAllowed exception

        Examples:
        | command |
        | Off     |
        | Standby |
        | On      |

    @XTP-5416 @XTP-5415
    Scenario Outline: Command succeeds in allowed state
        Given I connect to the SDP controller
        And the state is <initial_state>
        When I call <command>
        Then the state is <final_state>

        Examples:
        | initial_state | command | final_state |
        | OFF           | Standby | STANDBY     |
        | OFF           | On      | ON          |
        | STANDBY       | Off     | OFF         |
        | STANDBY       | On      | ON          |
        | ON            | Off     | OFF         |
        | ON            | Standby | STANDBY     |

    @XTP-5417 @XTP-5415
    Scenario Outline: Command is rejected in disallowed state
        Given I connect to the SDP controller
        And the state is <initial_state>
        When I call <command>
        Then the device raises an API_CommandNotAllowed exception

        Examples:
        | initial_state | command |
        | OFF           | Off     |
        | STANDBY       | Standby |
        | ON            | On      |

@SKA_low @RCal
Feature: SDP Integration - Real-time Calibration

    @XTP-42929 @XTP-49804
    Scenario: Visibility receive with bandpass calibration
        Given I connect to an SDP subarray
        And obsState is EMPTY
        And I select the dataset for the Low RCAL pipeline
        And the volumes are created and the CBF emulator input data is copied
        And I deploy the visibility receive script configured to calibrate
        And the queue connector is configured to receive cal solutions from appropriate data queues
        When I listen for the Signal Metrics gain_calibration_out websocket with 1 messages
        And SDP is commanded to capture data from the Low telescope
        And I wait for all websockets to finish
        Then the published gains are correct
        And QA metric data adheres to a schema that describes the format of the metric data
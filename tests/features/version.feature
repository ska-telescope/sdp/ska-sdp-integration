@lmc_controller_version
Feature: SDP Integration - Version Information

    @XTP-28693 @XTP-28692
    Scenario: SDP version information is available
        Given I connect to the SDP controller
        When I read the sdpVersion attribute
        Then it contains the components and dependencies with the correct structure and versions

    @XTP-28694 @XTP-28692
    Scenario: Version ID is available
        Given I connect to the SDP controller
        When I read the versionId attribute
        Then I will see the correctly deployed version string

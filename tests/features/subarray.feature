Feature: SDP Integration - Subarray

    @XTP-31250 @XTP-5347
    Scenario: State is DISABLE when admin mode is OFFLINE
        Given I connect to an SDP subarray
        And adminMode is OFFLINE
        Then the state is DISABLE

    @XTP-31251 @XTP-5347
    Scenario Outline: Command is rejected when admin mode is OFFLINE
        Given I connect to an SDP subarray
        And adminMode is OFFLINE
        When I call <command>
        Then the device raises an API_CommandNotAllowed exception

        Examples:
        | command          |
        | Off              |
        | On               |
        | AssignResources  |
        | ReleaseResources |
        | Configure        |
        | End              |
        | Scan             |
        | EndScan          |
        | Abort            |
        | Restart          |

    @XTP-917 @XTP-5347
    Scenario Outline: On/Off command
        Given I connect to an SDP subarray
        And the state is <initial_state>
        When I call <command>
        Then the state is <final_state>
        And obsState is EMPTY

        Examples:
        | initial_state | command | final_state |
        | OFF           | On      | ON          |
        | ON            | Off     | OFF         |

    @XTP-71703 @XTP-5347
    Scenario: healthState is DEGRADED when a execution engine returns an application error
        Given I connect to an SDP subarray
        And obsState is EMPTY
        When I call AssignResources and the processing block is RUNNING but has error messages
        Then the subarray healthState is DEGRADED
        And the errorMessages tango attribute is populated

    @XTP-71704 @XTP-5347
    Scenario: obsState is FAULT when processing block state is FAILED
        Given I connect to an SDP subarray
        And obsState is EMPTY
        When I call AssignResources and the processing block state becomes FAILED
        Then obsState becomes FAULT
        And the errorMessages tango attribute is populated

    @XTP-120 @XTP-5347
    Scenario: AssignResources command assigns resources and starts execution block
        Given I connect to an SDP subarray
        And obsState is EMPTY
        When I call AssignResources
        Then obsState becomes IDLE
        And resources has the expected value
        And ebID has the expected value
        And receiveAddresses has the expected value

    @XTP-5348 @XTP-5347
    Scenario: Configure command sets scan type
        Given I connect to an SDP subarray
        And obsState is IDLE
        When I call Configure
        Then obsState is READY
        And scanType has the expected value

    @XTP-24830 @XTP-5347
    Scenario: Configure command transitions from CONFIGURING to READY
        Given I connect to an SDP subarray
        And obsState is EMPTY
        When I call AssignResources with a script for testing the Configure transition
        And I call Configure
        Then obsState is CONFIGURING
        And obsState becomes READY

    @XTP-5349 @XTP-5347
    Scenario: Scan command sets scan ID
        Given I connect to an SDP subarray
        And obsState is READY
        When I call Scan
        Then obsState is SCANNING
        And scanID has the expected value

    @XTP-5350 @XTP-5347
    Scenario: EndScan command clears scan ID
        Given I connect to an SDP subarray
        And obsState is SCANNING
        When I call EndScan
        Then obsState is READY
        And scanID is 0

    @XTP-5351 @XTP-5347
    Scenario: End command clears scan type and ends execution block
        Given I connect to an SDP subarray
        And obsState is READY
        When I call End
        Then obsState is IDLE
        And scanType is null
        And ebID is null
        And receiveAddresses is empty

    @XTP-122 @XTP-5347
    Scenario: ReleaseResources command releases resources
        Given I connect to an SDP subarray
        And obsState is IDLE
        When I call ReleaseResources
        Then obsState is EMPTY
        And resources is empty


    @XTP-965 @XTP-5347
    Scenario Outline: Command is rejected with an invalid JSON configuration
        Given I connect to an SDP subarray
        And obsState is <obs_state>
        When I call <command> with an invalid JSON configuration
        Then the device raises an API_CommandFailed exception

        Examples:
        | obs_state | command         |
        | EMPTY     | AssignResources |
        | IDLE      | Configure       |
        | READY     | Scan            |

    @XTP-20570 @XTP-5347
    Scenario Outline: AssignResources command is rejected with an invalid ID
        Given I connect to an SDP subarray
        And obsState is EMPTY
        When I call AssignResources with an invalid <entity> ID
        Then the device raises an API_CommandFailed exception

        Examples:
        | entity           |
        | execution-block  |
        | processing-block |

    @XTP-20571 @XTP-5347
    Scenario Outline: AssignResources command is rejected when processing script definition is incorrect
        Given I connect to an SDP subarray
        And obsState is EMPTY
        When I call AssignResources with malformed script of <script_problem>
        Then the device raises an API_CommandFailed exception

        Examples:
        | script_problem                 |
        | non-existent script definition |
        | incompatibly-versioned script  |
        | incompatible script parameters |

    @XTP-972 @XTP-5347
    Scenario Outline: Command is rejected in disallowed obsState
        Given I connect to an SDP subarray
        And obsState is <obs_state>
        When I call <command>
        Then the device raises an API_CommandNotAllowed exception

        Examples:
        | obs_state | command          |
        | EMPTY     | ReleaseResources |
        | EMPTY     | Configure        |
        | EMPTY     | End              |
        | EMPTY     | Scan             |
        | EMPTY     | EndScan          |
        | EMPTY     | Abort            |
        | EMPTY     | Restart          |
        | IDLE      | Scan             |
        | IDLE      | EndScan          |
        | IDLE      | Restart          |
        | READY     | AssignResources  |
        | READY     | ReleaseResources |
        | READY     | EndScan          |
        | READY     | Restart          |
        | SCANNING  | AssignResources  |
        | SCANNING  | ReleaseResources |
        | SCANNING  | Configure        |
        | SCANNING  | End              |
        | SCANNING  | Scan             |
        | SCANNING  | Restart          |

    @XTP-949 @XTP-5347
    Scenario Outline: Command is rejected when the state is OFF
        Given I connect to an SDP subarray
        And the state is OFF
        When I call <command>
        Then the device raises an API_CommandNotAllowed exception

        Examples:
        | command          |
        | Off              |
        | AssignResources  |
        | ReleaseResources |
        | Configure        |
        | End              |
        | Scan             |
        | EndScan          |
        | Abort            |
        | Restart          |

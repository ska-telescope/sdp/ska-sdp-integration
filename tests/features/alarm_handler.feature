Feature: SDP Integration - Triggering the Alarm Handler

    @XTP-75254 @XTP-75290
    Scenario: SDP Controller triggers alarms
        Given I connect to the SDP controller
        And I connect to the SKA alarm handler and configure it for sdp_healthstate_degraded alarm
        When the controller's healthState is DEGRADED
        Then the alarm handler's sdp_healthstate_degraded alarm is raised with priority warning

    @XTP-75246 @XTP-75290
    Scenario Outline: SDP Subarray triggers alarms
        Given I connect to an SDP subarray
        And I connect to the SKA alarm handler and configure it for <alarm_attribute> alarm
        When the subarray's <attribute> is <status>
        Then the alarm handler's <alarm_attribute> alarm is raised with priority <priority>

        Examples:
        | attribute     | status    | alarm_attribute                   | priority |
        | healthState   | DEGRADED  | sdp_subarray_healthstate_degraded | warning  |
        | obsState      | FAULT     | sdp_subarray_obsstate_fault       | fault    |
